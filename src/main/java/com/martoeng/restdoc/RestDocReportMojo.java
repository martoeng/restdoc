package com.martoeng.restdoc;

import java.io.File;
import java.util.Locale;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import com.martoeng.restdoc.model.RestDocConfiguration;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.project.Project;

/**
 * A Mojo for the creation of a RESTdoc report.
 * 
 * @author WalterM
 * 
 */
@Mojo(name = "restdoc", requiresProject = true, defaultPhase = LifecyclePhase.SITE)
public class RestDocReportMojo extends AbstractMavenReport {

    /** The "official" product name. **/
    public static final String PRODUCT_NAME = "RESTdoc";

    /** The base name of the created output folder. **/
    public static final String OUTPUT_NAME = "restdoc";

    /** The output directory, normally the Maven /target/ folder. **/
    @Parameter(property = "restdoc.outputDirectory", defaultValue = "${project.reporting.outputDirectory}/" + OUTPUT_NAME + "/")
    private File outputDirectory;

    /** The source directory under which all normal packages are located. **/
    @Parameter(property = "codereport.sourceDirectory", defaultValue = "${project.build.sourceDirectory}")
    private File sourceDirectory;

    /** String array defining the packages to exclude in the report. **/
    @Parameter
    private String[] excludedPackages;

    /** String array defining the packages to include in the report. **/
    @Parameter
    private final String[] includedPackages = new String[0];

    /** String array containing return value documentation patterns. **/
    @Parameter
    private final String[] returnPatternStrings = new String[0];

    @Parameter
    private String fileEncoding;

    /** The actual Maven project. **/
    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Component
    private Renderer siteRenderer;

    private final RestDocConfiguration configuration = new RestDocConfiguration();


    @Override
    public String getOutputName() {
        return OUTPUT_NAME;
    }

    @Override
    public String getName(final Locale locale) {
        return PRODUCT_NAME;
    }

    @Override
    public String getDescription(final Locale locale) {
        return "REST resource documentation";
    }

    @Override
    protected Renderer getSiteRenderer() {
        return this.siteRenderer;
    }

    @Override
    protected String getOutputDirectory() {
        return this.outputDirectory.getAbsolutePath();
    }

    @Override
    protected MavenProject getProject() {
        return this.project;
    }

    @Override
    public void execute() throws MojoExecutionException {
        if (includedPackages != null) configuration.setIncludedPackages(includedPackages);
        if (excludedPackages != null) configuration.setExcludedPackages(excludedPackages);
        if (returnPatternStrings != null) configuration.setReturnPatternStrings(returnPatternStrings);
        if (fileEncoding != null) configuration.setFileEncoding(fileEncoding);

        getLog().info("Parsing project source directory...");
        getLog().debug("File encoding: " + configuration.getFileEncoding());
        getLog().debug("Current directory: " + new File("").getAbsolutePath());
        Project javaProject = Project.create(this.project.getName(), this.project.getDescription(), configuration
            .getFileEncoding(), sourceDirectory);

        getLog().info("Creating report model...");
        RestDocReport report = RestDocReport.create(javaProject, configuration);

        getLog().info("Finished report with title '" + report.getTitle() + "'.");
    }

    @Override
    protected void executeReport(final Locale locale) throws MavenReportException {
        try {
            this.execute();
        } catch (MojoExecutionException e) {
            getLog().error(PRODUCT_NAME + " report generation failed.", e);
            return;
        }
    }

}
