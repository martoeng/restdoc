package com.martoeng.restdoc.util;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 * Utility class for various parser related helper methods.
 * 
 * @author WalterM
 * 
 */
public final class ParserUtils {

    /** Pattern for multi-line comments **/
    private static final Pattern COMMENT_PATTERN = Pattern.compile("\\s*\\**\\s*(.*)\\s*\\**\\s*");
    /** Line terminator String used for the String.split function. **/
    private static final String LINE_TERMINATOR_REGEX = "\r?\n";

    private ParserUtils() {}

    /**
     * Takes a list coming from the Japa parser and returns it null-safe.
     * 
     * @param list
     *        The list that may be {@code null}.
     * @param <T>
     *        The type of the list (given and returned).
     * @return Either the given list or an empty list if the list was {@code null}.
     */
    public static <T> List<T> safeList(final List<T> list) {
        if (list != null) return list;
        return Collections.emptyList();
    }

    /**
     * Parses the comments and removes all unnecessary whitespace and asterisks.
     * 
     * @param comments
     *        The comments String.
     * @return An array of type String containing the parsed comment line by line.
     */
    public static String[] parseComments(final String comments) {
        String[] sourceLines = comments.split(LINE_TERMINATOR_REGEX);
        int lineCount = 0;
        for (int i = 0; i < sourceLines.length; i++) {
            Matcher matcher = COMMENT_PATTERN.matcher(sourceLines[i]);
            if (StringUtils.isNotBlank(sourceLines[i]) && matcher.matches()) {
                sourceLines[i] = matcher.group(1);
            }
            if (StringUtils.isNotBlank(sourceLines[i])) {
                lineCount++;
            }
        }

        int j = 0;
        String[] targetLines = new String[lineCount];
        for (String line : sourceLines) {
            if (StringUtils.isNotBlank(line)) {
                targetLines[j] = line;
                j++;
            }
        }

        return targetLines;
    }

    /**
     * Extracts the package name of a fully qualified class name expression.
     * 
     * @param fullClassName
     *        The name of the class, e.g. "javax.ws.rs.GET".
     * @return The package name, e.g. "javax.ws.rs"
     */
    public static String extractPackage(final String fullClassName) {
        if (StringUtils.isBlank(fullClassName)) return "";
        int dotPos = fullClassName.lastIndexOf('.');
        if (dotPos > 0) {
            return fullClassName.substring(0, dotPos);
        } else {
            return "";
        }
    }

    /**
     * Builds a path from multiple path segments. The algorithm ensures that no double slashes reside in the resulting
     * String.
     * 
     * @param pathSegments
     *        A String array containing the segments.
     * @return A path built from the segments.
     */
    public static String buildPath(final String... pathSegments) {
        if (pathSegments == null || pathSegments.length == 0) return "";
        int i = 0;
        for (String s : pathSegments) {
            i += s.length();
        }
        StringBuilder builder = new StringBuilder(i);
        builder.append(pathSegments[0]);
        for (i = 1; i < pathSegments.length; i++) {
            if (builder.charAt(builder.length() - 1) == '/') {
                if (pathSegments[i].length() > 0 && pathSegments[i].charAt(0) == '/') {
                    builder.append(pathSegments[i].subSequence(1, pathSegments[i].length()));
                } else {
                    builder.append(pathSegments[i]);
                }
            } else {
                if (pathSegments[i].length() > 0 && pathSegments[i].charAt(0) != '/') {
                    builder.append('/');
                    builder.append(pathSegments[i]);
                } else {
                    builder.append(pathSegments[i]);
                }
            }
        }
        return builder.toString();
    }

}
