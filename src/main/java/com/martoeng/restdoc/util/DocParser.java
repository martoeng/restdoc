package com.martoeng.restdoc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import com.martoeng.restdoc.model.ExpressionResolver;
import com.martoeng.restdoc.model.MethodType;
import com.martoeng.restdoc.model.project.JavaAnnotation;
import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.JavaVariable;
import com.martoeng.restdoc.model.project.Project;

/**
 * Utility class for parsing Javadoc and annotation parameters.
 * 
 * @author WalterM
 * 
 */
public final class DocParser {

    /** The name of the status capturing group. **/
    public static final String STATUS_CAPTURING_GROUP_NAME = "status";
    /** The name of the description capturing group. **/
    public static final String DESCRIPTION_CAPTURING_GROUP_NAME = "description";
    /** A String array for the return patterns. **/
    public static final String[] RETURN_PATTERNS = {
        DocParser.STATUS_PATTERN + "\\W*" + DocParser.DESCRIPTION_PATTERN + "[.!]",
        DocParser.DESCRIPTION_PATTERN + "\\s" + DocParser.STATUS_PATTERN + "\\sis\\sreturned[.!]",
        DocParser.DESCRIPTION_PATTERN + "\\sis\\s" + DocParser.STATUS_PATTERN + "[.!]"
    };
    private static final String STATUS_PATTERN = "(?<" + STATUS_CAPTURING_GROUP_NAME + ">\\w+|\\d{3,3})";
    private static final String DESCRIPTION_PATTERN = "(?<" + DESCRIPTION_CAPTURING_GROUP_NAME + ">[^.!]+)";

    /** The Javadoc param tag. **/
    private static final String PARAM_TAG = "@param";
    /** The Javadoc return tag. **/
    private static final String RETURN_TAG = "@return";


    private DocParser() {}


    /**
     * Retrieves the name of a parameter by looking at the name and the annotations.
     * 
     * @param parameter
     *        The {@link JavaVariable parameter} to analyze.
     * @param cls
     *        The {@link JavaClass class} in which the parameter is used.
     * @param project
     *        The {@link Project} in which the class is contained.
     * @return The name of the parameter. If a name cannot be reconstructed, "&lt;unnamedParameter&gt;" is returned.
     */
    public static String parseParamName(final JavaVariable parameter, final JavaClass cls, final Project project) {
        if (StringUtils.isNotEmpty(parameter.getName())) {
            return parameter.getName();
        }

        for (JavaAnnotation annotation : parameter.getAnnotations()) {
            switch (annotation.getName()) {
            case "PathParam":
            case "QueryParam":
            case "FormParam":
            case "HeaderParam":
            case "CookieParam":
                return ExpressionResolver.resolveToString(annotation.getParameter(JavaAnnotation.VALUE), cls, project);
            }
        }

        return "<unnamedParameter>";
    }

    /**
     * Parses the annotation list for annotations that denote a certain path.
     * 
     * @param annotations
     *        The list of {@link JavaAnnotation JavaAnnotations}.
     * @param cls
     *        The class that contains the annotation list (either itself or a method that does).
     * @param project
     *        The project in which the class is contained.
     * @return A String that represents the resolved path, "/" otherwise.
     */
    public static String parsePath(final List<JavaAnnotation> annotations, final JavaClass cls, final Project project) {
        String value = null;
        for (JavaAnnotation annotation : annotations) {
            switch (annotation.getName()) {
            case JavaAnnotation.JAXRS_PATH:
                if (annotation.hasParameter(JavaAnnotation.VALUE)) value = ExpressionResolver.resolveToString(
                    annotation.getParameter(JavaAnnotation.VALUE), cls,
                    project);
                break;
            case JavaAnnotation.SPRING_REQUEST_MAPPING:
                if (annotation.hasParameter(JavaAnnotation.VALUE)) value = ExpressionResolver.resolveToString(
                    annotation.getParameter(JavaAnnotation.VALUE), cls,
                    project);
                break;
            }
        }

        if (value != null) {
            // append and prepend a slash
            value = value.startsWith("/") ? (value.endsWith("/") ? value : value + "/") : "/"
                + (value.endsWith("/") ? value : value + "/");
            return value.equals("//") ? "/" : value;
        } else {
            return "";
        }
    }

    /**
     * Parses the comments and collects the text that does not belong to a javadoc (or other) tag.
     * 
     * @param comments
     *        The comments to parse. Can be {@code null}.
     * @param ignoredComments
     *        Pattern array that describes comments to be ignored.
     * @return The parsed description text
     */
    public static String parseDescription(final String[] comments, final Pattern[] ignoredComments) {
        if (comments == null || comments.length == 0) return "";
        StringBuilder builder = new StringBuilder(256);
        for (String s : comments) {
            if (s.startsWith("@")) {
                break;
            }
            if (matchesAnyPattern(s, ignoredComments) == false) builder.append(DocParser.parseJavaDocTags(s));
        }

        return builder.toString();
    }

    /**
     * Parses the comments of a method to retrieve the description for a specific parameter.
     * 
     * @param comments
     *        The String array containing the method comments.
     * @param paramName
     *        The name of the parameter.
     * @param ignoredComments
     *        Pattern array that describes comments to be ignored.
     * 
     * @return The parsed description or an empty String if no description could be found.
     */
    public static String parseParamDescription(final String[] comments, final String paramName, final Pattern[] ignoredComments) {
        if (comments == null || comments.length == 0) return "";
        for (int i = 0; i < comments.length; i++) {
            if (comments[i].startsWith(DocParser.PARAM_TAG)) {
                String line = StringUtils.trim(comments[i].substring(DocParser.PARAM_TAG.length()));
                if (line.startsWith(paramName)) {
                    StringBuilder builder = new StringBuilder(256);
                    builder.append(DocParser.parseJavaDocTags(line.substring(paramName.length()).trim()));
                    for (int j = i + 1; j < comments.length; j++) {
                        if (comments[j].startsWith("@")) break;
                        if (builder.length() > 0) builder.append(' ');
                        if (matchesAnyPattern(comments[j], ignoredComments) == false) {
                            builder.append(DocParser.parseJavaDocTags(comments[j].trim()));
                        }
                    }
                    return builder.toString();
                }
            }
        }

        return "";
    }

    /**
     * Parses the comments of a method to retrieve the description of the return value.
     * 
     * @param comments
     *        The String array containing the method comments.
     * @param ignoredComments
     *        Pattern array that describes comments to be ignored.
     * @return The parsed return value description or an empty String if noe description could be found.
     */
    public static String parseReturnDescription(final String[] comments, final Pattern[] ignoredComments) {
        if (comments == null || comments.length == 0) return "";
        for (int i = 0; i < comments.length; i++) {
            if (comments[i].startsWith(RETURN_TAG)) {
                StringBuilder builder = new StringBuilder(256);
                builder.append(DocParser.parseJavaDocTags(StringUtils.trim(comments[i].substring(RETURN_TAG.length()))));
                for (int j = i + 1; j < comments.length; j++) {
                    if (comments[j].startsWith("@")) break;
                    if (matchesAnyPattern(comments[j], ignoredComments) == false) {
                        builder.append(DocParser.parseJavaDocTags(comments[j].trim()));
                    }
                }
                return builder.toString();
            }
        }

        return "";
    }

    /**
     * Parses the annotations on a method to retrieve the offered HTTP methods.
     * 
     * @param annotations
     *        The annotations set on the method.
     * @param cls
     *        The {@link JavaClass class} which contains the method.
     * @param project
     *        The {@link Project} in which the class is contained.
     * @return A {@link List} containing all HTTP methods found. The returned set may be empty.
     */
    public static List<MethodType> parseMethodTypes(final List<JavaAnnotation> annotations, final JavaClass cls,
        final Project project) {
        Set<MethodType> methodTypes = new HashSet<>();
        for (JavaAnnotation annotation : annotations) {
            switch (annotation.getName()) {
            case JavaAnnotation.JAXRS_GET:
            case JavaAnnotation.JAXRS_POST:
            case JavaAnnotation.JAXRS_PUT:
            case JavaAnnotation.JAXRS_DELETE:
            case JavaAnnotation.JAXRS_OPTIONS:
            case JavaAnnotation.JAXRS_HEAD:
                methodTypes.add(MethodType.parse(annotation.getName()));
                break;
            case JavaAnnotation.SPRING_REQUEST_MAPPING:
                if (annotation.hasParameter(JavaAnnotation.SPRING_REQMAP_METHOD)) {
                    for (String methodName : ExpressionResolver.resolveToStringArray(annotation
                        .getParameter(JavaAnnotation.SPRING_REQMAP_METHOD), cls, project)) {
                        methodTypes.add(MethodType.parse(methodName));
                    }
                } else {
                    methodTypes.add(MethodType.GET);
                }
                break;
            }
        }
        List<MethodType> methodTypeList = new ArrayList<MethodType>(methodTypes);
        Collections.sort(methodTypeList);
        return methodTypeList;
    }

    /**
     * Parses a method's or class' annotations and returns the {@link Set} of consumed media types.
     * 
     * @param annotations
     *        The list of {@link JavaAnnotation annotations} of the Java object.
     * @param cls
     *        The {@link JavaClass class} to be used for expression resolution.
     * @param project
     *        The {@link Project} in which the class is contained.
     * @return A {@link Set} containing all media types found. The returned Set may be empty.
     */
    public static Set<String> parseConsumedMediaTypes(final List<JavaAnnotation> annotations, final JavaClass cls,
        final Project project) {
        Set<String> mediaTypes = new HashSet<>();
        for (JavaAnnotation annotation : annotations) {
            switch (annotation.getName()) {
            case JavaAnnotation.JAXRS_CONSUMES:
                DocParser.parseMediaTypes(annotation, JavaAnnotation.VALUE, cls, project, mediaTypes);
                break;
            case JavaAnnotation.SPRING_REQUEST_MAPPING:
                DocParser.parseMediaTypes(annotation, "consumes", cls, project, mediaTypes);
                break;
            }
        }

        return mediaTypes;
    }

    /**
     * Parses a method's or class' annotations and returns the {@link Set} of produced media types.
     * 
     * @param annotations
     *        The list of {@link JavaAnnotation annotations} of the Java object.
     * @param cls
     *        The {@link JavaClass class} to be used for expression resolution.
     * @param project
     *        The {@link Project} in which the class is contained.
     * @return A {@link Set} containing all media types found. The returned Set may be empty.
     */
    public static Set<String> parseProducedMediaTypes(final List<JavaAnnotation> annotations, final JavaClass cls,
        final Project project) {
        Set<String> mediaTypes = new HashSet<>();
        for (JavaAnnotation annotation : annotations) {
            switch (annotation.getName()) {
            case JavaAnnotation.JAXRS_PRODUCES:
                DocParser.parseMediaTypes(annotation, JavaAnnotation.VALUE, cls, project, mediaTypes);
                break;
            case JavaAnnotation.SPRING_REQUEST_MAPPING:
                DocParser.parseMediaTypes(annotation, JavaAnnotation.SPRING_REQMAP_PRODUCES, cls, project, mediaTypes);
                break;
            }
        }

        return mediaTypes;
    }

    /**
     * Checks the annotation for a specific parameter and eventually adds the media type found within to a given set.
     * 
     * @param annotation
     *        The annotation to check.
     * @param parameterName
     *        The name of the annotation parameter.
     * @param cls
     *        The {@link JavaClass class} in which the annotation resides.
     * @param project
     *        The {@link Project} in which the class is contained.
     * @param mediaTypes
     *        The {@link Set} of media types.
     */
    private static void parseMediaTypes(final JavaAnnotation annotation, final String parameterName, final JavaClass cls,
        final Project project, final Set<String> mediaTypes) {
        if (annotation.hasParameter(parameterName)) {
            for (String s : ExpressionResolver.resolveToStringArray(annotation.getParameter(parameterName), cls, project)) {
                mediaTypes.add(s);
            }
        }
    }

    /**
     * 
     * @param string
     *        The String that may contain JavaDoc tags that should be replaced.
     * @return A {@link CharSequence} that is either a new sequence with replacements or the given input String.
     */
    public static CharSequence parseJavaDocTags(final String string) {
        int pos = string.indexOf("{@");
        // no tag to replace, return the string as-is
        if (pos == -1) {
            return string;
        }
        StringBuilder sb = new StringBuilder(string.length());
        int end = -1;
        while (pos != -1) {
            sb.append(string.substring(end + 1, pos));
            // look for closing tag bracket
            end = string.indexOf('}', pos + 2);
            if (end == -1) break;

            int tagNameEnd = string.indexOf(' ', pos + 2);
            if (tagNameEnd == -1) tagNameEnd = end;
            switch (string.substring(pos + 2, tagNameEnd)) {
            case "code":
                sb.append("<code>");
                sb.append(string.substring(tagNameEnd + 1, end));
                sb.append("</code>");
                break;
            case "docRoot":
                // do nothing
                break;
            case "inheritDoc":
                sb.append("See parent method.");
                break;
            case "link":
            case "linkplain":
                int space = string.indexOf(' ', tagNameEnd + 1);
                if (space != -1 && space < end) {
                    // space within tag
                    sb.append(string.substring(space + 1, end));
                } else {
                    // no space within tag
                    sb.append(string.substring(tagNameEnd + 1, end));
                }
                break;
            case "value":
                sb.append("value of ");
                sb.append(string.substring(tagNameEnd + 1, end));
                break;
            case "literal":
                sb.append(string.substring(tagNameEnd + 1, end));
                break;
            }

            // find new tag
            pos = string.indexOf("{@", end);
        }
        sb.append(string.substring(end + 1));
        return sb;
    }

    /**
     * Matches a String against an array of patterns.
     * 
     * @param string
     *        The String to match.
     * @param patterns
     *        An array of patterns.
     * @return {@code true} if any of the given patterns matches the String, {@code false} otherwise.
     */
    public static boolean matchesAnyPattern(final String string, final Pattern[] patterns) {
        if (patterns == null || patterns.length == 0) return false;
        for (Pattern pattern : patterns) {
            if (pattern.matcher(string).matches()) return true;
        }
        return false;
    }
}
