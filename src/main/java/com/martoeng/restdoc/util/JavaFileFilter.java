package com.martoeng.restdoc.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Simple Java file filter matching all .java files.
 * 
 * @author WalterM
 * 
 */
public class JavaFileFilter implements FileFilter {

    private static final JavaFileFilter INSTANCE = new JavaFileFilter();

    @Override
    public boolean accept(final File pathname) {
        return (pathname.isDirectory() == false && pathname.getName().toLowerCase().endsWith(".java"));
    }

    /** @return A JavaFileFilter instance. **/
    public static JavaFileFilter getInstance() {
        return INSTANCE;
    }

}
