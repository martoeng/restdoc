package com.martoeng.restdoc.util;

/**
 * Class that wraps the {@link AddRemoveObserver} interface and provides a default implementation for each method.
 * 
 * @author WalterM
 * 
 * @param <T>
 *        The type of the observed class.
 */
public class AddRemoveObserverAdapter<T> implements AddRemoveObserver<T> {

    @Override
    public T beforeAdded(final T item) {
        return item;
    }

    @Override
    public void afterAdded(final T item) {}

    @Override
    public boolean beforeRemoved(final T item) {
        return true;
    }

    @Override
    public void afterRemoved(final T item) {}

}
