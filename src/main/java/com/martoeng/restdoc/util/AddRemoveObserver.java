package com.martoeng.restdoc.util;

/**
 * Interface for an observer for addition or removal of a certain list object.
 * 
 * @author WalterM
 * 
 * @param <T>
 *        The type of the list object.
 */
public interface AddRemoveObserver<T> {

    /**
     * Called before an item is added. An observer can modify the object.
     * 
     * @param item
     *        The item that shall be added.
     * @return The item to be added. This can be the original item that was passed as a parameter or a new object or a
     *         wrapper around the original item.
     */
    T beforeAdded(T item);

    /**
     * Called after an item has been added.
     * 
     * @param item
     *        The item that was added. An observer can modify this object but cannot replace it anymore.
     */
    void afterAdded(T item);

    /**
     * Called before an item is removed from a list.
     * 
     * @param item
     *        The item that shall be removed.
     * @return {@code true} if the observer agrees that the item should be removed, {@code false} otherwise.
     */
    boolean beforeRemoved(T item);

    /**
     * Called after an item has been removed.
     * 
     * @param item
     *        The item that was removed and remains no longer in the list. Any reference to this object by the observer
     *        should be removed as well.
     */
    void afterRemoved(T item);

}
