package com.martoeng.restdoc.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Simple file filter matching only directories.
 * 
 * @author WalterM
 * 
 */
public class DirectoryFilter implements FileFilter {

    private static final DirectoryFilter INSTANCE = new DirectoryFilter();

    @Override
    public boolean accept(final File pathname) {
        return pathname.isDirectory();
    }

    /** @return A DirectoryFilter instance. **/
    public static DirectoryFilter getInstance() {
        return INSTANCE;
    }

}
