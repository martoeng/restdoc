package com.martoeng.restdoc.printer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import org.apache.commons.lang.StringEscapeUtils;
import com.martoeng.restdoc.model.RestDocReport;

/**
 * Abstract baseclass for HTML printers with useful functions.
 * 
 * @author walterm
 * 
 */
public abstract class AbstractHtmlPrinter {

    protected static final String CSS_CLASS = "class";
    protected static final String ID = "id";
    protected static final String TAG_PARAGRAPH = "p";
    protected static final String TAG_SPAN = "span";
    protected static final String TAG_DIV = "div";
    protected static final String TAG_TABLE = "table";
    protected static final String TAG_TABLE_ROW = "tr";
    protected static final String TAG_TABLE_CELL = "td";
    protected static final String TAG_TABLE_HEAD = "thead";
    protected static final String TAG_TABLE_HEAD_CELL = "th";
    protected static final String TAG_TABLE_BODY = "tbody";
    protected static final String TAG_ANCHOR = "a";
    protected static final String TAG_HR = "hr";
    protected static final String TAG_LI = "li";

    /**
     * Prints a headline.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param level
     *        The heading level (1 to 7).
     * @param headline
     *        The text of the headline
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void printHeader(final Writer fw, final int level, final String headline) throws IOException {
        writeHtmlTag(fw, "h" + String.valueOf(level), headline);
    }

    /**
     * Prints the HTML file header including document type.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param cssFile
     *        The name of the main css file to link to.
     * @param title
     *        The title of the page.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void printHtmlHeader(final Writer fw, final String cssFile, final String title) throws IOException {
        printHtmlHeader(fw, cssFile, title, (String[]) null);
    }

    /**
     * Prints the HTML file header including document type.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param cssFile
     *        The name of the main css file to link to.
     * @param title
     *        The title of the page.
     * @param jsFiles
     *        A String array containing the names of Javascript files.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void printHtmlHeader(final Writer fw, final String cssFile, final String title, final String... jsFiles)
        throws IOException {
        fw.write("<!DOCTYPE html>");
        lineFeed(fw);
        openHtmlTag(fw, "html");
        lineFeed(fw);
        openHtmlTag(fw, "head");
        lineFeed(fw);
        writeHtmlTag(fw, "title", title);
        lineFeed(fw);
        writeHtmlTag(fw, "link", null, "rel", "stylesheet", "type", "text/css", "href", cssFile);
        if (jsFiles != null) {
            for (String jsFile : jsFiles) {
                lineFeed(fw);
                writeHtmlTag(fw, "script", "", "src", jsFile);
            }
        }
        lineFeed(fw);
        closeHtmlTag(fw, "head");
        openHtmlTag(fw, "body");
        lineFeed(fw);
        writeHtmlTag(fw, "h1", title);
        lineFeed(fw);
    }

    /**
     * Prints a line feed. For an HTML line break, see {@link #lineBreak(Writer)}.
     * 
     * @param fw
     *        The {@link Writer} to use.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void lineFeed(final Writer fw) throws IOException {
        fw.write('\n');
    }

    /**
     * Prints an HTML line break tag.
     * 
     * @param fw
     *        The {@link Writer} to use.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void lineBreak(final Writer fw) throws IOException {
        fw.write("<br/>");
        lineFeed(fw);
    }

    /**
     * Prints the HTML file footer, closing the document structurally.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param report
     *        The {@link RestDocReport} to use.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void printFooter(final Writer fw, final RestDocReport report) throws IOException {
        closeHtmlTag(fw, "body");
        closeHtmlTag(fw, "html");
    }

    /**
     * Opens an HTML tag.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param tag
     *        The HTML tag name to open.
     * @param attributeNamesAndValues
     *        A String array containing attribute names and values. On every even index an attribute name is assumed, on
     *        every odd index the value for the preceding attribute is assumed.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void openHtmlTag(final Writer fw, final String tag, final String... attributeNamesAndValues) throws IOException {
        printOpeningHtmlTag(fw, tag, attributeNamesAndValues);
        fw.write('>');
    }

    private void printOpeningHtmlTag(final Writer fw, final String tag, final String... attributeNamesAndValues)
        throws IOException {
        fw.write('<');
        fw.write(tag);
        if (attributeNamesAndValues != null && attributeNamesAndValues.length > 0) {
            for (int i = 0; i < attributeNamesAndValues.length; i += 2) {
                fw.write(' ');
                fw.write(attributeNamesAndValues[i]);
                fw.write("=\"");
                fw.write(attributeNamesAndValues[i + 1]);
                fw.write('\"');
            }
        }
    }

    /**
     * Closes an HTML tag.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param tag
     *        The name of the tag.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void closeHtmlTag(final Writer fw, final String tag) throws IOException {
        fw.write("</");
        fw.write(tag);
        fw.write('>');
        lineFeed(fw);
    }

    /**
     * Writes an HTML tag, opening it, writing content and closing it.
     * 
     * @param fw
     *        The {@link Writer} to print to.
     * @param tag
     *        The name of the tag.
     * @param content
     *        The content that will be printed within the tag. If {@code null}, the tag is closed directly (for example
     *        {@code <img .../>}).
     * @param attributeNamesAndValues
     *        A String array containing attribute names and values. On every even index an attribute name is assumed, on
     *        every odd index the value for the preceding attribute is assumed.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void writeHtmlTag(final Writer fw, final String tag, final String content, final String... attributeNamesAndValues)
        throws IOException {

        if (content != null) {
            openHtmlTag(fw, tag, attributeNamesAndValues);
            write(fw, content);
            closeHtmlTag(fw, tag);
        } else {
            printOpeningHtmlTag(fw, tag, attributeNamesAndValues);
            fw.write("/>");
        }
    }

    /**
     * Prints a String conforming to HTML.
     * 
     * @param fw
     *        The Writer to use.
     * @param string
     *        The String to print. If special characters are used they are converted to HTML entities.
     * @throws IOException
     *         Thrown on IO problems.
     */
    protected void write(final Writer fw, final String string) throws IOException {
        StringEscapeUtils.escapeHtml(fw, string);
    }


    /**
     * Copies a file to the output directory.
     * 
     * @param outputDirectory
     *        The target directory.
     * @param resourcePath
     *        The path of the resource.
     * @param resourceName
     *        The name of the resource file.
     */
    protected void copyFileToOutputFolder(final File outputDirectory, final String resourcePath, final String resourceName) {
        try (InputStream inputStream = this.getClass().getResourceAsStream(resourcePath + resourceName);
            OutputStream outputStream = new FileOutputStream(new File(outputDirectory, resourceName))) {

            int readBytes = 0;
            byte[] buffer = new byte[2048];
            while ((readBytes = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, readBytes);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
