package com.martoeng.restdoc.printer;

import java.io.File;
import com.martoeng.restdoc.model.RestDocReport;

/**
 * Interface for all printers that output the {@link RestDocReport}.
 * 
 * @author walterm
 * 
 */
public interface RestDocPrinter {

    /**
     * Prints the report.
     * 
     * @param outputDirectory
     *        The directory where the report files will go into.
     * @param report
     *        The {@link RestDocReport report} that shall be printed.
     */
    void print(File outputDirectory, RestDocReport report);

}
