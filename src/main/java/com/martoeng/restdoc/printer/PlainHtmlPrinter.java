package com.martoeng.restdoc.printer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import com.martoeng.restdoc.model.Method;
import com.martoeng.restdoc.model.MethodType;
import com.martoeng.restdoc.model.Parameter;
import com.martoeng.restdoc.model.Resource;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.ReturnValue;
import com.martoeng.restdoc.util.ParserUtils;

/**
 * Printer class that outputs a static HTML report.
 * 
 * @author walterm
 * 
 */
public class PlainHtmlPrinter extends AbstractHtmlPrinter implements RestDocPrinter {

    private static final String DESCRIPTION = "Description";
    private static final String HEADING_CSS_CLASS = "heading";
    private static final String STD_CSS_FILENAME = "plainhtmlstdreport.css";
    /** Configuration option whether to use a single file or one file per entity and resource. **/
    private final boolean singleFile;

    /**
     * Constructs a new plain HTML printer. By default, it will use a single output file.
     */
    public PlainHtmlPrinter() {
        singleFile = true;
    }

    @Override
    public void print(final File outputDirectory, final RestDocReport report) {
        // copy css to location
        copyFileToOutputFolder(outputDirectory, "/report/plainhtml/", STD_CSS_FILENAME);

        // generate report
        try {
            FileWriter fw = new FileWriter(new File(outputDirectory, report.getTitle() + "_plain.html"));
            printHtmlHeader(fw, STD_CSS_FILENAME, report.getTitle());
            printDescription(fw, report.getDescription());

            // generate overview table
            generateOverviewTable(fw, report);
            printHr(fw);

            for (Resource resource : report.getResources()) {
                printAnchor(fw, buildResourceLinkName(resource));
                printHeader(fw, 2, resource.getName());
                openHtmlTag(fw, TAG_DIV, CSS_CLASS, "resourcedescription");
                if (resource.getBasePath().equals("/") == false) printBasePath(fw, resource.getBasePath());
                printDescription(fw, resource.getDescription());
                printMediaTypes(fw, "Consumed media types: ", resource.getConsumedMediaTypes());
                printMediaTypes(fw, "Produced media types: ", resource.getProducedMediaTypes());
                closeHtmlTag(fw, TAG_DIV);
                for (Method method : resource.getMethods()) {
                    printAnchor(fw, buildMethodLinkName(method));
                    printHeader(fw, 3, method.getName());
                    openHtmlTag(fw, TAG_DIV, CSS_CLASS, "methoddescription");
                    printPath(fw, method.getMethodTypes(), method.getPath());
                    printDescription(fw, method.getDescription());
                    printHeader(fw, 4, "Return values");
                    printReturnValues(fw, method.getReturnValues());
                    printMediaTypes(fw, "Consumed media types: ", method.getConsumedMediaTypes());
                    printMediaTypes(fw, "Produced media types: ", method.getProducedMediaTypes());
                    if (method.getParameterCount() > 0) {
                        printHeader(fw, 4, "Parameters");
                        printParameters(fw, method.getParameters());
                    }
                    closeHtmlTag(fw, TAG_DIV);
                    printHr(fw);
                }
                printHr(fw);
            }

            printFooter(fw, report);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateOverviewTable(final Writer fw, final RestDocReport report) throws IOException {
        // table header
        openHtmlTag(fw, TAG_TABLE, "id", "overview");
        openHtmlTag(fw, TAG_TABLE_HEAD);
        openHtmlTag(fw, TAG_TABLE_ROW, CSS_CLASS, HEADING_CSS_CLASS);
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Name");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Path");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, DESCRIPTION);
        closeHtmlTag(fw, TAG_TABLE_ROW);
        closeHtmlTag(fw, TAG_TABLE_HEAD);

        // entries
        openHtmlTag(fw, TAG_TABLE_BODY);
        for (Resource resource : report.getResources()) {
            openHtmlTag(fw, TAG_TABLE_ROW);
            openHtmlTag(fw, TAG_TABLE_CELL);
            printAnchorLink(fw, resource);
            closeHtmlTag(fw, TAG_TABLE_CELL);
            writeHtmlTag(fw, TAG_TABLE_CELL, resource.getBasePath());
            writeHtmlTag(fw, TAG_TABLE_CELL, resource.getDescription());
            closeHtmlTag(fw, TAG_TABLE_ROW);

            for (Method method : resource.getMethods()) {
                openHtmlTag(fw, TAG_TABLE_ROW);
                openHtmlTag(fw, TAG_TABLE_CELL);
                printAnchorLink(fw, method, resource.getName() + "#" + method.getName());
                closeHtmlTag(fw, TAG_TABLE_CELL);
                writeHtmlTag(fw, TAG_TABLE_CELL, ParserUtils.buildPath(resource.getBasePath(), method.getPath()));
                writeHtmlTag(fw, TAG_TABLE_CELL, method.getDescription());
                closeHtmlTag(fw, TAG_TABLE_ROW);
            }
        }
        closeHtmlTag(fw, TAG_TABLE_BODY);

        closeHtmlTag(fw, TAG_TABLE);
    }

    private void printAnchor(final Writer fw, final String anchorName) throws IOException {
        writeHtmlTag(fw, TAG_ANCHOR, null, "id", anchorName);
    }

    private void printAnchorLink(final Writer fw, final Resource resource) throws IOException {
        if (isSingleFile()) {
            writeHtmlTag(fw, TAG_ANCHOR, resource.getName(), "href", "#" + buildResourceLinkName(resource));
        }
    }

    private void printAnchorLink(final Writer fw, final Method method, final String text) throws IOException {
        if (isSingleFile()) {
            writeHtmlTag(fw, TAG_ANCHOR, text != null ? text : method.getName(), "href", "#" + buildMethodLinkName(method));
        }
    }

    private String buildMethodLinkName(final Method method) {
        return method.getResource().getName() + "_" + method.getName();
    }

    private String buildResourceLinkName(final Resource resource) {
        return resource.getName();
    }

    private void printMediaTypes(final Writer fw, final String mediaTypesName, final Collection<String> mediaTypes)
        throws IOException {
        if (mediaTypes.isEmpty()) return;
        openHtmlTag(fw, TAG_PARAGRAPH, CSS_CLASS, "mediatypes");
        fw.write(mediaTypesName);
        fw.write(": ");
        for (String mediaType : mediaTypes) {
            writeHtmlTag(fw, TAG_SPAN, mediaType, CSS_CLASS, "mediatype");
            fw.write(" ");
        }
        closeHtmlTag(fw, TAG_PARAGRAPH);
    }

    private void printDescription(final Writer fw, final String description) throws IOException {
        writeHtmlTag(fw, TAG_PARAGRAPH, description, CSS_CLASS, "description");
    }

    private void printPath(final Writer fw, final List<MethodType> methods, final String path) throws IOException {
        openHtmlTag(fw, TAG_PARAGRAPH, CSS_CLASS, "path");
        if (methods.size() == 1) {
            fw.write(methods.iterator().next().toString());
            fw.write(": ");
        } else {
            StringBuilder builder = new StringBuilder(32);
            for (Iterator<MethodType> iterator = methods.iterator(); iterator.hasNext();) {
                builder.append(iterator.next().toString());
                if (iterator.hasNext()) builder.append(", ");
            }
            fw.write(builder.toString());
            fw.write(": ");
        }
        writeHtmlTag(fw, TAG_SPAN, path, CSS_CLASS, "path");
        closeHtmlTag(fw, TAG_PARAGRAPH);
    }

    private void printBasePath(final Writer fw, final String basePath) throws IOException {
        openHtmlTag(fw, TAG_PARAGRAPH, CSS_CLASS, "basepath");
        fw.write("Basepath: ");
        writeHtmlTag(fw, TAG_SPAN, basePath, CSS_CLASS, "basepath");
        closeHtmlTag(fw, TAG_PARAGRAPH);
    }

    private void printHr(final Writer fw) throws IOException {
        writeHtmlTag(fw, TAG_HR, null);
    }

    private void printReturnValues(final Writer fw, final Collection<ReturnValue> returnValues) throws IOException {
        if (returnValues.isEmpty()) return;
        openHtmlTag(fw, TAG_TABLE, CSS_CLASS, "returnvalues");
        openHtmlTag(fw, TAG_TABLE_HEAD);
        openHtmlTag(fw, TAG_TABLE_ROW, CSS_CLASS, HEADING_CSS_CLASS);
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Code", CSS_CLASS, "returncode");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Status", CSS_CLASS, "returnstatus");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, DESCRIPTION, CSS_CLASS, "returncondition");
        closeHtmlTag(fw, TAG_TABLE_ROW);
        closeHtmlTag(fw, TAG_TABLE_HEAD);

        // iterate over return values
        openHtmlTag(fw, TAG_TABLE_BODY);
        for (ReturnValue returnValue : returnValues) {
            openHtmlTag(fw, TAG_TABLE_ROW);
            writeHtmlTag(fw, TAG_TABLE_CELL, String.valueOf(returnValue.getStatusCode().getStatus()), CSS_CLASS, "returncode");
            writeHtmlTag(fw, TAG_TABLE_CELL, returnValue.getStatusCode().getName(), CSS_CLASS, "returnstatus");
            writeHtmlTag(fw, TAG_TABLE_CELL, returnValue.getDescription(), CSS_CLASS, "returncondition");
            closeHtmlTag(fw, TAG_TABLE_ROW);
        }
        closeHtmlTag(fw, TAG_TABLE_BODY);

        closeHtmlTag(fw, TAG_TABLE);
    }

    private void printParameters(final Writer fw, final Collection<Parameter> parameters) throws IOException {
        if (parameters.isEmpty()) return;
        openHtmlTag(fw, TAG_TABLE, CSS_CLASS, "parameters");
        openHtmlTag(fw, TAG_TABLE_HEAD);
        openHtmlTag(fw, TAG_TABLE_ROW, CSS_CLASS, HEADING_CSS_CLASS);
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Name", CSS_CLASS, "paramname");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, "Type", CSS_CLASS, "paramtype");
        writeHtmlTag(fw, TAG_TABLE_HEAD_CELL, DESCRIPTION, CSS_CLASS, "paramdescription");
        closeHtmlTag(fw, TAG_TABLE_ROW);
        closeHtmlTag(fw, TAG_TABLE_HEAD);

        openHtmlTag(fw, TAG_TABLE_BODY);
        for (Parameter parameter : parameters) {
            openHtmlTag(fw, TAG_TABLE_ROW);
            writeHtmlTag(fw, TAG_TABLE_CELL, parameter.getName(), CSS_CLASS, "paramname");
            writeHtmlTag(fw, TAG_TABLE_CELL, parameter.getType().toString(), CSS_CLASS, "paramtype");
            writeHtmlTag(fw, TAG_TABLE_CELL, parameter.getDescription(), CSS_CLASS, "paramdescription");
            closeHtmlTag(fw, TAG_TABLE_ROW);
        }
        closeHtmlTag(fw, TAG_TABLE_BODY);

        closeHtmlTag(fw, TAG_TABLE);
    }

    /** @return {@code true} if the report should reside in a single HTML file, {@code false} otherwise. **/
    public boolean isSingleFile() {
        return this.singleFile;
    }

}
