package com.martoeng.restdoc.printer;

import com.martoeng.restdoc.model.Entity;
import com.martoeng.restdoc.model.Method;
import com.martoeng.restdoc.model.MethodType;
import com.martoeng.restdoc.model.Parameter;
import com.martoeng.restdoc.model.Resource;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.ReturnValue;
import com.martoeng.restdoc.model.ValueRestriction;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Printer class that outputs a nicely formatted static HTMl report.
 * 
 * @author walterm
 * 
 */
public class NiceHtmlPrinter extends AbstractHtmlPrinter implements RestDocPrinter {

    private static final String LABEL_COLLAPSE = "Collapse";


    private static final String COLLAPSE = "collapse";


    private static final String HASH = "#";
    private static final String HREF = "href";
    private static final String HIERARCHY_SEPARATOR = "_";
    private static final String DIV_RESOURCE_CONTENT = "divResourceContent";
    private static final String DIV_ENTITY_CONTENT = "divEntityContent";
    private static final String DIV_METHOD_CONTENT = "divMethodContent";
    private static final String LINK_ID_COLLAPSE = "linkCollapse";
    private static final String LINK_ID_COLLAPSE_ALL_METHODS = "linkCollapseMethods";

    private static final String CSS_FILENAME = "nicehtml.css";
    private static final String JQUERY_FILENAME = "jquery-2.1.1.min.js";
    private static final String RESTDOC_SCRIPT_FILENAME = "_restdoc.js";

    private static final String RESOURCE_PATH = "/report/nicehtml/";

    private static final String JQUERY_SELECT_ID = "$(\"#";
    private static final String JQUERY_FUNCTION_END = "});\n";
    private static final String JQUERY_CLICK = "\").click(function(){\n";


    @Override
    public void print(final File outputDirectory, final RestDocReport report) {
        // copy all necessary files to the output directory
        copyFileToOutputFolder(outputDirectory, RESOURCE_PATH, CSS_FILENAME);
        copyFileToOutputFolder(outputDirectory, RESOURCE_PATH, JQUERY_FILENAME);

        // print HTMl file
        try (FileWriter fw = new FileWriter(new File(outputDirectory, report.getTitle() + "_nice.html"))) {
            printHtmlHeader(fw, CSS_FILENAME, report.getTitle(), JQUERY_FILENAME, report.getTitle() + RESTDOC_SCRIPT_FILENAME);
            printDescription(fw, report.getDescription());

            // print resources/controllers
            for (Resource resource : report.getResources()) {
                printResource(fw, resource);
            }

            if (report.getEntities().isEmpty() == false) {
                printVerticalSeparator(fw);
            }

            // print entities
            for (Entity entity : report.getEntities()) {
                printEntity(fw, entity);
            }

            printFooter(fw, report);
        } catch (IOException e) {
            e.printStackTrace();
        }

        printJavaScriptFile(outputDirectory, report);
    }

    private void printVerticalSeparator(final FileWriter fw) throws IOException {
        writeHtmlTag(fw, TAG_HR, null, CSS_CLASS, "section");
    }

    private void printJavaScriptFile(final File outputDirectory, final RestDocReport report) {
        try (FileWriter fw = new FileWriter(new File(outputDirectory, report.getTitle() + RESTDOC_SCRIPT_FILENAME))) {
            fw.write("\"use strict\";\n");
            fw.write("$(document).ready( function() {\n");
            for (Resource resource : report.getResources()) {
                // "Expand resource" link
                fw.write(JQUERY_SELECT_ID + LINK_ID_COLLAPSE + resource.getName() + JQUERY_CLICK);
                fw.write(JQUERY_SELECT_ID + DIV_RESOURCE_CONTENT + resource.getName() + "\").toggle(500);");
                fw.write("$(this).text($(this).text() == \"Collapse\" ? \"Expand\" : \"Collapse\");");
                fw.write(JQUERY_FUNCTION_END);
                // "Expand methods" link
                fw.write(JQUERY_SELECT_ID + LINK_ID_COLLAPSE_ALL_METHODS + resource.getName() + JQUERY_CLICK);
                fw.write("var m = null;\n");
                fw.write("var l = " + JQUERY_SELECT_ID + LINK_ID_COLLAPSE_ALL_METHODS + resource.getName() + "\");");
                for (Method method : resource.getMethods()) {
                    fw.write("m = " + JQUERY_SELECT_ID + LINK_ID_COLLAPSE + getMethodId(resource, method) + "\");");
                    fw.write("if(l.text()==\"Expand methods\"){\n");
                    fw.write("if(m.text()==\"Expand\") {m.click();}\n");
                    fw.write("}else{\n");
                    fw.write("if(m.text()==\"Collapse\") {m.click();}\n");
                    fw.write("}\n");
                }
                fw.write("l.text(l.text()==\"Expand methods\" ? \"Collapse methods\" : \"Expand methods\");");
                fw.write(JQUERY_FUNCTION_END);
                // print "Expand method" link
                for (Method method : resource.getMethods()) {
                    fw.write(JQUERY_SELECT_ID + LINK_ID_COLLAPSE + getMethodId(resource, method) + JQUERY_CLICK);
                    fw.write(JQUERY_SELECT_ID + DIV_METHOD_CONTENT + getMethodId(resource, method) + "\").toggle(500);\n");
                    fw.write("$(this).text($(this).text() == \"Collapse\" ? \"Expand\" : \"Collapse\");");
                    fw.write(JQUERY_FUNCTION_END);
                }
            }
            // collapse all methods
            for (Resource resource : report.getResources()) {
                fw.write(JQUERY_SELECT_ID + LINK_ID_COLLAPSE_ALL_METHODS + resource.getName() + "\").click();");
            }
            fw.write("});");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printDescription(final Writer fw, final String description) throws IOException {
        if (description != null && description.length() > 0) {
            writeHtmlTag(fw, "p", description, "class", "description");
        }
    }

    private void printResource(final Writer fw, final Resource resource) throws IOException {
        openHtmlTag(fw, TAG_DIV, CSS_CLASS, "resource", ID, "divResource" + resource.getName());
        printHeader(fw, 2, resource.getName());
        writeHtmlTag(fw, TAG_ANCHOR, LABEL_COLLAPSE, CSS_CLASS, COLLAPSE, ID, LINK_ID_COLLAPSE + resource.getName(), HREF, HASH);
        writeHtmlTag(fw, TAG_ANCHOR, "Collapse methods", CSS_CLASS, COLLAPSE, ID, LINK_ID_COLLAPSE_ALL_METHODS
            + resource.getName(), HREF, HASH);
        lineBreak(fw);
        openHtmlTag(fw, TAG_DIV, ID, DIV_RESOURCE_CONTENT + resource.getName());
        printBasePath(fw, resource.getBasePath());
        printDescription(fw, resource.getDescription());
        for (Method method : resource.getMethods()) {
            printMethod(fw, resource, method);
        }
        closeHtmlTag(fw, TAG_DIV);
        closeHtmlTag(fw, TAG_DIV);
    }

    private void printEntity(final FileWriter fw, final Entity entity) throws IOException {
        String fullEntityName = entity.getCanonicalName().replace('.', '_');
        openHtmlTag(fw, TAG_DIV, CSS_CLASS, "entity", ID, "divEntity" + fullEntityName);
        printHeader(fw, 2, entity.getSimpleName());
        writeHtmlTag(fw, TAG_ANCHOR, LABEL_COLLAPSE, CSS_CLASS, COLLAPSE, ID, LINK_ID_COLLAPSE + fullEntityName, HREF, HASH);
        lineBreak(fw);
        openHtmlTag(fw, TAG_DIV, ID, DIV_ENTITY_CONTENT + fullEntityName);
        printPackage(fw, entity.getPackageName());
        printDescription(fw, entity.getDescription());
        closeHtmlTag(fw, TAG_DIV);
        closeHtmlTag(fw, TAG_DIV);
    }

    private void printMethod(final Writer fw, final Resource resource, final Method method) throws IOException {
        openHtmlTag(fw, TAG_LI, CSS_CLASS, "method " + method.getMethodTypes().get(0).name().toLowerCase());
        for (MethodType methodType : method.getMethodTypes()) {
            writeHtmlTag(fw, TAG_SPAN, methodType.name(), CSS_CLASS, "methodtype " + methodType.name().toLowerCase());
        }
        printHeader(fw, 3, method.getPath());
        writeHtmlTag(fw, TAG_ANCHOR, LABEL_COLLAPSE, ID, LINK_ID_COLLAPSE + getMethodId(resource, method), HREF, HASH, CSS_CLASS,
            COLLAPSE);
        writeHtmlTag(fw, TAG_SPAN, getFirstSentence(method.getDescription()), CSS_CLASS, "shortdescription");
        openHtmlTag(fw, TAG_DIV, ID, DIV_METHOD_CONTENT + getMethodId(resource, method));
        printDescription(fw, method.getDescription());
        lineBreak(fw);
        if (method.getParameterCount() > 0) {
            printHeader(fw, 4, "Parameters");
            openHtmlTag(fw, TAG_TABLE, CSS_CLASS, "parameters");
            openHtmlTag(fw, TAG_TABLE_BODY);
            for (Parameter parameter : method.getParameters()) {
                openHtmlTag(fw, TAG_TABLE_ROW);
                writeHtmlTag(fw, TAG_TABLE_CELL, parameter.getName(), CSS_CLASS, "paramname");
                writeHtmlTag(fw, TAG_TABLE_CELL, parameter.getType().toString(), CSS_CLASS, "paramtype");
                openHtmlTag(fw, TAG_TABLE_CELL, CSS_CLASS, "paramdescription");
                fw.write(parameter.getDescription());
                if (parameter.hasRestrictions()) {
                    lineBreak(fw);
                    for (ValueRestriction restriction : parameter.getRestrictions()) {
                        printRestriction(fw, parameter, restriction);
                        lineBreak(fw);
                    }
                }
                closeHtmlTag(fw, TAG_TABLE_CELL);
                closeHtmlTag(fw, TAG_TABLE_ROW);
            }
            closeHtmlTag(fw, TAG_TABLE_BODY);
            closeHtmlTag(fw, TAG_TABLE);
        } else {
            printHeader(fw, 4, "No Parameters.");
        }
        lineBreak(fw);
        printHeader(fw, 4, "Return values");
        openHtmlTag(fw, TAG_TABLE, CSS_CLASS, "returnvalues");
        openHtmlTag(fw, TAG_TABLE_BODY);
        for (ReturnValue returnValue : method.getReturnValues()) {
            openHtmlTag(fw, TAG_TABLE_ROW);
            writeHtmlTag(fw, TAG_TABLE_CELL, returnValue.getStatusCode().getName(), "title", "Status code is "
                + String.valueOf(returnValue.getStatusCode().getStatus()), CSS_CLASS, "returncode");
            writeHtmlTag(fw, TAG_TABLE_CELL, returnValue.getDescription());
            closeHtmlTag(fw, TAG_TABLE_ROW);
        }
        closeHtmlTag(fw, TAG_TABLE_BODY);
        closeHtmlTag(fw, TAG_TABLE);

        closeHtmlTag(fw, TAG_DIV);
        closeHtmlTag(fw, TAG_LI);
    }

    private void printRestriction(final Writer fw, final Parameter parameter, final ValueRestriction restriction)
        throws IOException {
        fw.write(restriction.getDescription(parameter.getName()));
        fw.write('.');
    }

    private void printBasePath(final Writer fw, final String basepath) throws IOException {
        if (basepath.equals("/") == false) {
            writeHtmlTag(fw, TAG_PARAGRAPH, "Basepath: " + basepath, CSS_CLASS, "basepath");
        }
    }

    private void printPackage(final FileWriter fw, final String packageName) throws IOException {
        writeHtmlTag(fw, TAG_PARAGRAPH, "Package: " + packageName, CSS_CLASS, "basepath");
    }

    private String getMethodId(final Resource resource, final Method method) {
        return resource.getName() + HIERARCHY_SEPARATOR + method.getName();
    }

    private String getFirstSentence(final String text) {
        if (text != null && text.isEmpty() == false) {
            int pos = Math.min(text.indexOf('.'), text.indexOf('!'));
            while (pos != -1) {
                if (pos < text.length() - 1 && Character.isWhitespace(text.charAt(pos + 1))) {
                    return text.substring(0, pos + 1);
                }
                pos = Math.min(text.indexOf('.', pos + 1), text.indexOf('!', pos + 1));
            }
            return text;
        } else {
            return "";
        }
    }
}
