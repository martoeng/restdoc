package com.martoeng.restdoc.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents an enumeration.
 * 
 * @author walterm
 *
 */
public class Enumeration implements Iterable<com.martoeng.restdoc.model.Enumeration.Entry> {

    private final String simpleName;
    private final String packageName;
    /** The enumeration entries. **/
    private final List<Entry> entries;


    /**
     * Constructs a new Enumeration.
     * 
     * @param simpleName
     *        The simple class name of the enumeration.
     * @param packageName
     *        The full package name of the enumeration.
     * **/
    public Enumeration(final String simpleName, final String packageName) {
        this.simpleName = simpleName;
        this.packageName = packageName;
        this.entries = new ArrayList<>();
    }

    /**
     * Adds an enumeration entry.
     * 
     * @param name
     *        The name of the enum constant.
     * @param description
     *        The description of the enum constant.
     */
    public void addEntry(final String name, final String description) {
        this.entries.add(new Entry(name, description));
    }

    /** @return The simple class name of the enumeration. **/
    public String getSimpleName() {
        return simpleName;
    }

    /** @return The full name of the package. **/
    public String getPackageName() {
        return packageName;
    }

    @Override
    public Iterator<com.martoeng.restdoc.model.Enumeration.Entry> iterator() {
        return this.entries.iterator();
    }


    /**
     * Represents a single enumeration entry.
     * 
     * @author walterm
     *
     */
    class Entry {

        private final String name;
        private final String description;


        public Entry(final String name, final String description) {
            this.name = name;
            this.description = description;
        }

        /** @return The name of the enum constant. **/
        public String getName() {
            return name;
        }

        /** @return The description of the enum constant. **/
        public String getDescription() {
            return description;
        }

    }

}
