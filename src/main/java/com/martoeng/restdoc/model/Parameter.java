package com.martoeng.restdoc.model;

import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * Represents a parameter.
 * 
 * @author WalterM
 * 
 */
public class Parameter {

    private final DataType type;
    private final String name;
    private final String description;
    private final List<ValueRestriction> restrictions;


    /**
     * Constructs a new {@link Parameter}.
     * 
     * @param type
     *        The {@link DataType type} of the parameter.
     * @param name
     *        The name of the parameter (if known).
     * @param description
     *        The description of the parameter.
     * @param restrictions
     *        The restrictions that may be defined for the parameter.
     */
    protected Parameter(final DataType type, final String name, final String description,
        final List<ValueRestriction> restrictions) {
        this.type = type;
        this.name = name;
        this.description = description;
        this.restrictions = restrictions;
    }

    /** @return The type of the parameter. Can be an integer, float, ... **/
    public DataType getType() {
        return this.type;
    }

    /** @return The name of the parameter (if known). **/
    public String getName() {
        return this.name;
    }

    /** @return {@code true} if the parameter is named and {@link #getName()} will return it, {@code false} otherwise. **/
    public boolean hasName() {
        return StringUtils.isNotEmpty(this.name);
    }

    /** @return The description for this parameter. **/
    public String getDescription() {
        return this.description;
    }

    /**
     * @return {@code true} if the parameter has a description and {@link #getDescription()} will return it,
     *         {@code false} otherwise.
     **/
    public boolean hasDescription() {
        return StringUtils.isNotBlank(this.description);
    }

    /** @return {@code true} if the parameter's value is somehow restricted, {@code false} otherwise. **/
    public boolean hasRestrictions() {
        return this.restrictions.size() > 0;
    }

    /** @return A {@link List} of {@link ValueRestriction value restrictions} like min or max value. **/
    public List<ValueRestriction> getRestrictions() {
        return this.restrictions;
    }

    @Override
    public String toString() {
        return getName() + " (" + getType() + ")";
    }

}
