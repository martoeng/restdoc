package com.martoeng.restdoc.model.project;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Representation of a Java annotation.
 * 
 * @author walterm
 * 
 */
public class JavaAnnotation {

    /** The name of the Spring request mapping annotation. **/
    public static final String SPRING_REQUEST_MAPPING = "RequestMapping";
    /** Method parameter of the Spring request mapping annotation. **/
    public static final String SPRING_REQMAP_METHOD = "method";
    /** Produces parameter of the Spring request mapping annotation. **/
    public static final String SPRING_REQMAP_PRODUCES = "produces";

    /** The name of the Jax-rs annotation for a path. **/
    public static final String JAXRS_PATH = "Path";
    /** The name of the Jax-rs annotation for HTTP GET. **/
    public static final String JAXRS_GET = "GET";
    /** The name of the Jax-rs annotation for HTTP POST. **/
    public static final String JAXRS_POST = "POST";
    /** The name of the Jax-rs annotation for HTTP PUT. **/
    public static final String JAXRS_PUT = "PUT";
    /** The name of the Jax-rs annotation for HTTP DELETE. **/
    public static final String JAXRS_DELETE = "DELETE";
    /** The name of the Jax-rs annotation for HTTP HEAD. **/
    public static final String JAXRS_HEAD = "HEAD";
    /** The name of the Jax-rs annotation for HTTP OPTIONS. **/
    public static final String JAXRS_OPTIONS = "OPTIONS";
    /** The name of the media type consumption annotation. **/
    public static final String JAXRS_CONSUMES = "Consumes";
    /** The name of the media type production annotation. **/
    public static final String JAXRS_PRODUCES = "Produces";

    /** The name of the default attribute of an annotation. **/
    public static final String VALUE = "value";


    private static final String[] INHERITED_ANNOTATIONS = {
        JAXRS_PATH, JAXRS_GET, JAXRS_POST, JAXRS_PUT, JAXRS_DELETE, JAXRS_HEAD, JAXRS_OPTIONS, SPRING_REQUEST_MAPPING
    };

    private static final Logger LOG = LoggerFactory.getLogger(JavaAnnotation.class);


    private final String name;
    private Map<String, Expression> parameters;


    /**
     * Constructs a new {@link JavaAnnotation}.
     * 
     * @param annotation
     *        The parser's {@link AnnotationExpr annotation expression}.
     */
    protected JavaAnnotation(final AnnotationExpr annotation) {
        this.name = annotation.getName().getName();
        List<Node> nodes = annotation.getChildrenNodes();
        nodes.remove(0); // remove NameExpr
        if (nodes.size() == 0) {
            this.parameters = Collections.emptyMap();
        } else if (nodes.size() == 1 && !(nodes.get(0) instanceof MemberValuePair)) {
            this.parameters = new HashMap<>(1);
            this.parameters.put("value", (Expression) nodes.get(0));
        } else {
            this.parameters = new HashMap<>(nodes.size());
            for (Node node : nodes) {
                MemberValuePair mvp = (MemberValuePair) node;
                this.parameters.put(mvp.getName(), mvp.getValue());
            }
        }
    }

    /**
     * Constructs a new {@link JavaAnnotation}.
     * 
     * @param annotation
     *        The Java reflection {@link Annotation}.
     */
    protected JavaAnnotation(final Annotation annotation) {
        this.name = annotation.annotationType().getSimpleName();
        this.parameters = new HashMap<>(3);
        for (Method m : annotation.annotationType().getDeclaredMethods()) {
            m.setAccessible(true);
            Object value = null;
            try {
                value = m.invoke(annotation);
                if (value.getClass().isArray()) {
                    int size = Array.getLength(value);
                    List<Expression> values = new ArrayList<Expression>(size);
                    for (int i = 0; i < size; i++) {
                        Object obj = Array.get(value, i);
                        values.add(new StringLiteralExpr(obj != null ? obj.toString() : ""));
                    }
                    this.parameters.put(m.getName(), new ArrayInitializerExpr(values));
                } else {
                    this.parameters.put(m.getName(), new StringLiteralExpr(value != null ? value.toString() : ""));
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                LOG.warn("Could not access annotation value " + m.getName() + " for "
                    + annotation.annotationType().getSimpleName());
            }
        }
    }

    /** @return The name of the annotation, e.g. "GET". **/
    public String getName() {
        return this.name;
    }

    /**
     * Returns the value of the annotation parameter.
     * 
     * @param parameterName
     *        The name of the parameter.
     * @return An {@link Expression} defining the value for this annotation parameter.
     */
    public Expression getParameter(final String parameterName) {
        return this.parameters.get(parameterName);
    }

    /** @return A {@link Set} of type String with the names of all annotation parameters. **/
    public Set<String> getParameterNames() {
        return this.parameters.keySet();
    }

    /** @return The number of parameters this annotation has. **/
    public int getParameterCount() {
        return this.parameters.size();
    }

    /** @return {@code true}, if the parameter count is at least one, {@code false} otherwise. **/
    public boolean hasParameters() {
        return this.parameters.size() > 0;
    }

    /**
     * Returns whether a given parameter is set.
     * 
     * @param parameterName
     *        The name of the parameter.
     * @return {@code true}, if the parameter is set, {@code false} otherwise.
     */
    public boolean hasParameter(final String parameterName) {
        return this.parameters.containsKey(parameterName);
    }

    /** @return A {@link Set} of type {@link Entry} that contains all parameters with their keys and values. **/
    public Set<Entry<String, Expression>> getParameters() {
        return this.parameters.entrySet();
    }

    @Override
    public String toString() {
        return this.getName() + "(parameters=" + this.parameters.toString() + ")";
    }

    /**
     * Creates a list of type {@link JavaAnnotation} from a given list of Japa annotations.
     * 
     * @param annotationExpressions
     *        The parser's Java annotations.
     * @return A {@link List} of type {@link JavaAnnotation} containing the converted annotations.
     */
    protected static List<JavaAnnotation> createList(final List<AnnotationExpr> annotationExpressions) {
        // if size is 0, return a new empty array list as there might be dependencies adding annotations later
        if (annotationExpressions == null || annotationExpressions.size() == 0) return new ArrayList<>(0);
        List<JavaAnnotation> javaAnnotations = new ArrayList<>(annotationExpressions.size());
        for (AnnotationExpr expr : annotationExpressions) {
            javaAnnotations.add(new JavaAnnotation(expr));
        }
        return javaAnnotations;
    }

    /**
     * Creates a list of type {@link JavaAnnotation} from a given array of Java reflection annotations.
     * 
     * @param annotations
     *        The annotations.
     * @return A {@link List} of type {@link JavaAnnotation} containing the converted annotations.
     */
    protected static List<JavaAnnotation> createList(final Annotation[] annotations) {
        List<JavaAnnotation> javaAnnotations = new ArrayList<>(annotations.length);
        for (Annotation annotation : annotations) {
            javaAnnotations.add(new JavaAnnotation(annotation));
        }
        return javaAnnotations;
    }

    /**
     * Returns whether the specified annotation is inherited in sub-classes or not.
     * 
     * @param annotationName
     *        The name of the annotation, e.g. "GET".
     * @return {@code true} if the annotation is inherited, {@code false} otherwise.
     */
    public static boolean isAnnotationInherited(final String annotationName) {
        for (String name : INHERITED_ANNOTATIONS) {
            if (name.equals(annotationName)) return true;
        }
        return false;
    }

}
