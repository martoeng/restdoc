package com.martoeng.restdoc.model.project;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.util.LinkedList;
import java.util.List;

/**
 * Collector for methods.
 * 
 * @author walterm
 * 
 */
class MethodCollector extends VoidVisitorAdapter<List<JavaMethod>> {

    private final TypeDeclaration typeDeclaration;

    protected MethodCollector(final TypeDeclaration typeDeclaration) {
        this.typeDeclaration = typeDeclaration;
    }


    protected List<JavaMethod> collect() {
        List<JavaMethod> methods = new LinkedList<>();
        this.typeDeclaration.accept(this, methods);

        return methods;
    }

    @Override
    public void visit(final MethodDeclaration method, final List<JavaMethod> methods) {
        methods.add(JavaMethod.create(method));
        super.visit(method, methods);
    }
}
