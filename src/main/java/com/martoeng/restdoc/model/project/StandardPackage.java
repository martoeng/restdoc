package com.martoeng.restdoc.model.project;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.martoeng.restdoc.util.AddRemoveObserver;

/**
 * Standard implementation of the {@link Package} interface.
 * 
 * @author walterm
 * 
 */
public class StandardPackage implements Package {

    private final List<AddRemoveObserver<Package>> packageObservers;
    private final List<AddRemoveObserver<JavaClass>> classObservers;
    private final List<Package> packages;
    private final List<JavaClass> classes;
    private final String name;
    private String path;
    private final Package parentPackage;

    /**
     * Creates a new {@link StandardPackage}.
     * 
     * @param name
     *        The name of the package. May be blank for the root package.
     * @param parent
     *        The parent package, may be {@code null} for the root package.
     */
    protected StandardPackage(final String name, final Package parent) {
        this.packages = new ArrayList<>();
        this.classes = new ArrayList<>();
        this.packageObservers = new LinkedList<>();
        this.classObservers = new LinkedList<>();
        this.name = name;
        this.parentPackage = parent;
        if (parent != null && StringUtils.isNotBlank(parent.getAbsoluteName())) {
            path = parent.getAbsoluteName();
        } else {
            path = "";
        }
    }

    @Override
    public Iterable<Package> getPackageIterable() {
        return this.packages;
    }

    @Override
    public Iterable<JavaClass> getJavaClassIterable() {
        return this.classes;
    }

    @Override
    public Package getParent() {
        return this.parentPackage;
    }

    @Override
    public void addPackage(final Package pkg) {
        // Notify observers for classes before the class is added
        Package p = pkg;
        for (AddRemoveObserver<Package> observer : this.packageObservers) {
            p = observer.beforeAdded(p);
            // if an observer returns null, don't add it
            if (p == null) return;
        }

        this.packages.add(p);

        // Notify all observers after the class has been added
        for (AddRemoveObserver<Package> observer : this.packageObservers) {
            observer.afterAdded(p);
        }
    }

    @Override
    public void removePackage(final Package pkg) {
        // Notify all observers for classes before the class is removed
        for (AddRemoveObserver<Package> observer : this.packageObservers) {
            if (observer.beforeRemoved(pkg) == false) return;
        }

        this.packages.remove(pkg);

        // Notify all observers after the class has been removed
        for (AddRemoveObserver<Package> observer : this.packageObservers) {
            observer.afterRemoved(pkg);
        }
    }

    @Override
    public void addClass(final JavaClass javaClass) {
        // Notify observers for classes before the class is added
        JavaClass cls = javaClass;
        for (AddRemoveObserver<JavaClass> observer : this.classObservers) {
            cls = observer.beforeAdded(cls);
            // if an observer returns null, don't add it
            if (cls == null) return;
        }

        this.classes.add(cls);

        // Notify all observers after the class has been added
        for (AddRemoveObserver<JavaClass> observer : this.classObservers) {
            observer.afterAdded(cls);
        }
    }

    @Override
    public void removeClass(final JavaClass javaClass) {
        // Notify all observers for classes before the class is removed
        for (AddRemoveObserver<JavaClass> observer : this.classObservers) {
            if (observer.beforeRemoved(javaClass) == false) return;
        }

        this.classes.remove(javaClass);

        // Notify all observers after the class has been removed
        for (AddRemoveObserver<JavaClass> observer : this.classObservers) {
            observer.afterRemoved(javaClass);
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void addJavaClassObserver(final AddRemoveObserver<JavaClass> observer) {
        this.classObservers.add(observer);
    }

    @Override
    public void addPackageObserver(final AddRemoveObserver<Package> observer) {
        this.packageObservers.add(observer);
    }

    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public String getAbsoluteName() {
        if (StringUtils.isNotBlank(getPath())) {
            return getPath() + "." + getName();
        } else {
            return getName();
        }
    }

    @Override
    public int getClassCount() {
        return this.classes.size();
    }

    @Override
    public int getTotalClassCount() {
        int amount = this.getClassCount();
        for (Package pkg : this.packages) {
            amount += pkg.getTotalClassCount();
        }
        return amount;
    }

    @Override
    public Package buildPath(final String packagePath) {
        if (StringUtils.isBlank(packagePath)) return this;
        int dotPos = packagePath.indexOf('.');
        if (dotPos == -1) dotPos = packagePath.length();
        String packageName = packagePath.substring(0, dotPos);
        // look for an existing package
        for (Package pkg : this.packages) {
            if (pkg.getName().equals(packageName)) {
                if (dotPos + 1 > packagePath.length()) return pkg;
                return pkg.buildPath(packagePath.substring(dotPos + 1));
            }
        }
        // package does not yet exist
        Package pkg = new StandardPackage(packageName, this);
        this.addPackage(pkg);
        return pkg.buildPath(packagePath.substring(dotPos < packagePath.length() ? dotPos + 1 : dotPos));
    }

    @Override
    public String toString() {
        return this.getAbsoluteName();
    }
}
