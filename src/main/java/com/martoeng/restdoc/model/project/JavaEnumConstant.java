package com.martoeng.restdoc.model.project;

/**
 * Represents a Java enum constant.
 * 
 * @author walterm
 *
 */
public class JavaEnumConstant {

    private String name;
    private String[] comments;

    /**
     * Constructs a new Java enum constant.
     * 
     * @param name
     *        The name of the constant.
     */
    protected JavaEnumConstant(final String name) {
        this.name = name;
    }

    /**
     * Sets the comments of this constant.
     * 
     * @param comments
     *        The raw comments represented by a String array.
     */
    protected void setComments(final String[] comments) {
        this.comments = comments;
    }

    /** @return The name of the enum constant. **/
    public String getName() {
        return this.name;
    }

    /** @return The raw comments of the enum constant represented by a String array. **/
    public String[] getComments() {
        return this.comments;
    }

}
