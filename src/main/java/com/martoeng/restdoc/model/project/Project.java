package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.util.AddRemoveObserver;
import com.martoeng.restdoc.util.AddRemoveObserverAdapter;
import com.martoeng.restdoc.util.DirectoryFilter;
import com.martoeng.restdoc.util.JavaFileFilter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a Java project.
 * 
 * @author walterm
 * 
 */
public final class Project {

    private static final Logger LOG = LoggerFactory.getLogger(Project.class);

    private final Package pkg;
    private final String fileEncoding;
    private final String title;
    private final String description;
    private final Map<String, JavaClass> classMap;
    private final Map<String, Package> packageMap;
    private final AddRemoveObserver<JavaClass> javaClassObserver;
    private final AddRemoveObserver<Package> packageObserver;


    private Project(final String title, final String description, final String fileEncoding) {
        this.title = title;
        this.description = description;
        this.fileEncoding = fileEncoding;
        this.pkg = new StandardPackage("", null);
        this.javaClassObserver = new JavaClassObserver();
        this.packageObserver = new PackageObserver();
        this.pkg.addJavaClassObserver(this.javaClassObserver);
        this.pkg.addPackageObserver(this.packageObserver);
        this.classMap = new ConcurrentHashMap<>();
        this.packageMap = new HashMap<>();
    }

    /** @return The title of the project. **/
    public String getTitle() {
        return this.title;
    }

    /** @return The description of the project. **/
    public String getDescription() {
        return this.description;
    }

    /** @return The virtual root package for the project being above the first real code package. **/
    public Package getProjectPackage() {
        return this.pkg;
    }

    /** @return The name of the encoding of the source files. **/
    public String getFileEncoding() {
        return this.fileEncoding;
    }

    /**
     * Returns a package for the given path name.
     * 
     * @param fullPathName
     *        The path name, e.g. "com.test.project".
     * @return Either a {@link Package} or {@code null} if the package is not registered.
     */
    public Package getPackage(final String fullPathName) {
        return this.packageMap.get(fullPathName);
    }

    /**
     * Returns a {@link JavaClass} for the given path name.
     * 
     * @param fullPathName
     *        The path name, e.g. "com.test.project.TestClass".
     * @return Either a {@link JavaClass} or {@code null} if the class is not registered.
     */
    public JavaClass getJavaClass(final String fullPathName) {
        return this.classMap.get(fullPathName);
    }

    /**
     * Returns whether a certain class is registered.
     * 
     * @param fullPathName
     *        The path name, e.g. "com.test.project.TestClass".
     * @return {@code true} if the class is registered, {@code false} otherwise.
     */
    public boolean containsJavaClass(final String fullPathName) {
        return this.classMap.containsKey(fullPathName);
    }

    /** @return An {@link Iterator} for all {@link JavaClass} objects in the project. **/
    public Iterator<JavaClass> getJavaClassIterator() {
        return new JavaClassIterator(this.classMap.entrySet());
    }

    private void registerClass(final JavaClass javaClass) {
        this.classMap.put(javaClass.getPackage().getAbsoluteName() + "." + javaClass.getName(), javaClass);
    }

    private void registerPackage(final Package newPackage) {
        if (this.packageMap.containsKey(newPackage.getAbsoluteName()) == false) {
            this.packageMap.put(newPackage.getAbsoluteName(), newPackage);
        }
    }

    /**
     * Creates a new Project.
     * 
     * @param title
     *        The title of the project.
     * @param description
     *        A short introductory description of the project.
     * @param fileEncoding
     *        The name of the encoding of the source files.
     * @param sourceDirectories
     *        The main source directory or various directories.
     * @return A {@link Project} that contains all {@link JavaClass} objects, sorted by their {@link Package}.
     */
    public static Project create(final String title, final String description, final String fileEncoding,
        final File... sourceDirectories) {
        Project project = new Project(title, description, fileEncoding);
        for (File directory : sourceDirectories) {
            if (directory.isDirectory()) scanFoldersRecursively(directory, fileEncoding, project.getProjectPackage());
        }

        for (Iterator<JavaClass> classIterator = project.getJavaClassIterator(); classIterator.hasNext();) {
            JavaClass cls = classIterator.next();
            JavaClass.resolveDependencies(cls, project);
        }

        for (Iterator<JavaClass> classIterator = project.getJavaClassIterator(); classIterator.hasNext();) {
            JavaClass cls = classIterator.next();
            for (JavaImport javaImport : cls.getImports()) {
                if (project.containsJavaClass(javaImport.getFullName()) == false) {
                    Package pkg = project.getProjectPackage().buildPath(javaImport.getPath());
                    try {
                        project.registerClass(JavaClass.create(Class.forName(javaImport.getFullName()), pkg));
                    } catch (ClassNotFoundException e) {
                        // TODO: set to info level
                        LOG.debug("Could not find class definition for {}", javaImport.getFullName());
                    }
                }
            }
        }


        return project;
    }

    private static void scanFoldersRecursively(final File directory, final String fileEncoding, final Package pkg) {
        // Scan for Java files
        for (File file : directory.listFiles(JavaFileFilter.getInstance())) {
            JavaClass.create(file, fileEncoding, pkg);
        }

        // Scan for sub-packages
        for (File dir : directory.listFiles(DirectoryFilter.getInstance())) {
            StandardPackage sp = new StandardPackage(dir.getName(), pkg);
            pkg.addPackage(sp);
            scanFoldersRecursively(dir, fileEncoding, sp);
        }
    }


    /***********************************************************/
    /** Observe changes in package and class structure. **/

    /**
     * Small observer helper class for any created {@link JavaClass classes}.
     * 
     * @author walterm
     * 
     */
    class JavaClassObserver extends AddRemoveObserverAdapter<JavaClass> {

        @Override
        public void afterAdded(final JavaClass item) {
            registerClass(item);
        }

    }

    /**
     * Small observer helper class to register any created sub-package.
     * 
     * @author walterm
     * 
     */
    class PackageObserver extends AddRemoveObserverAdapter<Package> {

        @Override
        public void afterAdded(final Package item) {
            item.addJavaClassObserver(Project.this.javaClassObserver);
            item.addPackageObserver(Project.this.packageObserver);
            registerPackage(item);
        }

    }

    /**
     * An {@link Iterator} for {@link JavaClass} objects. This iterator wraps an entry-set.
     * 
     * @author walterm
     * 
     */
    final class JavaClassIterator implements Iterator<JavaClass> {

        private final Iterator<Entry<String, JavaClass>> entrySetIterator;


        private JavaClassIterator(final Set<Entry<String, JavaClass>> entrySet) {
            this.entrySetIterator = entrySet.iterator();
        }

        @Override
        public boolean hasNext() {
            return this.entrySetIterator.hasNext();
        }

        @Override
        public JavaClass next() {
            return this.entrySetIterator.next().getValue();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**********************************************************/
}
