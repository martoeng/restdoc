package com.martoeng.restdoc.model.project;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.martoeng.restdoc.model.ExpressionResolver;
import com.martoeng.restdoc.util.ParserUtils;

/**
 * Collector for all classes the given class depends on.
 * 
 * @author walterm
 * 
 */
public class DependenciesCollector extends VoidVisitorAdapter<List<JavaClass>> {

    private static final Logger LOG = LoggerFactory.getLogger(DependenciesCollector.class);

    private final JavaClass javaClass;
    private final Project project;


    /**
     * Constructs a new collector.
     * 
     * @param javaClass
     *        The class whose dependencies shall be collected.
     * @param project
     *        The project the class belongs to.
     */
    protected DependenciesCollector(final JavaClass javaClass, final Project project) {
        this.javaClass = javaClass;
        this.project = project;
    }

    /** @return A {@link List} of {@link JavaClass JavaClasses} that represents the dependencies. **/
    protected List<JavaClass> collect() {
        if (this.javaClass.getTypeDeclaration() == null) {
            return Collections.emptyList();
        }

        LinkedList<JavaClass> dependencies = new LinkedList<>();
        if (this.javaClass.getTypeDeclaration() instanceof ClassOrInterfaceDeclaration) {
            ClassOrInterfaceDeclaration decl = (ClassOrInterfaceDeclaration) this.javaClass.getTypeDeclaration();
            for (ClassOrInterfaceType type : ParserUtils.safeList(decl.getExtends())) {
                collect(type, dependencies);
                this.javaClass.setParentClass(this.project.getJavaClass(ExpressionResolver.resolveName(type.getName(),
                    this.javaClass, this.project)));
            }
            for (ClassOrInterfaceType type : ParserUtils.safeList(decl.getImplements())) {
                collect(type, dependencies);
            }
        } else if (this.javaClass.getTypeDeclaration() instanceof EnumDeclaration) {
            EnumDeclaration decl = (EnumDeclaration) this.javaClass.getTypeDeclaration();
            for (ClassOrInterfaceType type : ParserUtils.safeList(decl.getImplements())) {
                collect(type, dependencies);
            }
        }
        this.javaClass.getTypeDeclaration().accept(this, dependencies);
        return dependencies;
    }

    private void collect(final ClassOrInterfaceType type, final List<JavaClass> dependencies) {
        String fullName = ExpressionResolver.resolveName(type.getName(), this.javaClass, this.project);
        if (this.project.containsJavaClass(fullName)) {
            dependencies.add(this.project.getJavaClass(fullName));
        } else {
            try {
                String packageName = ParserUtils.extractPackage(fullName);
                Package pkg = this.project.getProjectPackage().buildPath(packageName);
                JavaClass jc = JavaClass.create(Class.forName(fullName), pkg);
                jc.setExcludedFromDoc(true);
                dependencies.add(jc);
            } catch (ClassNotFoundException e) {
                LOG.error("Could not instantiate class " + fullName);
            }
        }
    }
}
