package com.martoeng.restdoc.model.project;

import java.util.List;

/**
 * Interface marking another class that handles annotations.
 * 
 * @author walterm
 * 
 */
public interface JavaAnnotated {

    /** @return A {@link List} of type {@link JavaAnnotation} containing all used annotations. **/
    List<JavaAnnotation> getAnnotations();

    /**
     * Adds an annotation to the list of annotations.
     * 
     * @param annotation
     *        The {@link JavaAnnotation} to be added.
     */
    void addAnnotation(JavaAnnotation annotation);

    /**
     * Finds and returns the specified annotation (if present).
     * 
     * @param annotationName
     *        The name of the annotation to retrieve.
     * @return The {@link JavaAnnotation} with the specified name or {@code null} if there is no annotation with this
     *         name.
     */
    JavaAnnotation findAnnotation(final String annotationName);

}
