package com.martoeng.restdoc.model.project;

import java.util.List;

/**
 * Basic class representing an object in the Java hierarchy. Provides some basic functionality.
 * 
 * @author walterm
 * 
 */
public class JavaObject {

    /**
     * Finds and returns the specified annotation (if present).
     * 
     * @param annotations
     *        The list of annotations.
     * @param annotationName
     *        The name of the annotation to retrieve.
     * @return The {@link JavaAnnotation} with the specified name or {@code null} if there is no annotation with this
     *         name.
     */
    protected JavaAnnotation findAnnotation(final List<JavaAnnotation> annotations, final String annotationName) {
        for (JavaAnnotation annotation : annotations) {
            if (annotation.getName().equals(annotationName)) {
                return annotation;
            }
        }
        return null;
    }

}
