package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.model.DataType;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.VariableDeclarator;

/**
 * Represents a parameter, field or return value.
 * 
 * @author walterm
 * 
 */
public class JavaVariable extends JavaObject implements JavaAnnotated {

    private static final Logger LOG = LoggerFactory.getLogger(JavaVariable.class);

    private final String name;
    private final DataType type;
    private List<JavaAnnotation> annotations;
    private Object value;


    /**
     * Constructs a new {@link JavaVariable}.
     * 
     * @param name
     *        The name of the variable, may be {@code null}.
     * @param type
     *        The {@link DataType type} of the variable.
     */
    protected JavaVariable(final String name, final DataType type) {
        this(name, type, null);
    }

    /**
     * Constructs a new {@link JavaVariable}.
     * 
     * @param name
     *        The name of the varaible, may be {@code null}.
     * @param type
     *        The {@link DataType type} of the variable.
     * @param value
     *        The actual value.
     */
    protected JavaVariable(final String name, final DataType type, final Object value) {
        this.name = name;
        this.type = type;
        this.value = value;
    }

    /**
     * @return The name of the variable. May be {@code null} in case of parameters created via reflection or for return
     *         values.
     **/
    public String getName() {
        return this.name;
    }

    /** @return The {@link DataType type} of the variable. **/
    public DataType getType() {
        return this.type;
    }

    @Override
    public List<JavaAnnotation> getAnnotations() {
        return this.annotations;
    }

    @Override
    public JavaAnnotation findAnnotation(final String annotationName) {
        return this.findAnnotation(this.annotations, annotationName);
    }

    @Override
    public void addAnnotation(final JavaAnnotation annotation) {
        this.annotations.add(annotation);
    }

    private void setAnnotations(final List<JavaAnnotation> annotations) {
        this.annotations = annotations;
    }

    /**
     * Sets the (optional) value of the variable.
     * 
     * @param value
     *        The value which might be a parser's expression or a concrete value.
     */
    public void setValue(final Object value) {
        this.value = value;
    }

    /**
     * @return The value of this variable.
     * @see #setValue(Object)
     */
    public Object getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.getName() + "=" + this.value + " (type=" + this.getType().toString() + ", annotations="
            + (annotations != null ? annotations.toString() : "") + ")";
    }

    /**
     * Creates a list of {@link JavaVariable} objects for a parameter list.
     * 
     * @param parameters
     *        The list of {@link Parameter} objects.
     * @return A {@link List} of type {@link JavaVariable} containing all converted parameters.
     */
    protected static List<JavaVariable> createParameterList(final List<Parameter> parameters) {
        if (parameters == null || parameters.size() == 0) return Collections.emptyList();
        List<JavaVariable> variables = new ArrayList<>(parameters.size());
        for (Parameter param : parameters) {
            JavaVariable var = new JavaVariable(param.getId().getName(), DataType.valueOf(param.getType()));
            var.setAnnotations(JavaAnnotation.createList(param.getAnnotations()));
            variables.add(var);
        }
        return variables;
    }

    /**
     * Creates a list of {@link JavaVariable} objects for a parameter list.
     * 
     * @param method
     *        The reflection {@link Method} containing the parameter definitions.
     * @return A {@link List} of type {@link JavaVariable} containing all converted parameters.
     */
    protected static List<JavaVariable> createParameterList(final Method method) {
        Class<?>[] types = method.getParameterTypes();
        List<JavaVariable> variables = new ArrayList<>(types.length);
        for (int i = 0; i < types.length; i++) {
            JavaVariable var = new JavaVariable(null, DataType.valueOf(types[i]));
            var.setAnnotations(JavaAnnotation.createList(method.getParameterAnnotations()[i]));
            variables.add(var);
        }
        return variables;
    }

    /**
     * Creates a list of {@link JavaVariable} objects representing fields from a field declaration.
     * 
     * @param fieldDeclaration
     *        The {@link FieldDeclaration} possibly containing multiple fields.
     * @return A {@link List} of type {@link JavaVariable} containing all converted fields.
     */
    public static List<JavaVariable> create(final FieldDeclaration fieldDeclaration) {
        DataType varType = DataType.valueOf(fieldDeclaration.getType());
        List<VariableDeclarator> variableList = fieldDeclaration.getVariables();
        List<JavaVariable> javaVariables = new ArrayList<>(variableList.size());
        for (VariableDeclarator varDeclarator : variableList) {
            JavaVariable javaVariable = new JavaVariable(varDeclarator.getId().getName(), varType);
            javaVariable.setAnnotations(JavaAnnotation.createList(ParserUtils.safeList(fieldDeclaration.getAnnotations())));
            // set init expression as value to be resolved later
            javaVariable.setValue(varDeclarator.getInit());
            // add the variable to the list
            javaVariables.add(javaVariable);
        }

        return javaVariables;
    }

    /**
     * Creates a {@link JavaVariable} representing a field.
     * 
     * @param field
     *        The Java reflection {@link Field}.
     * @return A {@link JavaVariable} representing the given {@link Field}.
     */
    public static JavaVariable create(final Field field) {
        JavaVariable javaVariable = new JavaVariable(field.getName(), DataType.valueOf(field.getType()));
        javaVariable.setAnnotations(JavaAnnotation.createList(field.getAnnotations()));
        try {
            javaVariable.setValue(field.get(null));
        } catch (IllegalArgumentException | IllegalAccessException e) {
            LOG.warn("Could not retrieve constant '" + field.getName() + "'.", e);
        }
        return javaVariable;
    }

    /**
     * Creates a nameless {@link JavaVariable} from a given value.
     * 
     * @param value
     *        The value to wrap.
     * @return A {@link JavaVariable} object containing the value and a {@link DataType} description.
     */
    public static JavaVariable createFromValue(final Object value) {
        return new JavaVariable(null, DataType.valueOf(value.getClass()), value);
    }

    /**
     * Creates a nameless {@link JavaVariable} from a given class name.
     * 
     * @param typeName
     *        The canonical name of the class.
     * @return A {@link JavaVariable} object containing the {@link DataType} description.
     */
    public static JavaVariable createFromType(final String typeName) {
        Class<?> cls;
        try {
            cls = Class.forName(typeName);
        } catch (ClassNotFoundException e) {
            return new JavaVariable(null, DataType.valueOf(Object.class));
        }
        return new JavaVariable(null, DataType.valueOf(cls));
    }

    /** @return The String representation of the value or {@code null} if the value is {@code null}. **/
    public String getValueAsString() {
        if (value != null) {
            return value.toString();
        } else {
            if (this.type != null) {
                return this.type.toString();
            }
            return null;
        }
    }

    /**
     * Performs an addition.
     * 
     * @param operandRight
     *        The second operand.
     * @return A new {@link JavaVariable} that represents the new value built by adding operandRight's value to this
     *         one.
     */
    public JavaVariable plus(final JavaVariable operandRight) {
        switch (type.getType()) {
        case FLOAT:
        case INTEGER:
            return new JavaVariable(this.name, this.type, addUpNumbers((Number) value, (Number) operandRight.getValue()));
        case STRING:
            return new JavaVariable(this.name, this.type, ((String) this.value) + operandRight.getValueAsString());
        default:
            throw new UnsupportedOperationException("Could not add " + operandRight + " to " + value);
        }
    }

    private Number addUpNumbers(final Number n1, final Number n2) {
        if (n1 instanceof BigDecimal || n2 instanceof BigDecimal) {
            return convertToBigDecimal(n1).add(convertToBigDecimal(n2));
        } else if (n1 instanceof BigInteger || n2 instanceof BigInteger) {
            return convertToBigInteger(n1).add(convertToBigInteger(n2));
        } else if (n1 instanceof Double || n2 instanceof Double) {
            return Double.valueOf(n1.doubleValue() + n2.doubleValue());
        } else if (n1 instanceof Float || n2 instanceof Float) {
            return Float.valueOf(n1.floatValue() + n2.floatValue());
        } else if (n1 instanceof Long || n2 instanceof Long) {
            return Long.valueOf(n1.longValue() + n2.longValue());
        } else if (n1 instanceof Integer || n2 instanceof Integer) {
            return Integer.valueOf(n1.intValue() + n2.intValue());
        } else if (n1 instanceof Byte || n2 instanceof Byte) {
            return Byte.valueOf((byte) (n1.byteValue() + n2.byteValue()));
        } else {
            return null;
        }
    }

    private BigDecimal convertToBigDecimal(final Number n) {
        if (n instanceof BigDecimal) {
            return (BigDecimal) n;
        } else if (n instanceof Number) {
            return BigDecimal.valueOf(n.doubleValue());
        } else {
            return BigDecimal.ZERO;
        }
    }

    private BigInteger convertToBigInteger(final Number n) {
        if (n instanceof BigInteger) {
            return (BigInteger) n;
        } else if (n instanceof Number) {
            return BigInteger.valueOf(n.longValue());
        } else {
            return BigInteger.ZERO;
        }
    }

}
