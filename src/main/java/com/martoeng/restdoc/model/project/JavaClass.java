package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.model.VariableType;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.TokenMgrError;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;

/**
 * Representation of a single Java class.
 * 
 * @author walterm
 * 
 */
public final class JavaClass extends JavaObject implements JavaAnnotated {

    private static final Logger LOG = LoggerFactory.getLogger(JavaClass.class);

    /** The name of this class. **/
    private final String name;
    /** The package this class belongs to. **/
    private final Package pkg;
    /** The parsed comment lines. **/
    private String[] comments;
    /** The AST of this class. **/
    private TypeDeclaration typeDeclaration;
    /** The annotations. **/
    private List<JavaAnnotation> annotations;
    /** The declared methods. **/
    private List<JavaMethod> methods;
    /** The list of fields. **/
    private List<JavaVariable> fields;
    /** The imports from the source file. **/
    private List<JavaImport> imports;
    /** Whether to exclude this class in the docs. **/
    private boolean excludedFromDoc;
    /** The classes this class depends on. **/
    private List<JavaClass> dependencies;
    /** The parent class. **/
    private JavaClass parentClass;
    /** Whether this is an enumeration class or a "normal" class. **/
    private boolean enumeration;
    /** List of enum constant names. **/
    private List<JavaEnumConstant> enumConstants;


    private JavaClass(final String name, final Package pkg) {
        this.name = name;
        this.pkg = pkg;
    }

    /** @return The package this class belongs to. **/
    public Package getPackage() {
        return this.pkg;
    }

    private void setTypeDeclaration(final TypeDeclaration typeDeclaration) {
        this.typeDeclaration = typeDeclaration;
    }

    /** @return The {@link CompilationUnit} to further analyse the class' syntax tree. **/
    public TypeDeclaration getTypeDeclaration() {
        return this.typeDeclaration;
    }

    private void setComments(final String[] comments) {
        this.comments = comments;
    }

    /** @return A String array representing the comment lines for the class definition. **/
    public String[] getComments() {
        return this.comments;
    }

    private void setMethods(final List<JavaMethod> methods) {
        this.methods = methods;
    }

    /** @return A {@link List} of type {@link JavaMethod} containing all declared methods. **/
    public List<JavaMethod> getMethods() {
        return this.methods;
    }

    private void setAnnotations(final List<JavaAnnotation> annotations) {
        this.annotations = annotations;
    }

    @Override
    public List<JavaAnnotation> getAnnotations() {
        return this.annotations;
    }

    @Override
    public JavaAnnotation findAnnotation(final String annotationName) {
        return this.findAnnotation(this.annotations, annotationName);
    }

    @Override
    public void addAnnotation(final JavaAnnotation annotation) {
        this.annotations.add(annotation);
    }

    private void setFields(final List<JavaVariable> fields) {
        this.fields = fields;
    }

    private void addField(final JavaVariable field) {
        this.fields.add(field);
    }

    /**
     * Returns all fields this class declares.
     * 
     * @return A {@link List} of {@link JavaVariable JavaVariables} representing the fields.
     */
    public List<JavaVariable> getFields() {
        return this.fields;
    }

    /**
     * Returns the field with the given name (if present).
     * 
     * @param fieldName
     *        The name of the field to retrieve.
     * @return Either the field or {@code null} if this field is not present.
     */
    public JavaVariable findField(final String fieldName) {
        for (JavaVariable field : this.fields) {
            if (field.getName().equals(fieldName)) return field;
        }
        return null;
    }

    /** @return The name of this class. **/
    public String getName() {
        return this.name;
    }

    private void setImports(final List<JavaImport> imports) {
        this.imports = imports;
    }

    /** @return A {@link List} of type {@link JavaImport} that contains the imported package/class names. **/
    public List<JavaImport> getImports() {
        return this.imports;
    }

    /** @return A {@link List} of type {@link JavaClass} that contains all the classes this class depends on. **/
    public List<JavaClass> getDependencies() {
        return this.dependencies;
    }

    private void setDependencies(final List<JavaClass> dependencies) {
        this.dependencies = dependencies;
    }

    /** @return A {@link JavaClass} representing the parent class or {@code null}. **/
    public JavaClass getParentClass() {
        return parentClass;
    }

    /**
     * Sets the parent class of this java class.
     * 
     * @param javaClass
     *        The parent class to be set or {@code null}.
     */
    protected void setParentClass(final JavaClass javaClass) {
        this.parentClass = javaClass;
    }


    /**
     * Finds and returns an import (if present).
     * 
     * @param importName
     *        The name (excluding any path segments) of the import, e.g. "Serializable".
     * @return A {@link JavaImport} object or {@code null} if no import with this name can be found.
     */
    public JavaImport findImport(final String importName) {
        for (JavaImport imp : this.imports) {
            if (imp.getName().equals(importName)) {
                return imp;
            }
        }
        return null;
    }

    /** @return {@code true} when the class should not be documented, {@code false} otherwise. **/
    public boolean isExcludedFromDoc() {
        return this.excludedFromDoc;
    }

    /**
     * Sets whether this class should be excluded from the resulting documentation.
     * 
     * @param excludedFromDoc
     *        {@code true} if the class should not be documented, {@code false} otherwise.
     */
    public void setExcludedFromDoc(final boolean excludedFromDoc) {
        this.excludedFromDoc = excludedFromDoc;
    }

    @Override
    public String toString() {
        return this.name + " (methods=" + methods.toString() + ", fields=" + fields.toString() + ", annotations="
            + annotations.toString() + ", imports=" + imports.toString()
            + (dependencies != null ? (", dependencies=" + dependencies.toString()) : "") + ")";
    }

    /**
     * Creates an array of {@link JavaClass} objects that represent all classes defined in the given source file.
     * 
     * @param sourceFile
     *        The .java source file.
     * @param encoding
     *        The name of the encoding of the source files.
     * @param pkg
     *        The package in which the class resides.
     * @return An array of type {@link JavaClass} that contains all class definitions or {@code null} in case of a
     *         parsing error.
     */
    public static JavaClass[] create(final File sourceFile, final String encoding, final Package pkg) {
        try {
            CompilationUnit cu = JavaParser.parse(sourceFile, encoding);
            List<JavaImport> imports = JavaImport.createList(cu.getImports());
            List<TypeDeclaration> types = ParserUtils.safeList(cu.getTypes());
            JavaClass[] classes = new JavaClass[types.size()];
            for (int i = 0; i < classes.length; i++) {
                TypeDeclaration type = types.get(i);
                JavaClass cls = new JavaClass(type.getName(), pkg);
                cls.setTypeDeclaration(type);

                // set enum flag and collect enum constants if necessary
                cls.setEnumeration(type instanceof EnumDeclaration);
                cls.enumConstants = cls.isEnumeration()
                    ? new EnumConstantsCollector(type).collect() : Collections.<JavaEnumConstant>emptyList();

                cls.setImports(imports);

                if (type.getComment() != null) {
                    cls.setComments(ParserUtils.parseComments(type.getComment().getContent()));
                } else {
                    cls.setComments(new String[0]);
                }
                cls.setMethods(new MethodCollector(type).collect());
                cls.setAnnotations(JavaAnnotation.createList(ParserUtils.safeList(type.getAnnotations())));
                cls.setFields(new FieldCollector(type).collect());

                // put in array and package
                classes[i] = cls;
                pkg.addClass(cls);
            }

            return classes;
        } catch (TokenMgrError | ParseException | IOException e) {
            LOG.warn("Could not parse " + sourceFile.getName(), e);
            return new JavaClass[0];
        }
    }

    /**
     * Resolves dependent classes and adds them to the list of dependencies.
     * 
     * @param cls
     *        The class to investigate.
     * @param project
     *        The project this class belongs to.
     */
    public static void resolveDependencies(final JavaClass cls, final Project project) {
        List<JavaClass> dependencies = new DependenciesCollector(cls, project).collect();
        cls.setDependencies(dependencies);

        for (JavaClass dependency : dependencies) {
            // annotations on class level
            checkAndAddAnnotation(cls, dependency);

            // fields
            for (JavaVariable dependencyField : dependency.getFields()) {
                if (cls.findField(dependencyField.getName()) == null) {
                    cls.addField(dependencyField);
                }
            }

            // methods
            for (JavaMethod dependencyMethod : dependency.getMethods()) {
                JavaMethod method = cls.findMethod(dependencyMethod.getName(), dependencyMethod.getParameters());
                if (method != null) {
                    // method present, eventually add annotations
                    checkAndAddAnnotation(method, dependencyMethod);
                } else {
                    // method not present, add it
                    cls.methods.add(dependencyMethod);
                }

            }
        }
    }

    /**
     * Checks whether a target object has the annotations of a source object. Only annotations that support inheritance
     * will be added.
     * 
     * @param target
     *        The target to be checked. Eventually annotations from the source object will be added to this object.
     * @param source
     *        The source object possibly containing annotations to be added to the target object.
     */
    private static void checkAndAddAnnotation(final JavaAnnotated target, final JavaAnnotated source) {
        for (JavaAnnotation sourceAnnotation : source.getAnnotations()) {
            if (JavaAnnotation.isAnnotationInherited(sourceAnnotation.getName()) == false) continue;

            JavaAnnotation annotation = target.findAnnotation(sourceAnnotation.getName());
            if (annotation == null) {
                // add, because it does not exist on the class
                target.addAnnotation(sourceAnnotation);
            }
        }
    }

    /**
     * Finds a method with the given name and parameter list.
     * 
     * @param methodName
     *        The name of the method.
     * @param parameters
     *        The parameter list. Only the types are compared.
     * @return Either a {@link JavaMethod} object or {@code null} if no method matches.
     */
    public JavaMethod findMethod(final String methodName, final List<JavaVariable> parameters) {
        for (JavaMethod method : this.methods) {
            if (method.getName().equals(methodName) == false) continue;
            if (method.getParameters().size() != parameters.size()) continue;

            List<JavaVariable> methodParams = method.getParameters();
            boolean match = true;
            for (int i = 0; i < parameters.size(); i++) {
                if (parameters.get(i).getType().typeEquals(methodParams.get(i).getType()) == false) {
                    match = false;
                    break;
                }
            }
            if (match) return method;
        }

        return null;
    }

    /**
     * Finds a method with the given name and parameter type list.
     * 
     * @param methodName
     *        The name of the method.
     * @param parameterTypes
     *        The parameter type list.
     * @return Either a {@link JavaMethod} object or {@code null} if no method matches.
     */
    public JavaMethod findMethodByType(final String methodName, final List<VariableType> parameterTypes) {
        for (JavaMethod method : this.methods) {
            if (method.equals(methodName) == false) continue;
            if (method.getParameters().size() != parameterTypes.size()) continue;

            List<JavaVariable> methodParams = method.getParameters();
            boolean match = true;
            for (int i = 0; i < parameterTypes.size(); i++) {
                if (parameterTypes.get(i).equals(methodParams.get(i).getType()) == false) {
                    match = false;
                    break;
                }
            }
            if (match) return method;
        }

        return null;
    }

    /**
     * Finds a method with the given name and number of parameters. Note that there may be more than one method matching
     * the name and parameter count! There is no guarantee which one of them will be chosen in that case.
     * 
     * @param methodName
     *        The name of the method.
     * @param parameterCount
     *        The number of parameters.
     * @return Either a {@link JavaMethod} object or {@code null} if no method matches.
     */
    public JavaMethod findMethodByParameterCount(final String methodName, final int parameterCount) {
        for (JavaMethod method : this.methods) {
            if (method.getName().equals(methodName) && method.getParameters().size() == parameterCount) return method;
        }

        return null;
    }

    /**
     * Creates a {@link JavaClass} from a Java reflection class definition.
     * 
     * @param cls
     *        The reflection class definition for the class.
     * @param pkg
     *        The package where the class should be added.
     * @return A new JavaClass object representing the reflection class definition including all methods inherited from
     *         parent classes.
     */
    public static JavaClass create(final Class<?> cls, final Package pkg) {
        JavaClass javaClass = new JavaClass(cls.getSimpleName(), pkg);
        javaClass.setAnnotations(JavaAnnotation.createList(cls.getAnnotations()));

        List<JavaMethod> methods = new ArrayList<>(cls.getMethods().length);
        for (Method m : cls.getMethods()) {
            if (isJavaObjectMethod(m) == false) {
                methods.add(JavaMethod.create(m));
            }
        }
        javaClass.setMethods(methods);
        javaClass.setFields(new FieldCollector(cls).collect());

        if (cls.isEnum()) {
            javaClass.setEnumeration(true);
            javaClass.enumConstants = new ArrayList<>(cls.getEnumConstants().length);
            for (Object o : cls.getEnumConstants()) {
                Enum<?> e = (Enum<?>) o;
                javaClass.enumConstants.add(new JavaEnumConstant(e.name()));
            }
        }

        // not available, so store an empty list
        javaClass.setComments(new String[0]);
        javaClass.setImports(new ArrayList<JavaImport>(0));

        pkg.addClass(javaClass);

        return javaClass;
    }

    private static boolean isJavaObjectMethod(final Method m) {
        return (m.getDeclaringClass().equals(Object.class));
    }

    /** @return {@code true} if this class represents an enumeration, {@code false} otherwise. **/
    public boolean isEnumeration() {
        return this.enumeration;
    }

    /**
     * Sets whether this class represents an enumeration or not.
     * 
     * @param enumeration
     *        {@code true} if this is an enumeration, {@code false} otherwise.
     */
    private void setEnumeration(final boolean enumeration) {
        this.enumeration = enumeration;
    }

    /** @return The list of enum constants if {@link #isEnumeration()} returns {@code true}, otherwise an empty list. **/
    public List<JavaEnumConstant> getEnumConstants() {
        return enumConstants;
    }

}
