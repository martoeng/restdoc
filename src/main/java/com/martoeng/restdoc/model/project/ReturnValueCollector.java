package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.model.DataType;
import com.martoeng.restdoc.model.ExpressionResolver;
import com.martoeng.restdoc.model.ReturnValue;
import com.martoeng.restdoc.model.StatusCode;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.LinkedList;
import java.util.List;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.stmt.ReturnStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

/**
 * A collector for discovering return values within method declarations.
 * 
 * @author walterm
 * 
 */
public class ReturnValueCollector extends VoidVisitorAdapter<List<ReturnValue>> {

    private static final String SPRING_HTTP_STATUS_QUALIFIER = "HttpStatus.";
    private static final String SPRING_HTTP_STATUS_FULL_QUALIFIER = "org.springframework.http.HttpStatus.";

    private final MethodDeclaration methodDeclaration;
    private final JavaClass cls;
    private final Project project;

    /**
     * Constructs a new collector for return values.
     * 
     * @param methodDeclaration
     *        The method declaration to use for return value discovery.
     * @param cls
     *        The class in which the method resided.
     * @param project
     *        The project to which the class belongs to.
     */
    public ReturnValueCollector(final MethodDeclaration methodDeclaration, final JavaClass cls, final Project project) {
        this.methodDeclaration = methodDeclaration;
        this.cls = cls;
        this.project = project;
    }

    /** @return The collected list of return values found in the method declaration. **/
    public List<ReturnValue> collect() {
        List<ReturnValue> list = new LinkedList<>();
        this.methodDeclaration.accept(this, list);
        return list;
    }

    @Override
    public void visit(final ReturnStmt statement, final List<ReturnValue> list) {
        Expression expr = statement.getExpr();
        if (expr instanceof MethodCallExpr) {
            // it's a method call, check for JAX-RS Response.create calls
            MethodCallExpr methodCall = (MethodCallExpr) expr;
            if (methodCall.getScope() != null) {
                String scope = methodCall.getScope().toString();
                if (scope.startsWith("Response.") || scope.startsWith("javax.ws.rs.Response.")) {
                    evaluateJaxRsResponseCall(methodCall, list);
                    super.visit(statement, list);
                    return;
                }
            }

            JavaClass calledClass = null;
            JavaMethod calledMethod = null;
            if (methodCall.getScope() != null) {
                calledClass = project.getJavaClass(ExpressionResolver.resolveName(ExpressionResolver.resolveToString(methodCall
                    .getScope(), cls, project), cls, project));
            } else {
                calledClass = cls;
            }
            if (calledClass != null) {
                calledMethod = calledClass.findMethodByParameterCount(methodCall.getName(), ParserUtils.safeList(
                    methodCall.getArgs()).size());
                if (calledMethod != null) {
                    if (calledMethod.getMethodDeclaration() != null) {
                        list.addAll(new ReturnValueCollector(calledMethod.getMethodDeclaration(), calledClass, project).collect());
                    }
                }
            }

        } else if (expr instanceof ObjectCreationExpr) {
            ObjectCreationExpr oce = (ObjectCreationExpr) expr;
            switch (oce.getType().getName()) {
            case "ResponseEntity":
                evaluateSpringResponseEntityConstructor(oce, list);
                break;
            }
        } else {
            list.add(new ReturnValue(StatusCode.OK, ExpressionResolver.resolveToString(expr, cls, project), DataType
                .valueOf(methodDeclaration.getType())));
        }
        super.visit(statement, list);
    }

    private void evaluateJaxRsResponseCall(final MethodCallExpr methodCall, final List<ReturnValue> list) {
        StatusCode code = null;
        Node expr = methodCall;
        while (true) {
            if (expr instanceof MethodCallExpr) {
                switch (((MethodCallExpr) expr).getName()) {
                case "status":
                    String constantName = ExpressionResolver.resolveToString(((MethodCallExpr) expr).getArgs().get(0), cls,
                        project);
                    if (constantName.contains(".")) {
                        constantName = constantName.substring(constantName.lastIndexOf('.'));
                    }
                    code = StatusCode.find(constantName);
                    break;
                case "temporaryRedirect":
                    code = StatusCode.TEMPORARY_REDIRECT;
                    break;
                case "serverError":
                    code = StatusCode.INTERNAL_SERVER_ERROR;
                    break;
                case "seeOther":
                    code = StatusCode.SEE_OTHER;
                    break;
                case "ok":
                    code = StatusCode.OK;
                    break;
                case "notModified":
                    code = StatusCode.NOT_MODIFIED;
                    break;
                case "notAcceptable":
                    code = StatusCode.NOT_ACCEPTABLE;
                    break;
                case "noContent":
                    code = StatusCode.NO_CONTENT;
                    break;
                case "created":
                    code = StatusCode.CREATED;
                    break;
                }
                expr = ((MethodCallExpr) expr).getScope();
            } else {
                break;
            }
        }

        if (code != null) {
            list.add(new ReturnValue(code, findReturnStatementComment(methodCall), DataType.valueOf(methodDeclaration.getType())));
        }

    }

    private void evaluateSpringResponseEntityConstructor(final ObjectCreationExpr oce, final List<ReturnValue> list) {
        StatusCode code = null;
        if (ParserUtils.safeList(oce.getArgs()).size() > 0) {
            code = getStatusCodeFromHttpStatus(oce.getArgs().get(oce.getArgs().size() - 1));
        }
        list.add(new ReturnValue(code, findReturnStatementComment(oce), DataType.valueOf(methodDeclaration.getType())));
    }

    private String findReturnStatementComment(final Node node) {
        // look for a comment for the return statement (which is a parent node of the OCE)
        Node returnStmt = node;
        while (returnStmt != null && !(returnStmt instanceof ReturnStmt)) {
            returnStmt = returnStmt.getParentNode();
        }
        return returnStmt.getComment() != null ? returnStmt.getComment().getContent() : "(Derived from " + cls.getName() + "."
            + methodDeclaration.getName() + " in the source code)";
    }

    /**
     * Convert the HttpStatus expression to a {@link StatusCode}.
     * 
     * @param expr
     *        The {@link Expression} from the parser.
     * @return A {@link StatusCode} constant.
     */
    private StatusCode getStatusCodeFromHttpStatus(final Expression expr) {
        String status = expr.toString();
        if (status.startsWith(SPRING_HTTP_STATUS_QUALIFIER)) {
            return StatusCode.find(status.substring(SPRING_HTTP_STATUS_QUALIFIER.length()));
        } else if (status.startsWith(SPRING_HTTP_STATUS_FULL_QUALIFIER)) {
            return StatusCode.find(status.substring(SPRING_HTTP_STATUS_FULL_QUALIFIER.length()));
        } else {
            return StatusCode.find(status);
        }
    }
}
