package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.util.AddRemoveObserver;

/**
 * Interface describing the functionality of a package representation.
 * 
 * @author walterm
 * 
 */
public interface Package {

    /** @return An Iterable of type Package that represents all sub-packages. **/
    Iterable<Package> getPackageIterable();

    /** @return An Iterable of type JavaClass that represents all classes present in this package. **/
    Iterable<JavaClass> getJavaClassIterable();

    /**
     * Adds a class to the package.
     * 
     * @param javaClass
     *        The {@link JavaClass} to be added.
     */
    void addClass(JavaClass javaClass);

    /**
     * Removes a class from the package.
     * 
     * @param javaClass
     *        The {@link JavaClass} to be removed.
     */
    void removeClass(JavaClass javaClass);

    /**
     * Adds a new sub-package.
     * 
     * @param pkg
     *        The package to be added.
     */
    void addPackage(Package pkg);

    /**
     * Removes a sub-package.
     * 
     * @param pkg
     *        The package to be removed.
     */
    void removePackage(Package pkg);

    /** @return The name of the package, for example "test" for "com.martoeng.test". **/
    String getName();

    /** @return The path of the package, for example "com.martoeng" for "com.martoeng.test". **/
    String getPath();

    /** @return The fully qualified package name, for example "com.martoeng.test". **/
    String getAbsoluteName();

    /** @return The parent package or {@code null} if it is the root package. **/
    Package getParent();

    /**
     * Adds an observer for packages.
     * 
     * @param observer
     *        The {@link AddRemoveObserver} of type {@link Package} to be registered.
     */
    void addPackageObserver(AddRemoveObserver<Package> observer);

    /**
     * Adds an observer for classes.
     * 
     * @param observer
     *        The {@link AddRemoveObserver} of type {@link JavaClass} to be registered.
     */
    void addJavaClassObserver(AddRemoveObserver<JavaClass> observer);

    /** @return The number of classes registered for this package. **/
    int getClassCount();

    /** @return The number of classes registered for this and all sub-packages. **/
    int getTotalClassCount();

    /**
     * Creates a package structure for the given package name, e.g. "com.example.test". If the package or parts of the
     * package path already exist, they will be used.
     * 
     * @param packageName
     *        The name of the package to create.
     * @return The final package that is created for the given path, e.g. "test" for "com.example.test".
     */
    Package buildPath(String packageName);

}
