package com.martoeng.restdoc.model.project;

import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.ModifierSet;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

/**
 * Collector for all fields of a class.
 * 
 * @author walterm
 * 
 */
public class FieldCollector extends VoidVisitorAdapter<List<JavaVariable>> {

    private final TypeDeclaration typeDeclaration;
    private final Class<?> cls;


    /**
     * Constructs a new collector.
     * 
     * @param typeDeclaration
     *        The declaration of the class or interface.
     */
    protected FieldCollector(final TypeDeclaration typeDeclaration) {
        this.typeDeclaration = typeDeclaration;
        this.cls = null;
    }

    /**
     * Constructs a new collector.
     * 
     * @param cls
     *        The declaration of the class or interface.
     */
    protected FieldCollector(final Class<?> cls) {
        this.cls = cls;
        this.typeDeclaration = null;
    }

    /** @return A {@link List} of {@link JavaVariable JavaVariables} that represent the fields. **/
    protected List<JavaVariable> collect() {
        List<JavaVariable> fields = new LinkedList<>();
        if (this.typeDeclaration != null) {
            this.typeDeclaration.accept(this, fields);
        } else {
            for (Field field : cls.getFields()) {
                fields.add(JavaVariable.create(field));
            }
        }
        return fields;

    }

    @Override
    public void visit(final FieldDeclaration fieldDeclaration, final List<JavaVariable> fieldList) {
        if (ModifierSet.isStatic(fieldDeclaration.getModifiers()) && ModifierSet.isFinal(fieldDeclaration.getModifiers())) {
            fieldList.addAll(JavaVariable.create(fieldDeclaration));
        }
        super.visit(fieldDeclaration, fieldList);
    }
}
