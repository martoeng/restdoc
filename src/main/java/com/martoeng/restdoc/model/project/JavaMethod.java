package com.martoeng.restdoc.model.project;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import java.lang.reflect.Method;
import java.util.List;
import com.martoeng.restdoc.model.VariableType;
import com.martoeng.restdoc.util.ParserUtils;

/**
 * Representation of a Java method.
 * 
 * @author walterm
 * 
 */
public final class JavaMethod extends JavaObject implements JavaAnnotated {

    private final String name;
    private final List<JavaAnnotation> annotations;
    private final String[] comments;
    private final List<JavaVariable> parameters;
    private final VariableType returnType;
    private final String returnTypeName;
    private final MethodDeclaration methodDeclaration;


    private JavaMethod(final MethodDeclaration methodDeclaration) {
        this.name = methodDeclaration.getName();
        this.annotations = JavaAnnotation.createList(methodDeclaration.getAnnotations());
        if (methodDeclaration.getComment() != null) {
            this.comments = ParserUtils.parseComments(methodDeclaration.getComment().getContent());
        } else {
            this.comments = new String[0];
        }
        this.parameters = JavaVariable.createParameterList(methodDeclaration.getParameters());
        this.methodDeclaration = methodDeclaration;
        this.returnType = VariableType.valueOf(methodDeclaration.getType());

        if (methodDeclaration.getType() instanceof ClassOrInterfaceType) {
            this.returnTypeName = ((ClassOrInterfaceType) methodDeclaration.getType()).getName();
        } else {
            this.returnTypeName = methodDeclaration.getType().toString();
        }
    }

    private JavaMethod(final Method method) {
        this.name = method.getName();
        this.annotations = JavaAnnotation.createList(method.getAnnotations());
        this.comments = new String[0];
        this.parameters = JavaVariable.createParameterList(method);
        this.methodDeclaration = null;
        this.returnType = VariableType.valueOf(method.getReturnType());
        this.returnTypeName = method.getReturnType().getCanonicalName();
    }

    /** @return The name of the method. **/
    public String getName() {
        return this.name;
    }

    /** @return The type returned by this method. **/
    public VariableType getReturnType() {
        return this.returnType;
    }

    /** @return The comments on this method. **/
    public String[] getComments() {
        return this.comments;
    }

    @Override
    public List<JavaAnnotation> getAnnotations() {
        return this.annotations;
    }

    @Override
    public void addAnnotation(final JavaAnnotation annotation) {
        this.annotations.add(annotation);
    }

    @Override
    public JavaAnnotation findAnnotation(final String annotationName) {
        return this.findAnnotation(this.annotations, annotationName);
    }

    /** @return The list of parameters for this method. **/
    public List<JavaVariable> getParameters() {
        return this.parameters;
    }

    /** @return The name of the type that is returned. **/
    public String getReturnTypeName() {
        return returnTypeName;
    }

    /** @return The parser's {@link MethodDeclaration}. **/
    public MethodDeclaration getMethodDeclaration() {
        return this.methodDeclaration;
    }

    /** @return {@code true} if this is a getter method (including boolean is-methods), {@code false} if not. **/
    public boolean isGetter() {
        if (this.parameters.size() == 0) {
            return this.name.length() > 3 && this.name.startsWith("get") && Character.isUpperCase(this.name.charAt(3))
                || this.name.length() > 2 && this.name.startsWith("is") && Character.isUpperCase(this.name.charAt(2));
        }
        return false;
    }

    /** @return {@code true} if this is a setter method, {@code false} if not. **/
    public boolean isSetter() {
        return this.parameters.size() == 1 && this.name.length() > 3 && this.name.startsWith("set")
            && Character.isUpperCase(this.name.charAt(3));
    }

    /** @return The name of the field if this is an accessor method, {@code null} otherwise. **/
    public String extractFieldName() {
        if (this.parameters.size() == 0) {
            if (this.name.charAt(0) == 'g') {
                return Character.toLowerCase(this.name.charAt(3)) + this.name.substring(4);
            } else if (this.name.charAt(0) == 'i') {
                return Character.toLowerCase(this.name.charAt(2)) + this.name.substring(3);
            }
        } else if (this.parameters.size() == 1) {
            return Character.toLowerCase(this.name.charAt(3)) + this.name.substring(4);
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name + " (annotations=" + annotations.toString() + ", parameters=" + parameters.toString() + ", returnType="
            + returnType.toString() + ")";
    }

    /**
     * Constructs a new {@link JavaMethod} via Japa parser.
     * 
     * @param methodDeclaration
     *        The parser's method declaration.
     * @return A new {@link JavaMethod} object representing the parser's declaration.
     */
    protected static JavaMethod create(final MethodDeclaration methodDeclaration) {
        return new JavaMethod(methodDeclaration);
    }

    /**
     * Constructs a new {@link JavaMethod} via Java reflection.
     * 
     * @param method
     *        The Java reflection method.
     * @return A new {@link JavaMethod} object representing the reflection {@link Method}.
     */
    protected static JavaMethod create(final Method method) {
        return new JavaMethod(method);
    }

}
