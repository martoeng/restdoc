package com.martoeng.restdoc.model.project;

import com.martoeng.restdoc.model.Entity;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.LinkedList;
import java.util.List;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

/**
 * Collector for enumeration constants.
 * 
 * @author walterm
 *
 */
public class EnumConstantsCollector extends VoidVisitorAdapter<List<JavaEnumConstant>> {

    private EnumDeclaration enumDeclaration;

    /**
     * Constructs a collector for enumeration constants
     * 
     * @param td
     *        The type declaration that shall be visited to extract the information.
     */
    protected EnumConstantsCollector(final TypeDeclaration td) {
        this.enumDeclaration = (EnumDeclaration) td;
    }

    /** @return The {@link List} of {@link Entity} objects representing the enumeration constants. **/
    public List<JavaEnumConstant> collect() {
        List<JavaEnumConstant> enumList = new LinkedList<>();
        this.enumDeclaration.accept(this, enumList);
        return enumList;
    }

    @Override
    public void visit(final EnumConstantDeclaration decl, final List<JavaEnumConstant> enumList) {
        JavaEnumConstant constant = new JavaEnumConstant(decl.getName());
        if (decl.getComment() != null) {
            constant.setComments(ParserUtils.parseComments(decl.getComment().getContent()));
        } else {
            constant.setComments(new String[0]);
        }
        enumList.add(constant);

        super.visit(decl, enumList);
    }

}
