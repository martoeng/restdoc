package com.martoeng.restdoc.model.project;

import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.QualifiedNameExpr;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author walterm
 * 
 */
public class JavaImport {

    private final String path;
    private final String name;


    /**
     * Constructs a new {@link JavaImport}.
     * 
     * @param path
     *        The path for the class, e.g. "java.io".
     * @param name
     *        The name of the import, e.g. "Serializable".
     */
    protected JavaImport(final String path, final String name) {
        this.path = path;
        this.name = name;
    }

    /** @return The name of the imported class, e.g. "GET". **/
    public String getName() {
        return this.name;
    }

    /** @return The path of the imported class, e.g. "javax.ws.rs". **/
    public String getPath() {
        return this.path;
    }

    /** @return The fully qualified name of the imported class, e.g. "javax.ws.rs.GET". **/
    public String getFullName() {
        return this.path + "." + this.name;
    }

    @Override
    public String toString() {
        return this.getFullName();
    }

    /**
     * Creates a {@link List} of type {@link JavaImport} that contains all import declarations.
     * 
     * @param importDeclarations
     *        The parser's list of import declarations.
     * @return A list containing all the declared imports as {@link JavaImport} objects.
     */
    public static List<JavaImport> createList(final List<ImportDeclaration> importDeclarations) {
        if (importDeclarations == null || importDeclarations.size() == 0) return Collections.emptyList();
        List<JavaImport> imports = new ArrayList<>(importDeclarations.size());
        for (ImportDeclaration importDeclaration : importDeclarations) {
            QualifiedNameExpr qne = (QualifiedNameExpr) importDeclaration.getName();
            imports.add(new JavaImport(qne.getQualifier().toString(), qne.getName()));
        }
        return imports;
    }
}
