package com.martoeng.restdoc.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Class representing parameter or response entities.
 * 
 * @author walterm
 * 
 */
public class Entity {

    private final String description;
    private final String simpleName;
    private final String packageName;
    private Map<String, Field> fields;


    /**
     * Constructs a new Entity.
     * 
     * @param simpleName
     *        The simple name of the class, for example "List".
     * @param packageName
     *        The full package name, for example "java.util".
     * @param description
     *        The description for this entity, a short comment about the functionality.
     */
    public Entity(final String simpleName, final String packageName, final String description) {
        this.simpleName = simpleName;
        this.packageName = packageName;
        this.description = description;
        this.fields = new HashMap<>();
    }

    /** @return The simple name of the class, for example "List". **/
    public String getSimpleName() {
        return simpleName;
    }

    /** @return The full package name, for example "java.util". **/
    public String getPackageName() {
        return packageName;
    }

    /** @return The canonical (absolute) representation of the class name. **/
    public String getCanonicalName() {
        return packageName + "." + simpleName;
    }

    /** @return The description of the entity. **/
    public String getDescription() {
        return description;
    }

    /**
     * Retrieves a field or adds a new one if it cannot be found.
     * 
     * @param fieldName
     *        The name of the field.
     * @return A {@link Field}. Either one already present or a newly created one.
     */
    public Field getOrAddField(final String fieldName) {
        Field field = fields.get(fieldName);
        if (field == null) {
            field = new Field(fieldName);
            this.fields.put(fieldName, field);
        }
        return field;
    }

    /** @return A {@link Collection} of {@link Field fields}. **/
    public Collection<Field> getFields() {
        return this.fields.values();
    }
}
