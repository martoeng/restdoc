package com.martoeng.restdoc.model;

import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Enumeration of generalized data types.
 * 
 * @author WalterM
 * 
 */
public enum VariableType {

    /** String data type (subsumes Strings and characters). **/
    STRING,
    /** Boolean data type. **/
    BOOLEAN,
    /** Integer data type (subsumes byte, short, int, long and BigInteger). **/
    INTEGER,
    /** Float data type (subsumes float, double and BigDecimal). **/
    FLOAT,
    /** List/Array data type. **/
    LIST,
    /** Map data type. **/
    MAP,
    /** Object data type. **/
    OBJECT,
    /** Enumeration data type. **/
    ENUMERATION,
    /** Void data type. **/
    VOID;

    private static Map<String, VariableType> mapping;

    /**
     * Converts the class of a type to a {@link VariableType}.
     * 
     * @param cls
     *        The class, e.g. {@code Long.class}.
     * @return A {@link VariableType} representing the given type.
     */
    public static VariableType valueOf(final Class<?> cls) {
        if (cls.isEnum()) return VariableType.ENUMERATION;
        VariableType returnedType = mapping.get(cls.getCanonicalName());
        return returnedType != null ? returnedType : OBJECT;
    }

    /**
     * Converts the type denoted by the Japa parser.
     * 
     * @param type
     *        The given parser type representation.
     * @return A {@link VariableType} representing the given type.
     */
    public static VariableType valueOf(final Type type) {
        VariableType returnedType = null;
        if (type instanceof ClassOrInterfaceType) {
            returnedType = mapping.get(((ClassOrInterfaceType) type).getName());
        } else {
            returnedType = mapping.get(type.toString());
        }
        return returnedType != null ? returnedType : OBJECT;
    }

    /**
     * Converts the type denoted by the given String.
     * 
     * @param string
     *        The String representing the (full) type name.
     * @return A {@link VariableType} representing the given type.
     */
    public static VariableType valueOfString(final String string) {
        VariableType returnedType = mapping.get(string);
        return returnedType != null ? returnedType : OBJECT;
    }

    /**
     * Converts a {@link List} of {@link Type types} to a {@link List} of {@link VariableType variable types}.
     * 
     * @param typeList
     *        The list to convert.
     * @return A {@link List} of type {@link VariableType}.
     */
    public static List<VariableType> createList(final List<Type> typeList) {
        if (typeList == null || typeList.size() == 0) return Collections.emptyList();
        List<VariableType> varTypes = new ArrayList<>(typeList.size());
        for (Type type : typeList) {
            varTypes.add(VariableType.valueOf(type));
        }
        return varTypes;
    }

    static {
        mapping = new HashMap<>();

        mapping.put("boolean", BOOLEAN);
        mapping.put("Boolean", BOOLEAN);
        mapping.put("java.lang.Boolean", BOOLEAN);

        mapping.put("char", STRING);
        mapping.put("Character", STRING);
        mapping.put("java.lang.Character", STRING);

        mapping.put("String", STRING);
        mapping.put("java.lang.String", STRING);

        mapping.put("byte", INTEGER);
        mapping.put("Byte", INTEGER);
        mapping.put("java.lang.Byte", INTEGER);

        mapping.put("short", INTEGER);
        mapping.put("Short", INTEGER);
        mapping.put("java.lang.Short", INTEGER);

        mapping.put("int", INTEGER);
        mapping.put("Int", INTEGER);
        mapping.put("Integer", INTEGER);
        mapping.put("java.lang.Integer", INTEGER);

        mapping.put("long", INTEGER);
        mapping.put("Long", INTEGER);
        mapping.put("java.lang.Long", INTEGER);

        mapping.put("BigInteger", INTEGER);
        mapping.put("java.math.BigInteger", INTEGER);

        mapping.put("float", FLOAT);
        mapping.put("java.lang.Float", FLOAT);

        mapping.put("double", FLOAT);
        mapping.put("java.lang.Double", FLOAT);

        mapping.put("BigDecimal", FLOAT);
        mapping.put("java.math.BigDecimal", FLOAT);

        mapping.put("List", LIST);
        mapping.put("java.util.List", LIST);

        mapping.put("Map", MAP);
        mapping.put("java.util.Map", MAP);
        mapping.put("HashMap", MAP);
        mapping.put("java.util.HashMap", MAP);

        mapping.put("void", VOID);
    }

}
