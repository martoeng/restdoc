package com.martoeng.restdoc.model;

import java.util.regex.Pattern;


/**
 * Configuration class that encapsulates various options.
 * 
 * @author walterm
 * 
 */
public class RestDocConfiguration {

    private static final String[] EMPTY_ARRAY = new String[0];
    private static final Pattern[] EMPTY_PATTERN_ARRAY = new Pattern[0];

    private Pattern[] returnPatterns;
    private String[] includedPackages;
    private String[] excludedPackages;
    private Pattern[] ignoredCommentPatterns;
    private String fileEncoding;


    /** Creates a new configuration instance. All options are set to empty/default values. **/
    public RestDocConfiguration() {
        this.returnPatterns = EMPTY_PATTERN_ARRAY;
        this.includedPackages = EMPTY_ARRAY;
        this.excludedPackages = EMPTY_ARRAY;
        this.ignoredCommentPatterns = EMPTY_PATTERN_ARRAY;
        this.fileEncoding = "UTF-8";
    }

    /** @return The patterns for REST method return values (never {@code null}). **/
    public Pattern[] getReturnPatterns() {
        return this.returnPatterns;
    }

    /**
     * Sets the pattern Strings for REST method return values.
     * 
     * @param returnPatternStrings
     *        The pattern Strings. If {@code null} is supplied, an empty array is used instead.
     */
    public void setReturnPatternStrings(final String[] returnPatternStrings) {
        if (returnPatternStrings == null) {
            this.returnPatterns = EMPTY_PATTERN_ARRAY;
        } else {
            this.returnPatterns = compilePatterns(returnPatternStrings);
        }
    }

    /**
     * @return A String array containing the package names that should be included in the report (never {@code null}).
     *         If the array is empty, all packages should be included (except those that are specified via
     *         {@link #setExcludedPackages(String[])}).
     **/
    public String[] getIncludedPackages() {
        return includedPackages;
    }

    /**
     * Sets the packages that should be included in the report.
     * 
     * @param includedPackageNames
     *        A String array containing the packages to include in the report. If {@code null} is supplied, an empty
     *        array is used instead.
     * @return The configuration object.
     */
    public RestDocConfiguration setIncludedPackages(final String... includedPackageNames) {
        if (includedPackages == null) {
            this.includedPackages = EMPTY_ARRAY;
        } else {
            this.includedPackages = includedPackageNames;
        }
        return this;
    }

    /**
     * @return A String array containing the package names that should be excluded from the report (never {@code null}).
     *         If the array is empty, no packages should be excluded. However, if inclusion packages are specified, only
     *         those packages will be used. If inclusion and exclusion packages are specified only those are used that
     *         are listed in the included packages and not listed in the excluded packages.
     **/
    public String[] getExcludedPackages() {
        return excludedPackages;
    }

    /**
     * Sets the packages that should be excluded from the report.
     * 
     * @param excludedPackageNames
     *        A String array containing the packages to exclude from the report. If {@code null} is supplied, an empty
     *        array is used instead.
     * @return The configuration object.
     */
    public RestDocConfiguration setExcludedPackages(final String... excludedPackageNames) {
        if (excludedPackages == null) {
            this.excludedPackages = EMPTY_ARRAY;
        } else {
            this.excludedPackages = excludedPackageNames;
        }
        return this;
    }

    /**
     * @return A {@link Pattern} array containing patterns that describe comment lines that should be excluded. This may
     *         be for example some CheckStyle or other tool hints.
     **/
    public Pattern[] getIgnoredCommentPatterns() {
        return ignoredCommentPatterns;
    }

    /**
     * Sets the patterns of comments that should be excluded.
     * 
     * @param ignoredCommentPatternStrings
     *        A String array containing the patterns for comments that should be excluded. If {@code null} is supplied,
     *        an empty array is used instead.
     */
    public void setIgnoredCommentPatternStrings(final String... ignoredCommentPatternStrings) {
        if (ignoredCommentPatternStrings == null) {
            this.ignoredCommentPatterns = EMPTY_PATTERN_ARRAY;
        } else {
            this.ignoredCommentPatterns = compilePatterns(ignoredCommentPatternStrings);
        }
    }


    /** @return The name of the encoding of the source files. **/
    public String getFileEncoding() {
        return this.fileEncoding;
    }

    /**
     * Sets the encoding of the source files.
     * 
     * @param fileEncoding
     *        The name of the character set.
     */
    public void setFileEncoding(final String fileEncoding) {
        this.fileEncoding = fileEncoding;
    }

    private static Pattern[] compilePatterns(final String[] patternStrings) {
        // compile the return patterns
        Pattern[] returnedPatterns = new Pattern[patternStrings.length];
        for (int i = 0; i < returnedPatterns.length; i++) {
            returnedPatterns[i] = Pattern.compile(patternStrings[i]);
        }
        return returnedPatterns;
    }

}
