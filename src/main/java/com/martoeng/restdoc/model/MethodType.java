package com.martoeng.restdoc.model;


/**
 * Enumeration of HTTP method types.
 * 
 * @author WalterM
 * 
 */
public enum MethodType {

    /** Normal GET request (default). **/
    GET,
    /** POST request normally including a MessageBody. **/
    POST,
    /** PUT request normally including a MessageBody. **/
    PUT,
    /** PATCH request normally including a MessageBody. **/
    PATCH,
    /** DELETE request causing the deletion of a resource. **/
    DELETE,
    /** HEAD request asking for meta-information of a resource. **/
    HEAD,
    /** OPTIONS request asking for the possible HTTP method types. **/
    OPTIONS;

    /**
     * Parses a String and returns the MethodType associated with the name.
     * 
     * @param typeName
     *        The name of the type, for example "GET".
     * @return A {@link MethodType} or {@code null} if no suitable method type could be found.
     */
    public static MethodType parse(final String typeName) {
        for (MethodType type : MethodType.values()) {
            if (type.toString().equals(typeName)) return type;
        }
        if (typeName.startsWith("org.springframework.web.bind.annotation.RequestMethod") || typeName.startsWith("RequestMethod.")) {
            return parse(typeName.substring(typeName.lastIndexOf('.') + 1));
        }
        return null;
    }

}
