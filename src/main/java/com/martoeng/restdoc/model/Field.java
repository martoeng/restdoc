package com.martoeng.restdoc.model;

/**
 * Represents a field of an entity.
 * 
 * @author walterm
 * 
 */
public class Field {

    private String name;
    private DataType dataType;
    private String description;
    private boolean readable;
    private boolean writable;

    /**
     * Constructs a new field.
     * 
     * @param name
     *        The name of the field.
     */
    public Field(final String name) {
        this.name = name;
    }

    /** @return The description of the field. **/
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the field.
     * 
     * @param description
     *        The description text.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /** @return The name of the field. **/
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the field.
     * 
     * @param name
     *        The name to be set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /** @return The {@link DataType type} of the field. **/
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Sets the type of the field.
     * 
     * @param dataType
     *        The {@link DataType type} of the field.
     */
    public void setDataType(final DataType dataType) {
        this.dataType = dataType;
    }

    /**
     * Sets whether the field is readable or not.
     * 
     * @param readable
     *        {@code true} if the field's value can be read, {@code false} otherwise.
     */
    public void setReadable(final boolean readable) {
        this.readable = readable;
    }

    /** @return {@code true} if the field is readable, {@code false} if not. **/
    public boolean isReadable() {
        return this.readable;
    }

    /**
     * Sets whether the field is writable or not.
     * 
     * @param writable
     *        {@code true} if the field's value can be set, {@code false} otherwise.
     */
    public void setWritable(final boolean writable) {
        this.writable = writable;
    }

    /** @return {@code true} if the field's value can be set, {@code false} if not. **/
    public boolean isWritable() {
        return this.writable;
    }

}
