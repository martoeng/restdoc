package com.martoeng.restdoc.model;

/**
 * Enumeration of the HTTP status codes.
 * 
 * @author WalterM
 * 
 */
public enum StatusCode {

    // CHECKSTYLE.OFF: JavadocVariable
    CONTINUE(100, "Continue"),
    SWITCHING_PROTOCOLS(101, "Switching Protocols", "switch protocols", "switching protocols"),
    PROCESSING(102, "Processing"),
    CHECKPOINT(103, "Checkpoint"),

    OK(200, "OK", "Okay", "okay"),
    CREATED(201, "Created", "create"),
    ACCEPTED(202, "Accepted", "accept"),
    NON_AUTHORATIVE_INFORMATION(203, "Non-Authorative Information", "non-authorative information", "nonauthorative information"),
    NO_CONTENT(204, "No Content", "no content", "empty"),
    RESET_CONTENT(205, "Reset Content", "reset content", "content reset"),
    PARTIAL_CONTENT(206, "Partial Content", "partial content"),
    MULTI_STATUS(207, "Multi-Status", "multi status"),
    ALREADY_REPORTED(208, "Already reported"),
    IM_USED(226, "IM used"),

    MULTIPLE_CHOICES(300, "Multiple Choices", "multiple choices", "multiple choice"),
    MOVED_PERMANENTLY(301, "Moved permanently", "moved permanently", "redirect"),
    FOUND(302, "Found", "MOVED_TEMPORARILY", "Moved temporarily"),
    SEE_OTHER(303, "See Other", "see other"),
    NOT_MODIFIED(304, "Not Modified", "not modified", "unmodified"),
    USE_PROXY(305, "Use Proxy", "use proxy", "proxied"),
    TEMPORARY_REDIRECT(307, "Temporary Redirect", "temporary redirect", "temp. redirect"),
    RESUME_INCOMPLETE(308, "Resume incomplete"),

    BAD_REQUEST(400, "Bad Request", "bad request"),
    UNAUTHORIZED(401, "Unauthorized", "not authorized"),
    PAYMENT_REQUIRED(402, "Payment required"),
    FORBIDDEN(403, "Forbidden", "access denied"),
    NOT_FOUND(404, "Not Found", "not found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed", "method not allowed", "unallowed method"),
    NOT_ACCEPTABLE(406, "Not Acceptable", "not acceptable", "unacceptable"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required", "proxy authentication required"),
    REQUEST_TIME_OUT(408, "Request Time-out", "REQUEST_TIMEOUT", "request time-out", "request timeout", "request time out",
        "request timed out"),
    CONFLICT(409, "Conflict"),
    GONE(410, "Gone"),
    LENGTH_REQUIRED(411, "Length Required"),
    PRECONDITION_FAILED(412, "Precondition Failed", "precondition failed"),
    REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large", "request entity too large"),
    REQUEST_URI_TOO_LONG(414, "Request-URI Too Long", "request uri too long"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type", "unsupported media type", "unsupported media-type",
        "media type unsupported"),
    REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not satisfied", "requested range not satisfiable"),
    EXPECTATION_FAILED(417, "Expectation failed", "expectation failed"),
    I_AM_A_TEAPOT(418, "I'm a teapot", "i'm a teapot", "teapot", "i am a teapot"),
    UPGRADE_REQUIRED(426, "Upgrade required"),
    PRECONDITION_REQUIRED(428, "Precondition Required"),
    TOO_MANY_REQUESTS(429, "Too Many Requests"),
    REQUEST_HEADER_FIELDS_TOO_LARGE(431, "Request Header Fields Too Large"),

    INTERNAL_SERVER_ERROR(500, "Internal Server Error", "internal server error", "internal error"),
    NOT_IMPLEMENTED(501, "Not Implemented", "not implemented"),
    BAD_GATEWAY(502, "Bad Gateway", "bad gateway"),
    SERVICE_UNAVAILABLE(503, "Service Unavailable", "service unavailable"),
    GATEWAY_TIME_OUT(504, "Gateway Time-out", "GATEWAY_TIMEOUT", "gateway time-out", "gateway timeout", "gateway time out"),
    HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version not supported", "http version not supported"),
    VARIANT_ALSO_NEGOTIATES(506, "Variant Also Negotiates", "variant also negotiates"),
    INSUFFICIENT_STORAGE(507, "Insufficient Storage", "insufficient storage"),
    LOOP_DETECTED(508, "Loop Detected"),
    BANDWIDTH_LIMIT_EXCEEDED(509, "Bandwidth Limit Exceeded"),
    NOT_EXTENDED(510, "Not Extended"),
    NETWORK_AUTHENTICATION_REQUIRED(511, "Network Authentication Required");

    // CHECKSTYLE.ON: JavadocVariable

    private final int status;
    private final String name;
    private final String[] alternativeNames;


    private StatusCode(final int status, final String name, final String... alternativeNames) {
        this.status = status;
        this.name = name;
        this.alternativeNames = alternativeNames;
    }

    /** @return The name of the status code as defined in the RFC. **/
    public String getName() {
        return this.name;
    }

    /** @return The HTTP status code. **/
    public int getStatus() {
        return this.status;
    }

    /** @return A String array containing alternative names for the status (for example "okay" instead of "OK"). **/
    public String[] getAlternativeNames() {
        return this.alternativeNames;
    }

    /**
     * Finds a {@link StatusCode} corresponding to the given name or status code.
     * 
     * @param nameOrStatusCode
     *        Either a constant name or the 3-digit status code.
     * @return If a status code matches, a {@link StatusCode} is returned, {@code null} otherwise.
     */
    public static StatusCode find(final String nameOrStatusCode) {
        // check if it is a 3-digit HTTP status code
        if (nameOrStatusCode.length() == 3 && Character.isDigit(nameOrStatusCode.charAt(0))
            && Character.isDigit(nameOrStatusCode.charAt(1)) && Character.isDigit(nameOrStatusCode.charAt(2))) {
            // status code
            int statusCode = Integer.parseInt(nameOrStatusCode);
            for (StatusCode status : StatusCode.values()) {
                if (status.getStatus() == statusCode) return status;
            }
            return null;
        }

        // checks all status names and alternative names
        for (StatusCode status : StatusCode.values()) {
            if (status.name().equalsIgnoreCase(nameOrStatusCode)) {
                return status;
            }
            if (status.getName().equalsIgnoreCase(nameOrStatusCode)) {
                return status;
            }
            for (String alternativeName : status.getAlternativeNames()) {
                if (alternativeName.equalsIgnoreCase(nameOrStatusCode)) {
                    return status;
                }
            }
        }

        return null;
    }

}
