package com.martoeng.restdoc.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a resource method.
 * 
 * @author WalterM
 * 
 */
public class Method {

    private final String name;
    private final List<MethodType> types;
    private String path;
    private final String description;
    private DataType returnType;
    private final List<ReturnValue> returnValues;
    private final List<Parameter> parameters;
    private final List<String> consumedMediaTypes;
    private final List<String> producedMediaTypes;
    private final Resource resource;

    /**
     * Constructs a new {@link Method}.
     * 
     * @param name
     *        The name of the method.
     * @param types
     *        The various {@link MethodType method types} this resource method supports.
     * @param path
     *        The path this resource method is mapped to.
     * @param description
     *        The description of this method.
     * @param resource
     *        The resource this method belongs to.
     */
    protected Method(final String name, final List<MethodType> types, final String path, final String description,
        final Resource resource) {
        this.name = name;
        this.types = types;
        this.path = path;
        this.description = description;
        this.returnValues = new LinkedList<>();
        this.parameters = new LinkedList<>();
        this.producedMediaTypes = new LinkedList<>();
        this.consumedMediaTypes = new LinkedList<>();
        this.resource = resource;
    }

    /** @return The name of the method. **/
    public String getName() {
        return this.name;
    }

    /** @return A {@link List} of {@link MethodType method types} this method supports. **/
    public List<MethodType> getMethodTypes() {
        return this.types;
    }

    /** @return The path this resource method is mapped to. **/
    public String getPath() {
        return this.path;
    }

    /** @return The description of this method. **/
    public String getDescription() {
        return this.description;
    }

    /** @return The canonical name of the returned type. **/
    public DataType getReturnType() {
        return this.returnType;
    }

    /**
     * Sets the returned type of this method.
     * 
     * @param type
     *        The {@link DataType} the method returns.
     */
    public void setReturnType(final DataType type) {
        this.returnType = type;
    }

    /** @return The list of possible {@link ReturnValue return values}. **/
    public List<ReturnValue> getReturnValues() {
        return this.returnValues;
    }

    /** @return The number of possible return values. **/
    public int getReturnValueCount() {
        return this.returnValues.size();
    }

    /**
     * Adds a {@link ReturnValue} for this method.
     * 
     * @param value
     *        The {@link ReturnValue} to be added.
     */
    protected void addReturnValue(final ReturnValue value) {
        this.returnValues.add(value);
    }

    /** @return The list of {@link Parameter parameters} for this method. **/
    public List<Parameter> getParameters() {
        return this.parameters;
    }

    /** @return The number of parameters. **/
    public int getParameterCount() {
        return this.parameters.size();
    }

    /**
     * Adds a {@link Parameter} for this method.
     * 
     * @param parameter
     *        The {@link Parameter} to be added.
     */
    protected void addParameter(final Parameter parameter) {
        this.parameters.add(parameter);
    }

    /**
     * @return The list of consumed media types. This returns only the method-specific media types excluding any
     *         definitions made by the resource.
     **/
    public List<String> getConsumedMediaTypes() {
        return this.consumedMediaTypes;
    }

    /**
     * @return The number of consumed media types. This returns only the number of method-specific media types excluding
     *         any resource definitions.
     **/
    public int getConsumedMediaTypesCount() {
        return this.consumedMediaTypes.size();
    }

    /**
     * Adds a consumed media type.
     * 
     * @param mediaType
     *        The media type, e.g. "application/json".
     */
    protected void addConsumedMediaType(final String mediaType) {
        this.consumedMediaTypes.add(mediaType);
    }

    /**
     * Adds all consumed media types in the collection.
     * 
     * @param mediaTypes
     *        The collection of media types to be added.
     */
    protected void addConsumedMediaTypes(final Collection<String> mediaTypes) {
        this.consumedMediaTypes.addAll(mediaTypes);
    }

    /**
     * @return The list of produced media types. This returns only the method-specific media types excluding any
     *         definitions made by the resource.
     **/
    public List<String> getProducedMediaTypes() {
        return this.producedMediaTypes;
    }

    /**
     * @return The number of produced media types. This returns only the number of method-specific media types excluding
     *         any resource definitions.
     */
    public int getProducedMediaTypesCount() {
        return this.producedMediaTypes.size();
    }

    /**
     * Adds a produced media type for this method.
     * 
     * @param mediaType
     *        The media type, e.g. "application/json".
     */
    protected void addProducedMediaType(final String mediaType) {
        this.producedMediaTypes.add(mediaType);
    }

    /**
     * Adds all produced media types in the collection.
     * 
     * @param mediaTypes
     *        The collection of media types to be added.
     */
    protected void addProducedMediaTypes(final Collection<String> mediaTypes) {
        this.producedMediaTypes.addAll(mediaTypes);
    }

    /** @return The resource this method belongs to. **/
    public Resource getResource() {
        return resource;
    }

    @Override
    public String toString() {
        return this.getName() + " (path=\"" + this.path + "\", returnValues=" + returnValues.toString() + ", consumes="
            + this.consumedMediaTypes.toString() + ", produces=" + this.producedMediaTypes.toString() + ", parameters="
            + this.parameters.toString() + ")";
    }

    /**
     * Sets the path for this method.
     * 
     * @param path
     *        The new path to be used.
     */
    protected void setPath(final String path) {
        this.path = path;
    }

}
