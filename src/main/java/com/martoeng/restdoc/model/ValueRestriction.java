package com.martoeng.restdoc.model;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import com.martoeng.restdoc.model.project.JavaAnnotation;
import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.Project;

/**
 * Represents the restrictions put on some values by various annotations.
 * 
 * @author WalterM
 * 
 */
public abstract class ValueRestriction {

    /** Placeholder for textual restriction representations. **/
    private static final String VALUE_PLACEHOLDER = "{value}";

    /**
     * Validates a value.
     * 
     * @param value
     *        The value to be validated.
     * @return {@code true} if the value is valid, {@code false} otherwise.
     */
    public abstract boolean validateValue(Object value);

    /** @return Returns a very short description of the restriction with applied values, e.g. "10 < value < 20". **/
    public abstract String getDescription();

    /**
     * Returns the short description with a replaced placeholder.
     * 
     * @param valueName
     *        The name of the value to use instead of the placeholder.
     * @return A very short description of the restriction with applied values and replaced placeholders.
     */
    public String getDescription(final String valueName) {
        return this.getDescription().replace(VALUE_PLACEHOLDER, valueName);
    }

    /**
     * Create all value restrictions that are specified via annotations.
     * 
     * @param annotations
     *        The list of {@link JavaAnnotation annotations}.
     * @param javaClass
     *        The class in which the element with the annotations was found.
     * @param project
     *        The project to which the class belongs.
     * @return A list of {@link ValueRestriction restrictions} that can be either checked or printed.
     */
    public static List<ValueRestriction> getValueRestrictions(final List<JavaAnnotation> annotations, final JavaClass javaClass,
        final Project project) {
        List<ValueRestriction> restrictions = new LinkedList<ValueRestriction>();
        for (JavaAnnotation annotation : annotations) {
            switch (annotation.getName()) {
            case "AssertFalse":
            case "AssertTrue":
                restrictions.add(new BooleanValueRestriction(annotation.getName().endsWith("True")));
                break;
            case "DecimalMax":
            case "Max":
            case "Min":
            case "DecimalMin":
                createMinMaxRestriction(javaClass, project, restrictions, annotation);
                break;
            case "Size":
                createSizeRestriction(javaClass, project, annotation);
                break;
            case "Pattern":
                PatternRestriction patternRestriction = new PatternRestriction(ExpressionResolver.resolveToString(annotation
                    .getParameter("regexp"), javaClass, project));
                restrictions.add(patternRestriction);
                break;
            case "NotNull":
                restrictions.add(new NotNullRestriction());
                break;
            }
        }

        return restrictions;
    }

    private static void createMinMaxRestriction(final JavaClass javaClass, final Project project,
        final List<ValueRestriction> restrictions, final JavaAnnotation annotation) {
        MinMaxRestriction minMaxRestriction = null;
        boolean add = true;
        for (ValueRestriction restriction : restrictions) {
            if (restriction instanceof MinMaxRestriction) {
                minMaxRestriction = (MinMaxRestriction) restriction;
                add = false;
                break;
            }
        }
        if (minMaxRestriction == null) minMaxRestriction = new MinMaxRestriction();
        BigDecimal value = new BigDecimal(ExpressionResolver.resolveToString(annotation
            .getParameter(JavaAnnotation.VALUE), javaClass, project));
        if (annotation.getName().endsWith("Max")) {
            minMaxRestriction.setMax(value);
        } else {
            minMaxRestriction.setMin(value);
        }
        if (add) restrictions.add(minMaxRestriction);
    }

    private static MinMaxRestriction createSizeRestriction(final JavaClass javaClass, final Project project,
        final JavaAnnotation annotation) {
        MinMaxRestriction sizeRestriction = new MinMaxRestriction();
        if (annotation.hasParameter("min")) {
            sizeRestriction.setMin(new BigDecimal(ExpressionResolver.resolveToString(annotation.getParameter("min"),
                javaClass, project)));
        }
        if (annotation.hasParameter("max")) {
            sizeRestriction.setMax(new BigDecimal(ExpressionResolver.resolveToString(annotation.getParameter("max"),
                javaClass, project)));
        }
        return sizeRestriction;
    }


    /**
     * Represents a not-null restriction.
     * 
     * @author WalterM
     * 
     */
    static class NotNullRestriction extends ValueRestriction {

        @Override
        public boolean validateValue(final Object value) {
            return (value != null);
        }

        @Override
        public String getDescription() {
            return VALUE_PLACEHOLDER + " must not be null";
        }
    }

    /**
     * Represents a boolean value restriction (AssertTrue, AssertFalse).
     * 
     * @author WalterM
     * 
     */
    static class BooleanValueRestriction extends ValueRestriction {

        private final Boolean booleanValue;

        protected BooleanValueRestriction(final boolean value) {
            this.booleanValue = Boolean.valueOf(value);
        }

        @Override
        public boolean validateValue(final Object value) {
            return (value != null ? booleanValue.equals(value) : booleanValue.equals(Boolean.FALSE));
        }

        @Override
        public String getDescription() {
            return VALUE_PLACEHOLDER + " must be " + booleanValue.toString();
        }
    }

    /**
     * Represents a min/max value restriction.
     * 
     * @author WalterM
     * 
     */
    static class MinMaxRestriction extends ValueRestriction {

        private BigDecimal min;
        private BigDecimal max;

        private void setMin(final BigDecimal min) {
            this.min = min;
        }

        private void setMax(final BigDecimal max) {
            this.max = max;
        }

        /**
         * {@inheritDoc} The value is assumed to be a BigDecimal or a String.
         */
        @Override
        public boolean validateValue(final Object value) {
            if (value instanceof BigDecimal) {
                return validateBigDecimal((BigDecimal) value);
            } else if (value instanceof String) {
                return validateString((String) value);
            }

            return false;
        }

        private boolean validateBigDecimal(final BigDecimal value) {
            if (min != null) {
                if (this.min.compareTo(value) > 0) {
                    return false;
                }
            }
            if (max != null) {
                if (this.max.compareTo(value) < 0) {
                    return false;
                }
            }
            return true;
        }

        private boolean validateString(final String value) {
            return value.length() >= (this.min != null ? this.min.longValue() : -1)
                && value.length() <= (this.max != null ? this.max.longValue() : Long.MAX_VALUE);
        }

        @Override
        public String getDescription() {
            return (this.min != null ? this.min.toString() + " <= " : "")
                + VALUE_PLACEHOLDER
                + (this.max != null ? " <= " + this.max.toString() : "");
        }
    }

    /**
     * Represents a regex pattern restriction.
     * 
     * @author WalterM
     * 
     */
    static class PatternRestriction extends ValueRestriction {

        private final Pattern pattern;

        protected PatternRestriction(final String pattern) {
            this.pattern = Pattern.compile(pattern);
        }

        @Override
        public boolean validateValue(final Object value) {
            return this.pattern.matcher((CharSequence) value).matches();
        }

        @Override
        public String getDescription() {
            return VALUE_PLACEHOLDER + " must be like " + this.pattern.pattern();
        }
    }

}
