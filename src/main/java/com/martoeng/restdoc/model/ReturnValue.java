package com.martoeng.restdoc.model;

/**
 * Represents a method's return value.
 * 
 * @author WalterM
 * 
 */
public class ReturnValue {

    private final StatusCode statusCode;
    private final DataType returnType;
    private final String description;


    /**
     * Constructs a new {@link ReturnValue}.
     * 
     * @param statusCode
     *        The status code which is returned, e.g. 200.
     * @param description
     *        The description why and when this value is returned.
     * @param returnType
     *        The {@link DataType} of the returned value.
     */
    public ReturnValue(final StatusCode statusCode, final String description, final DataType returnType) {
        this.statusCode = statusCode;
        this.description = description;
        this.returnType = returnType;
    }

    /** @return The HTTP status code which is returned, e.g. 200. **/
    public StatusCode getStatusCode() {
        return this.statusCode;
    }

    /** @return The description why and when the value is returned. **/
    public String getDescription() {
        return this.description;
    }

    /** @return The returned {@link DataType}. **/
    public DataType getReturnType() {
        return returnType;
    }

}
