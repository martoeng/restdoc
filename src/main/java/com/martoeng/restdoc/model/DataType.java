package com.martoeng.restdoc.model;

import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.Project;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.TypeParameter;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.PrimitiveType;
import com.github.javaparser.ast.type.ReferenceType;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.type.VoidType;

/**
 * Represents a data type that can have a sub-type
 * 
 * @author walterm
 * 
 */
public class DataType {

    /** Map containing instances to avoid duplicate instantiations of trivial data types. **/
    private static final Map<String, DataType> SAVED_INSTANCES = new HashMap<>();

    private final DataType subtype;
    private VariableType type;
    private final String typeName;

    /**
     * Constructs a new data type without a sub-type.
     * 
     * @param type
     *        The main type.
     */
    protected DataType(final VariableType type) {
        this.type = type;
        this.subtype = null;
        this.typeName = null;
    }

    /**
     * Constructs a new data type without a sub-type.
     * 
     * @param type
     *        The main type.
     * @param typeName
     *        The full name of the type described.
     */
    protected DataType(final VariableType type, final String typeName) {
        this.type = type;
        this.subtype = null;
        this.typeName = typeName;
    }

    /**
     * Constructs a new data type with a sub-type.
     * 
     * @param type
     *        The main type.
     * @param typeName
     *        The full name of the type described.
     * @param subType
     *        If the type is a list or array this is the type of the entries. If {@code null} is supplied, no generic
     *        information will be stored.
     */
    protected DataType(final VariableType type, final String typeName, final DataType subType) {
        this.type = type;
        this.subtype = subType;
        this.typeName = typeName;
    }

    /**
     * Constructs a new data type for an enum.
     * 
     * @param enumName
     *        The full class name of the enum.
     */
    protected DataType(final String enumName) {
        this.type = VariableType.ENUMERATION;
        this.subtype = null;
        this.typeName = enumName;
    }

    /**
     * Constructs a new data type.
     * 
     * @param type
     *        The main type.
     * @param subtype
     *        If the type is a list or array this is the type of the entries.
     */
    protected DataType(final VariableType type, final DataType subtype) {
        this.type = type;
        this.subtype = subtype;
        this.typeName = null;
    }

    /** @return The {@link VariableType type} description. **/
    public VariableType getType() {
        return type;
    }

    /** @return If the {@link #getType() type} is a list or array this contains the type of the entries. **/
    public DataType getSubtype() {
        return subtype;
    }

    /** @return The canonical class name of the enumeration. **/
    public String getEnumName() {
        return typeName;
    }

    /** @return The name of the type, if it is an object. **/
    public String getTypeName() {
        return this.typeName;
    }

    @Override
    public String toString() {
        if (type.equals(VariableType.ENUMERATION)) {
            return "ENUM:" + typeName.substring(typeName.lastIndexOf('.') + 1);
        } else if (subtype == null) {
            return type.name() + (type.equals(VariableType.OBJECT) ? ":" + typeName : "");
        } else {
            return type.name() + "<" + subtype.toString() + ">";
        }
    }

    /**
     * Checks if the type equals another type.
     * 
     * @param other
     *        The other DataType object.
     * @return {@code true} if the types equal, {@code false} otherwise.
     */
    public boolean typeEquals(final DataType other) {
        if (other == null) return false;
        if (this.type == null) return other.type == null;
        return (this.type.equals(other.type));
    }

    /**
     * Converts a {@link Type} declaration into a DataType.
     * 
     * @param type
     *        The type to convert.
     * @return A DataType instance that best represents the given type declaration.
     */
    public static DataType valueOf(final Type type) {
        String fullTypeName = extractFullTypeName(type);
        DataType returnedType = SAVED_INSTANCES.get(fullTypeName);
        if (returnedType != null) return returnedType;

        if (type instanceof PrimitiveType) {
            returnedType = new DataType(VariableType.valueOf(type));
        } else if (type instanceof VoidType) {
            returnedType = new DataType(VariableType.VOID);
        } else if (type instanceof ReferenceType) {
            if (((ReferenceType) type).getType() instanceof ClassOrInterfaceType) {
                ClassOrInterfaceType coit = (ClassOrInterfaceType) ((ReferenceType) type).getType();
                VariableType vt = VariableType.valueOf(coit);
                if (coit.getTypeArgs() != null) {
                    returnedType = new DataType(vt, DataType.valueOf(coit.getTypeArgs().get(0)));
                } else {
                    returnedType = new DataType(vt, extractFullTypeName(coit));
                }
            } else if (((ReferenceType) type).getType() instanceof PrimitiveType) {
                // primitive array
                returnedType = new DataType(VariableType.LIST, DataType.valueOf(((ReferenceType) type).getType()));
            } else {
                returnedType = new DataType(VariableType.valueOf(type), fullTypeName);
            }
        } else {
            returnedType = new DataType(VariableType.valueOf(type), fullTypeName);
        }

        SAVED_INSTANCES.put(fullTypeName, returnedType);
        return returnedType;
    }

    private static String extractFullTypeName(final Type type) {
        String name = type.toString();
        // check if canonical
        if (name.indexOf('.') != -1) return name;

        // check primitive type
        if (type instanceof ReferenceType && ((ReferenceType) type).getType() instanceof PrimitiveType) {
            return name;
        }

        Node node = type;
        while (node != null) {
            node = node.getParentNode();
            if (node instanceof MethodDeclaration) {
                // check for parameterized type
                MethodDeclaration md = (MethodDeclaration) node;
                for (TypeParameter tp : ParserUtils.safeList(md.getTypeParameters())) {
                    if (tp.getName().equals(name)) {
                        return "<" + name + ">";
                    }
                }
            } else if (node instanceof CompilationUnit) {
                CompilationUnit cu = (CompilationUnit) node;
                for (ImportDeclaration id : ParserUtils.safeList(cu.getImports())) {
                    if (id.getName().toString().endsWith("." + name)) {
                        return id.getName().toString();
                    }
                }
                return cu.getPackage().getName().toString() + "." + name;
            } else if (node instanceof ClassOrInterfaceDeclaration) {
                ClassOrInterfaceDeclaration coid = (ClassOrInterfaceDeclaration) node;
                // check type parameters
                for (TypeParameter tp : ParserUtils.safeList(coid.getTypeParameters())) {
                    if (tp.getName().equals(name)) {
                        return "<" + name + ">";
                    }
                }
                // prepend package name
                if (coid.getName().equals(name)) {
                    while (node != null) {
                        if (node instanceof CompilationUnit) {
                            return ((CompilationUnit) node).getPackage().toString() + name;
                        }
                        node = node.getParentNode();
                    }
                }
            }
        }

        return name;
    }

    /**
     * Converts a Class<?> definition into a DataType.
     * 
     * @param cls
     *        The class definition to convert.
     * @return A DataType instance that best represents the given class.
     */
    public static DataType valueOf(final Class<?> cls) {
        if (cls.isArray()) {
            return new DataType(VariableType.LIST, DataType.valueOf(cls.getComponentType()));
        } else if (cls.isEnum()) {
            return new DataType(cls.getCanonicalName());
        } else {
            return new DataType(VariableType.valueOf(cls), cls.getCanonicalName());
        }
    }

    /**
     * Consolidates all {@link DataType} instances and looks for enumerations. Enumerations can only be discovered after
     * all classes have been introspected, because a variable declaration of a normal reference type and an enumeration
     * looks perfectly the same.
     * 
     * @param project
     *        The {@link Project Java project} that contains all the class definitions.
     */
    protected static void consolidateEnumDataTypes(final Project project) {
        for (Entry<String, DataType> entry : SAVED_INSTANCES.entrySet()) {
            consolidateEnumDataTypes(entry.getValue(), project);
        }
    }

    private static void consolidateEnumDataTypes(final DataType dataType, final Project project) {
        if (dataType == null) return;
        if (dataType.type.equals(VariableType.OBJECT) && dataType.typeName != null) {
            JavaClass cls = project.getJavaClass(dataType.typeName);
            if (cls != null) {
                if (cls.isEnumeration()) {
                    dataType.type = VariableType.ENUMERATION;
                }
            }
            if (dataType.subtype != null) {
                consolidateEnumDataTypes(dataType.subtype, project);
            }
        }
    }
}
