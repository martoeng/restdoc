package com.martoeng.restdoc.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 * Represents a resource containing {@link Method methods}.
 * 
 * @author WalterM
 * 
 */
public class Resource {

    private static final String DEFAULT_PATH = "/";

    private final String name;
    private final String description;
    private String basePath;
    private final List<Method> methods;
    private final Set<String> consumedMediaTypes;
    private final Set<String> producedMediaTypes;


    /**
     * Constructs a new resource.
     * 
     * @param name
     *        The name of the resource.
     * @param description
     *        The description for the resource (if present).
     * @param basePath
     *        The base path this resource covers.
     */
    protected Resource(final String name, final String description, final String basePath) {
        this.name = name;
        this.description = description;
        this.basePath = StringUtils.isNotEmpty(basePath) ? basePath : DEFAULT_PATH;
        this.methods = new LinkedList<>();
        this.consumedMediaTypes = new HashSet<>();
        this.producedMediaTypes = new HashSet<>();
    }

    /** @return The name of the resource. **/
    public String getName() {
        return this.name;
    }

    /** @return The description of the resource (if present). **/
    public String getDescription() {
        return this.description;
    }

    /**
     * @return {@code true} if a description is present and {@link #getDescription()} will return it, {@code false}
     *         otherwise.
     **/
    public boolean hasDescription() {
        return StringUtils.isNotBlank(this.description);
    }

    /** @return The base path of the resource. If no special path is set, "/" is the default path. **/
    public String getBasePath() {
        return this.basePath;
    }

    /**
     * Sets the base path of this resource.
     * 
     * @param basePath
     *        The new base path.
     */
    protected void setBasePath(final String basePath) {
        this.basePath = basePath;
    }

    /** @return The list of methods that are registered for this resource. **/
    public List<Method> getMethods() {
        return this.methods;
    }

    /** @return The number of methods registered for this resource. **/
    public int getMethodCount() {
        return this.methods.size();
    }

    /**
     * Adds a method for this resource.
     * 
     * @param method
     *        The {@link Method method} to be added.
     */
    protected void addMethod(final Method method) {
        this.methods.add(method);
    }

    /** @return A {@link Set} of the media types consumed by this resource. **/
    public Set<String> getConsumedMediaTypes() {
        return this.consumedMediaTypes;
    }

    /** @return The number of media types consumed by this resource. **/
    public int getConsumedMediaTypesCount() {
        return this.consumedMediaTypes.size();
    }

    /**
     * Adds a consumed media type for this resource.
     * 
     * @param mediaType
     *        The media type, e.g. "application/json".
     */
    protected void addConsumedMediaType(final String mediaType) {
        this.consumedMediaTypes.add(mediaType);
    }

    /**
     * Adds consumed media types for this resource from a given collection.
     * 
     * @param mediaTypeCollection
     *        The collection of media types to be added.
     * @see #addConsumedMediaType(String)
     */
    protected void addConsumedMediaTypes(final Collection<String> mediaTypeCollection) {
        this.consumedMediaTypes.addAll(mediaTypeCollection);
    }

    /** @return A {@link Set} of media types produced by this resource. **/
    public Set<String> getProducedMediaTypes() {
        return this.producedMediaTypes;
    }

    /** @return The number of media types this resource produces. **/
    public int getProducedMediaTypesCount() {
        return this.producedMediaTypes.size();
    }

    /**
     * Adds a produced media type for this resource.
     * 
     * @param mediaType
     *        The media type, e.g. "application/json".
     */
    protected void addProducedMediaType(final String mediaType) {
        this.producedMediaTypes.add(mediaType);
    }

    /**
     * Adds produced media types for this resource from a given collection.
     * 
     * @param mediaTypeCollection
     *        The collection of media types to be added.
     */
    protected void addProducedMediaTypes(final Collection<String> mediaTypeCollection) {
        this.producedMediaTypes.addAll(mediaTypeCollection);
    }
}
