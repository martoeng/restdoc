package com.martoeng.restdoc.model;

import com.martoeng.restdoc.model.project.JavaAnnotation;
import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.JavaImport;
import com.martoeng.restdoc.model.project.JavaMethod;
import com.martoeng.restdoc.model.project.JavaVariable;
import com.martoeng.restdoc.model.project.Project;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.ArrayList;
import java.util.List;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.expr.SuperExpr;

/**
 * Resolves expressions as stored in {@link JavaVariable} values or {@link JavaAnnotation} parameter values.
 * 
 * @author walterm
 * 
 */
public final class ExpressionResolver {

    private ExpressionResolver() {}

    /**
     * Resolves an expression and returns a {@link JavaVariable} representation. The method may throw a
     * {@link RuntimeException} if the expression cannot be resolved.
     * 
     * @param expression
     *        The expression to resolve. May be for example a simple String or a binary expression.
     * @param cls
     *        The {@link JavaClass} where the expression was found.
     * @param project
     *        The {@link Project} which contains the class.
     * @return A {@link JavaVariable} that represents (best) the given expression.
     */
    public static JavaVariable resolveToVariable(final Object expression, final JavaClass cls, final Project project) {
        if (expression instanceof String) {
            return JavaVariable.createFromValue(expression);
        } else if (expression instanceof Integer || expression instanceof Long) {
            return JavaVariable.createFromValue(expression);
        } else if (expression instanceof StringLiteralExpr) {
            return JavaVariable.createFromValue(((StringLiteralExpr) expression).getValue());
        } else if (expression instanceof NameExpr) {
            return JavaVariable.createFromValue(resolveName(((NameExpr) expression).getName(), cls, project));
        } else if (expression instanceof BinaryExpr) {
            return resolveBinaryExpression(expression, cls, project);
        } else if (expression instanceof ArrayInitializerExpr) {
            return resolveArrayInitializerExpression(expression, cls, project);
        } else if (expression instanceof FieldAccessExpr) {
            return resolveFieldAccessExpression(expression, cls, project);
        } else if (expression instanceof SuperExpr) {
            return JavaVariable.createFromValue("super");
        } else if (expression instanceof MethodCallExpr) {
            return resolveMethodCallExpression(expression, cls, project);
        } else if (expression instanceof ObjectCreationExpr) {
            ObjectCreationExpr oce = (ObjectCreationExpr) expression;
            return JavaVariable.createFromType(resolveName(oce.getType().getName(), cls, project));
        }

        throw new RuntimeException("Expression '" + expression.toString() + "' (type " + expression.getClass().getSimpleName()
            + ") could not be resolved in " + cls.getPackage().getAbsoluteName() + "." + cls.getName() + "!");
    }

    private static JavaVariable resolveMethodCallExpression(final Object expression, final JavaClass cls, final Project project) {
        MethodCallExpr e = ((MethodCallExpr) expression);
        JavaClass c = project.getJavaClass(resolveName(((NameExpr) e.getScope()).getName(), cls, project));
        if (c == null) {
            return null;
        }
        List<JavaVariable> paramList = new ArrayList<>(ParserUtils.safeList(e.getArgs()).size());
        for (Expression paramExpr : ParserUtils.safeList(e.getArgs())) {
            paramList.add(resolveToVariable(paramExpr, cls, project));
        }
        JavaMethod method = c.findMethod(e.getName(), paramList);
        return JavaVariable.createFromType(method.getReturnTypeName());
    }

    private static JavaVariable resolveBinaryExpression(final Object expression, final JavaClass cls, final Project project) {
        BinaryExpr be = (BinaryExpr) expression;
        JavaVariable operandLeft = resolveToVariable(be.getLeft(), cls, project);
        JavaVariable operandRight = resolveToVariable(be.getRight(), cls, project);
        switch (be.getOperator()) {
        case plus:
            return operandLeft.plus(operandRight);
        default:
            throw new UnsupportedOperationException("Cannot resolve operators other than '+': " + be.toString() + ".");
        }
    }

    private static JavaVariable resolveArrayInitializerExpression(final Object expression, final JavaClass cls,
        final Project project) {
        ArrayInitializerExpr aie = (ArrayInitializerExpr) expression;
        List<Expression> arrayList = aie.getValues();
        if (arrayList.size() == 0) {
            return JavaVariable.createFromValue("");
        } else if (arrayList.size() == 1) {
            return resolveToVariable(arrayList.get(0), cls, project);
        } else {
            StringBuilder builder = new StringBuilder(32 * arrayList.size());
            builder.append(resolveToString(arrayList.get(0), cls, project));
            for (int i = 1; i < arrayList.size(); i++) {
                builder.append(", ");
                builder.append(resolveToString(arrayList.get(i), cls, project));
            }
            return JavaVariable.createFromValue(builder.toString());
        }
    }

    private static JavaVariable resolveFieldAccessExpression(final Object expression, final JavaClass cls, final Project project) {
        FieldAccessExpr fae = (FieldAccessExpr) expression;
        String className = resolveName(resolveToString(fae.getScope(), cls, project), cls, project);
        if (project.containsJavaClass(className)) {
            JavaVariable field = project.getJavaClass(className).findField(fae.getField());
            if (field != null && field.getValue() != null) {
                return field; // resolveToVariable(field.getValue(), cls, project);
            } else {
                return JavaVariable.createFromValue(className + "." + fae.getField());
            }
        } else {
            return JavaVariable.createFromValue(className + "." + fae.getField());
        }
    }


    /**
     * Resolves an expression and returns a String representation. The method may throw a {@link RuntimeException} if
     * the expression cannot be resolved.
     * 
     * @param expression
     *        The expression to resolve. May be for example a simple String or a binary expression.
     * @param cls
     *        The {@link JavaClass} where the expression was found.
     * @param project
     *        The {@link Project} which contains the class.
     * @return A String that represents (best) the given expression.
     */
    public static String resolveToString(final Object expression, final JavaClass cls, final Project project) {
        return resolveToVariable(expression, cls, project).getValueAsString();
    }

    /**
     * Resolves an expression to a String array.
     * 
     * @param expression
     *        The expression to resolve. May be for example a simple String or an Array initializer expression.
     * @param cls
     *        The {@link JavaClass} where the expression was found.
     * @param project
     *        The {@link Project} which contains the class.
     * @return A String that represents (best) the given expression.
     */
    public static String[] resolveToStringArray(final Expression expression, final JavaClass cls, final Project project) {
        if (expression instanceof StringLiteralExpr || expression instanceof NameExpr || expression instanceof FieldAccessExpr) {
            return new String[] {
                resolveToString(expression, cls, project)
            };
        } else if (expression instanceof ArrayInitializerExpr) {
            List<Expression> exprList = ((ArrayInitializerExpr) expression).getValues();
            String[] exprArray = new String[exprList.size()];
            for (int i = 0; i < exprList.size(); i++) {
                exprArray[i] = resolveToString(exprList.get(i), cls, project);
            }
            return exprArray;
        }

        return new String[0];
    }

    /**
     * Resolves the name of a class or field to its value or qualified name.
     * 
     * @param name
     *        The name to resolve.
     * @param cls
     *        The {@link JavaClass} which contains the expression.
     * @param project
     *        The {@link Project} which contains the class.
     * @return A String that represents the value or qualified name.
     */
    public static String resolveName(final String name, final JavaClass cls, final Project project) {
        // check super
        if (name.equals("super")) {
            return cls.getParentClass().getPackage().getAbsoluteName() + "." + cls.getParentClass().getName();
        }

        // check fields
        JavaVariable field = cls.findField(name);
        if (field != null) {
            return resolveToString(field.getValue(), cls, project);
        }

        // check import statements
        JavaImport imp = cls.findImport(name);
        if (imp != null) {
            return imp.getPath() + "." + imp.getName();
        }

        // check if it is a class on the same package level
        if (project.containsJavaClass(cls.getPackage().getAbsoluteName() + "." + name)) {
            return cls.getPackage().getAbsoluteName() + "." + name;
        }

        // check java.lang package
        try {
            Class.forName("java.lang." + name);
            return "java.lang." + name;
        } catch (ClassNotFoundException cnf) {
            // return the name as-is
            return name;
        }
    }

}
