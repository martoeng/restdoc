package com.martoeng.restdoc.model;

import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.JavaEnumConstant;
import com.martoeng.restdoc.model.project.JavaImport;
import com.martoeng.restdoc.model.project.JavaMethod;
import com.martoeng.restdoc.model.project.JavaVariable;
import com.martoeng.restdoc.model.project.Project;
import com.martoeng.restdoc.model.project.ReturnValueCollector;
import com.martoeng.restdoc.util.DocParser;
import com.martoeng.restdoc.util.ParserUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a full RESTdoc report for all resources.
 * 
 * @author walterm
 * 
 */
public class RestDocReport {

    private final String title;
    private final String description;
    private final List<Resource> resources;
    private final List<Entity> entities;
    private final List<Enumeration> enumerations;


    /**
     * Constructs a new report.
     * 
     * @param title
     *        The title of the report (normally the project name).
     * @param description
     *        The description of the report. A short introductory text.
     */
    protected RestDocReport(final String title, final String description) {
        this.title = title;
        this.description = description;
        this.resources = new LinkedList<>();
        this.entities = new LinkedList<>();
        this.enumerations = new LinkedList<>();
    }

    /** @return The title of the project report. **/
    public String getTitle() {
        return this.title;
    }

    /** @return The introductory description of the report. **/
    public String getDescription() {
        return this.description;
    }

    /** @return The {@link List} of {@link Resource} objects describing the registered controllers. **/
    public List<Resource> getResources() {
        return this.resources;
    }

    /** @return The {@link List} of {@link Entity} objects describing the registered parameter or response entities. **/
    public List<Entity> getEntities() {
        return this.entities;
    }

    /**
     * Adds a new {@link Resource}.
     * 
     * @param resource
     *        The resource to be included in the report.
     */
    public void addResource(final Resource resource) {
        this.resources.add(resource);
    }

    /**
     * Adds a new {@link Entity}.
     * 
     * @param entity
     *        The entity to be included in the report.
     */
    public void addEntity(final Entity entity) {
        this.entities.add(entity);
    }

    /**
     * Adds a new {@link Enumeration}.
     * 
     * @param enumeration
     *        The enumeration to be included in the report.
     */
    public void addEnumeration(final Enumeration enumeration) {
        this.enumerations.add(enumeration);
    }

    /**
     * Generates a new {@link RestDocReport} based on a given {@link Project Java project}.
     * 
     * @param project
     *        The project for which the report shall be generated.
     * @return A {@link RestDocReport} object that contains the final {@link Resource} objects.
     */
    public static RestDocReport create(final Project project) {
        return create(project, createStandardConfiguration());
    }

    private static RestDocConfiguration createStandardConfiguration() {
        RestDocConfiguration configuration = new RestDocConfiguration();
        configuration.setReturnPatternStrings(DocParser.RETURN_PATTERNS);
        return configuration;
    }

    /**
     * Generates a new {@link RestDocReport} based on a given {@link Project Java project}.
     * 
     * @param project
     *        The project for which the report shall be generated.
     * @param configuration
     *        A {@link RestDocConfiguration} object setting various options.
     * @return A {@link RestDocReport} object that contains the final {@link Resource} objects.
     */
    public static RestDocReport create(final Project project, final RestDocConfiguration configuration) {
        RestDocReport report = new RestDocReport(project.getTitle(), project.getDescription());
        Set<String> entityNames = new HashSet<>();
        Set<String> enumNames = new HashSet<>();

        for (Iterator<JavaClass> classIterator = project.getJavaClassIterator(); classIterator.hasNext();) {
            JavaClass cls = classIterator.next();
            if (cls.isExcludedFromDoc()) continue;
            if (isPackageIncluded(cls.getPackage().getAbsoluteName(), configuration) == false) continue;

            // name, description and path
            String name = cls.getName();
            String description = DocParser.parseDescription(cls.getComments(), configuration.getIgnoredCommentPatterns());
            String basePath = DocParser.parsePath(cls.getAnnotations(), cls, project);
            Resource resource = new Resource(name, description, basePath);

            // media types for resource
            Set<String> resourceConsumedMediaTypes = DocParser.parseConsumedMediaTypes(cls.getAnnotations(), cls, project);
            resource.addConsumedMediaTypes(resourceConsumedMediaTypes);
            Set<String> resourceProducedMediaTypes = DocParser.parseProducedMediaTypes(cls.getAnnotations(), cls, project);
            resource.addProducedMediaTypes(resourceProducedMediaTypes);

            // iterate over methods
            for (JavaMethod javaMethod : cls.getMethods()) {
                List<MethodType> methodTypes = DocParser.parseMethodTypes(javaMethod.getAnnotations(), cls, project);
                if (methodTypes.isEmpty()) continue;

                String path = DocParser.parsePath(javaMethod.getAnnotations(), cls, project);
                String methodDescription = DocParser.parseDescription(javaMethod.getComments(), configuration
                    .getIgnoredCommentPatterns());

                Method method = new Method(javaMethod.getName(), methodTypes, path, methodDescription, resource);

                // media types
                convertConsumedMediaTypes(javaMethod, cls, project, method, resourceConsumedMediaTypes);
                convertProducedMediaTypes(javaMethod, cls, project, method, resourceProducedMediaTypes);

                // parameters
                convertMethodParameters(project, cls, javaMethod, method, configuration.getIgnoredCommentPatterns());

                // return value
                convertReturnValues(report, javaMethod, method, cls, project, configuration);
                method.setReturnType(extractTypeInformation(javaMethod.getReturnTypeName(), cls, project));
                extractParameterAndReturnTypes(entityNames, enumNames, method);

                resource.addMethod(method);
            }

            while (alignBasePath(resource)) {/* call alignBasePath as long as it returns true */}

            // only add when the resource has something (namely methods) to offer
            if (resource.getMethodCount() > 0) {
                report.addResource(resource);
            }
        }

        // consolidate data types
        DataType.consolidateEnumDataTypes(project);

        // convert entities
        entityNames.remove(null);
        convertEntities(project, report, entityNames, enumNames, configuration);

        // convert enumerations
        convertEnums(project, report, enumNames, configuration);

        return report;
    }

    private static void extractParameterAndReturnTypes(final Set<String> entityNames, final Set<String> enumNames,
        final Method method) {
        // parameters
        for (Parameter parameter : method.getParameters()) {
            if (parameter.getType().equals(VariableType.ENUMERATION)) {
                enumNames.add(parameter.getType().getEnumName());
            } else if (parameter.getType().getType().equals(VariableType.OBJECT)) {
                entityNames.add(parameter.getType().getTypeName());
            }
        }

        // return type
        if (method.getReturnType().getType().equals(VariableType.OBJECT)) {
            entityNames.add(method.getReturnType().getTypeName());
            DataType subType = method.getReturnType().getSubtype();
            while (subType != null) {
                entityNames.add(subType.getTypeName());
                subType = subType.getSubtype();
            }
        } else if (method.getReturnType().equals(VariableType.ENUMERATION)) {
            enumNames.add(method.getReturnType().getEnumName());
        }
    }

    private static void convertEntities(final Project project, final RestDocReport report, final Set<String> entityNames,
        final Set<String> enumNames, final RestDocConfiguration configuration) {

        for (String entityName : entityNames) {
            JavaClass cls = project.getJavaClass(entityName);
            if (cls == null) continue;
            Entity entity = convertEntity(cls, configuration);
            report.addEntity(entity);
        }

        List<Entity> entitiesToSearch = new ArrayList<>(report.getEntities());
        List<Entity> newEntitiesToSearch = new ArrayList<>();
        do {
            newEntitiesToSearch.clear();
            for (Entity entity : entitiesToSearch) {
                for (Field field : entity.getFields()) {
                    if (field.getDataType().getType().equals(VariableType.OBJECT)) {
                        if (entityNames.contains(field.getDataType().getTypeName()) == false) {
                            JavaClass cls = project.getJavaClass(field.getDataType().getTypeName());
                            if (cls == null) continue;
                            Entity additionalEntity = convertEntity(cls, configuration);
                            report.addEntity(additionalEntity);
                            entityNames.add(additionalEntity.getCanonicalName());
                            newEntitiesToSearch.add(additionalEntity);
                        }
                    } else if (field.getDataType().getType().equals(VariableType.LIST)
                        && field.getDataType().getSubtype().getType().equals(VariableType.OBJECT)) {
                        JavaClass cls = project.getJavaClass(field.getDataType().getSubtype().getTypeName());
                        if (cls == null) continue;
                        Entity additionalEntity =
                            convertEntity(cls, configuration);
                        report.addEntity(additionalEntity);
                        entityNames.add(additionalEntity.getCanonicalName());
                        newEntitiesToSearch.add(additionalEntity);
                    } else if (field.getDataType().getType().equals(VariableType.ENUMERATION)) {
                        enumNames.add(field.getDataType().getEnumName());
                    }

                }
            }
            entitiesToSearch = new ArrayList<>(newEntitiesToSearch);
        } while (newEntitiesToSearch.isEmpty() == false);

    }

    private static Entity convertEntity(final JavaClass cls, final RestDocConfiguration configuration) {
        Entity entity = new Entity(cls.getName(), cls.getPackage().getAbsoluteName(),
            DocParser.parseDescription(cls.getComments(), configuration.getIgnoredCommentPatterns()));
        for (JavaMethod method : cls.getMethods()) {
            if (method.isGetter()) {
                String fieldName = method.extractFieldName();
                Field field = entity.getOrAddField(fieldName);
                field.setDataType(new DataType(method.getReturnType(), method.getReturnTypeName()));
                field.setReadable(true);
                String s = DocParser.parseReturnDescription(method.getComments(), configuration.getIgnoredCommentPatterns());
                if (s.isEmpty() == false) {
                    field.setDescription(s);
                } else {
                    s = DocParser.parseDescription(method.getComments(), configuration.getIgnoredCommentPatterns());
                    if (s.isEmpty() == false) field.setDescription(s);
                }
            } else if (method.isSetter()) {
                String fieldName = method.extractFieldName();
                Field field = entity.getOrAddField(fieldName);
                field.setDataType(method.getParameters().get(0).getType());
                field.setWritable(true);
                if (field.getDescription() == null) {
                    field.setDescription(DocParser.parseDescription(method.getComments(), configuration
                        .getIgnoredCommentPatterns()));
                }
            }
        }

        return entity;
    }

    private static void convertEnums(final Project project, final RestDocReport report, final Set<String> enumNames,
        final RestDocConfiguration configuration) {

        for (String enumName : enumNames) {
            JavaClass cls = project.getJavaClass(enumName);
            if (cls == null) continue;
            Enumeration enumeration = new Enumeration(cls.getName(), cls.getPackage().getAbsoluteName());
            for (JavaEnumConstant constant : cls.getEnumConstants()) {
                enumeration.addEntry(constant.getName(), DocParser.parseDescription(constant.getComments(),
                    configuration.getIgnoredCommentPatterns()));
            }
            report.addEnumeration(enumeration);
        }
    }


    /**
     * Extracts the type information available in form of a type name String.
     * 
     * @param returnTypeName
     *        The name of the type as a String. May be canonical or not. May contain generic type information.
     * @param cls
     *        The class which declares the method or variable.
     * @param project
     *        The project containing the class.
     * @return
     */
    private static DataType extractTypeInformation(final String typeName, final JavaClass cls, final Project project) {
        if (typeName == null) {
            return new DataType(VariableType.VOID);
        } else {
            String canonicalName = typeName;
            DataType subType = null;
            if (typeName.indexOf('<') != -1) {
                // generic type information
                subType = extractTypeInformation(typeName.substring(typeName.indexOf('<') + 1, typeName.length() - 1), cls,
                    project);
                canonicalName = typeName.substring(0, typeName.indexOf('<'));
            }

            if (canonicalName.indexOf('.') != -1) {
                // canonical name
                return new DataType(VariableType.valueOfString(canonicalName), canonicalName, subType);
            } else {
                // simple name, check basic data types
                VariableType vt = VariableType.valueOfString(canonicalName);
                if (vt.equals(VariableType.OBJECT)) {
                    // look for imports

                    JavaImport imp = cls.findImport(canonicalName);
                    if (imp != null) {
                        return new DataType(vt, imp.getFullName(), subType);
                    } else {
                        return new DataType(vt, null, null);
                    }
                } else {
                    return new DataType(vt, canonicalName);
                }
            }
        }
    }

    private static boolean alignBasePath(final Resource resource) {
        if (resource.getMethodCount() <= 1) return false;

        Method firstMethod = resource.getMethods().get(0);
        String firstPathSegment = firstMethod.getPath().substring(0, firstMethod.getPath().indexOf('/', 1));
        for (Method method : resource.getMethods()) {
            if (firstPathSegment.equals(method.getPath().substring(0, method.getPath().indexOf('/', 1))) == false) {
                return false;
            }
        }

        // all methods have the same first path segment, copy that to the resource
        resource.setBasePath(ParserUtils.buildPath(resource.getBasePath(), firstPathSegment) + "/");
        for (Method method : resource.getMethods()) {
            method.setPath(method.getPath().substring(firstPathSegment.length()));
        }

        return true;
    }

    private static void convertConsumedMediaTypes(final JavaMethod javaMethod, final JavaClass cls, final Project project,
        final Method method, final Set<String> resourceConsumedMediaTypes) {
        Set<String> methodConsumedMediaTypes = DocParser.parseConsumedMediaTypes(
            javaMethod.getAnnotations(), cls, project);
        if (methodConsumedMediaTypes.isEmpty()) methodConsumedMediaTypes.addAll(resourceConsumedMediaTypes);
        method.addConsumedMediaTypes(methodConsumedMediaTypes);
    }

    private static void convertProducedMediaTypes(final JavaMethod javaMethod, final JavaClass cls, final Project project,
        final Method method, final Set<String> resourceProducedMediaTypes) {
        Set<String> methodProducedMediaTypes = DocParser.parseProducedMediaTypes(
            javaMethod.getAnnotations(), cls, project);
        if (methodProducedMediaTypes.isEmpty()) methodProducedMediaTypes.addAll(resourceProducedMediaTypes);
        method.addProducedMediaTypes(methodProducedMediaTypes);
    }

    private static void convertMethodParameters(final Project project, final JavaClass cls, final JavaMethod javaMethod,
        final Method method, final Pattern[] ignoredComments) {

        for (JavaVariable parameter : javaMethod.getParameters()) {
            // only use parameters that have an annotation, without they will be injected by a framework
            if (parameter.getAnnotations().isEmpty()) continue;

            String paramName = DocParser.parseParamName(parameter, cls, project);
            List<ValueRestriction> restrictions = ValueRestriction.getValueRestrictions(
                parameter.getAnnotations(), cls, project);
            Parameter param = new Parameter(parameter.getType(), paramName, DocParser.parseParamDescription(javaMethod
                .getComments(), paramName, ignoredComments), restrictions);
            method.addParameter(param);
        }
    }

    private static void convertReturnValues(final RestDocReport report, final JavaMethod javaMethod, final Method method,
        final JavaClass cls, final Project project, final RestDocConfiguration configuration) {
        if (javaMethod.getReturnType().equals(VariableType.VOID)) {
            method.addReturnValue(new ReturnValue(StatusCode.OK, "", DataType.valueOf(void.class)));
        } else {
            String returnDescription = DocParser.parseReturnDescription(javaMethod.getComments(), configuration
                .getIgnoredCommentPatterns());
            for (String sentence : returnDescription.split("[\\.!]")) {
                for (Pattern pattern : configuration.getReturnPatterns()) {
                    Matcher matcher = pattern.matcher(sentence + ".");
                    if (matcher.matches()) {
                        ReturnValue rv = convertMatcherToReturnValue(matcher, method);
                        if (rv != null) method.addReturnValue(rv);
                    }
                }
            }

            if (method.getReturnValueCount() == 0) {
                if (javaMethod.getMethodDeclaration() != null) {
                    List<ReturnValue> values = new ReturnValueCollector(javaMethod.getMethodDeclaration(), cls, project)
                        .collect();
                    for (ReturnValue value : values) {
                        method.addReturnValue(value);
                    }
                }
            }

            if (method.getReturnValueCount() == 0 && returnDescription.isEmpty() == false) {
                method.addReturnValue(new ReturnValue(StatusCode.OK, returnDescription, method.getReturnType()));
            }
        }
    }

    private static boolean isPackageIncluded(final String packageName, final RestDocConfiguration configuration) {
        if (configuration.getExcludedPackages().length > 0) {
            for (String excludedPackageName : configuration.getExcludedPackages()) {
                if (packageName.startsWith(excludedPackageName)) {
                    return false;
                }
            }
        }

        if (configuration.getIncludedPackages().length > 0) {
            for (String includedPackageName : configuration.getIncludedPackages()) {
                if (packageName.startsWith(includedPackageName)) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }

    private static ReturnValue convertMatcherToReturnValue(final Matcher matcher, final Method method) {
        try {
            String status = matcher.group(DocParser.STATUS_CAPTURING_GROUP_NAME);
            String description = matcher.group(DocParser.DESCRIPTION_CAPTURING_GROUP_NAME);
            if (description.endsWith(",")) description = description.substring(0, description.length() - 1).trim();
            StatusCode code = StatusCode.find(status);
            // if no code can be matched, there's probably no meaningful sentence, return OK
            if (code == null) {
                return null;
            }
            return new ReturnValue(code, description.length() > 0 ? description + "." : description, method.getReturnType());
        } catch (Exception e) {
            return null;
        }
    }


}
