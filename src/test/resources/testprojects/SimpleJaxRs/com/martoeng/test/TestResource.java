package com.martoeng.test;

import java.io.Serializable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Controller class for several test resources.
 * 
 * @author walterm
 * 
 */
@Path("/test")
@Produces("application/json")
public class TestResource implements Serializable {
    
    private static final String ID_PARAM = 'i' + "d";

    /**
     * Retrieves nothing.
     * 
     * @param id
     *        The id of the object that will never be returned and therefore renders this parameter totally useless. Any
     *        integer value can be supplied.
     */
    @GET
    @Path("{" + ID_PARAM + "}")
    public void getNothing(@PathParam(ID_PARAM) final int id) {
        return;
    }

}