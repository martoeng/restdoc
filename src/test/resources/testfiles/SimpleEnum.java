/** Simple enum class. **/
enum SimpleEnum {
    VALUE_1(1, "Test value 1"),
    VALUE_2(2, "Test value 2");
    
    private int[] values;
}