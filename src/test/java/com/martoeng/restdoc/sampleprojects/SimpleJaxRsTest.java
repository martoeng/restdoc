package com.martoeng.restdoc.sampleprojects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.io.File;
import org.junit.Test;
import com.martoeng.restdoc.model.ExpressionResolver;
import com.martoeng.restdoc.model.Method;
import com.martoeng.restdoc.model.Parameter;
import com.martoeng.restdoc.model.Resource;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.StatusCode;
import com.martoeng.restdoc.model.VariableType;
import com.martoeng.restdoc.model.project.JavaAnnotation;
import com.martoeng.restdoc.model.project.JavaClass;
import com.martoeng.restdoc.model.project.JavaMethod;
import com.martoeng.restdoc.model.project.Package;
import com.martoeng.restdoc.model.project.Project;

/**
 * Test class for the SimpleJaxRs test project.
 * 
 * @author walterm
 * 
 */
public class SimpleJaxRsTest {

    private static final String TEST_TITLE = "test title";
    private static final String TEST_DESCRIPTION = "test description";

    /** Tests the project with the internal {@link Project} view. **/
    @Test
    public void testSimpleJaxRsProject() {
        Project project = Project.create(TEST_TITLE, TEST_DESCRIPTION, "UTF-8", new File(
            "src/test/resources/testprojects/SimpleJaxRs"));
        Package projectPackage = project.getProjectPackage();
        assertEquals("", projectPackage.getAbsoluteName());
        assertEquals("", projectPackage.getName());
        assertEquals(1 + 1, projectPackage.getTotalClassCount()); // one defined, one dependency
        Package test = project.getPackage("com.martoeng.test");
        assertNotNull(test);
        JavaClass testResource = project.getJavaClass("com.martoeng.test.TestResource");
        assertNotNull(testResource);
        assertNotNull(testResource.getComments());
        assertEquals(1, testResource.getMethods().size());
        JavaMethod method = testResource.getMethods().get(0);
        assertEquals(1, method.getParameters().size());
        assertEquals("id", ExpressionResolver.resolveToString(method.getParameters().get(0).findAnnotation("PathParam")
            .getParameter("value"), testResource, project));
        assertEquals(2, method.getAnnotations().size());
        JavaAnnotation pathAnnotation = method.findAnnotation("Path");
        assertEquals("{id}", ExpressionResolver.resolveToString(pathAnnotation.getParameter("value"), testResource, project));
    }

    /** Tests the project with the external {@link RestDocReport} view. **/
    @Test
    public void testSimpleJaxRsRestDoc() {
        RestDocReport restDocReport = RestDocReport.create(Project.create(TEST_TITLE, TEST_DESCRIPTION, "UTF-8", new File(
            "src/test/resources/testprojects/SimpleJaxRs")));
        assertEquals(TEST_TITLE, restDocReport.getTitle());
        assertEquals(TEST_DESCRIPTION, restDocReport.getDescription());
        assertNotNull(restDocReport.getResources());
        assertEquals(1, restDocReport.getResources().size());
        Resource resource = restDocReport.getResources().get(0);
        assertEquals("/test/", resource.getBasePath());
        assertEquals(1, resource.getProducedMediaTypesCount());
        // check methods
        assertEquals(1, resource.getMethodCount());
        Method method = resource.getMethods().get(0);
        assertNotNull(method);
        assertEquals("getNothing", method.getName());
        assertEquals(1, method.getProducedMediaTypesCount());
        assertEquals(1, method.getParameterCount());
        Parameter param = method.getParameters().get(0);
        assertEquals("id", param.getName());
        assertEquals(VariableType.INTEGER, param.getType());
        assertEquals("The id of the object that will never be returned and therefore renders this parameter totally useless. "
            + "Any integer value can be supplied.", param.getDescription());
        // check return value
        assertEquals(1, method.getReturnValueCount());
        assertEquals(StatusCode.OK, method.getReturnValues().get(0).getStatusCode());
    }

}
