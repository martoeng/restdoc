package com.martoeng.restdoc.sampleprojects;

import com.martoeng.restdoc.model.RestDocConfiguration;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.project.Project;
import com.martoeng.restdoc.printer.NiceHtmlPrinter;
import com.martoeng.restdoc.printer.RestDocPrinter;
import com.martoeng.restdoc.util.DocParser;
import java.io.File;
import org.junit.Test;

/**
 * Small test class for the REST example project.
 * 
 * @author walterm
 * 
 */
public class RestExampleTest {

    /** Test the Spring Boot MVC REST application. **/
    @Test
    public void testRestExample() {
        Project project = Project.create("RESTexample", "Planets REST example", "UTF-8", new File(
            "..\\rest-example\\src\\main\\java"));

        RestDocConfiguration config = new RestDocConfiguration();
        config.setReturnPatternStrings(DocParser.RETURN_PATTERNS);
        config.setIgnoredCommentPatternStrings(new String[] {
            "CHECKSTYLE\\.O.*", "CHECKSTYLE:.*"
        });
        RestDocReport report = RestDocReport.create(project, config);

        RestDocPrinter printer = new NiceHtmlPrinter();
        printer.print(new File("D:\\temp"), report);
    }
}
