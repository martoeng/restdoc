package com.martoeng.restdoc.sampleprojects;

import com.martoeng.restdoc.model.RestDocConfiguration;
import com.martoeng.restdoc.model.RestDocReport;
import com.martoeng.restdoc.model.project.Project;
import com.martoeng.restdoc.printer.NiceHtmlPrinter;
import com.martoeng.restdoc.printer.RestDocPrinter;
import com.martoeng.restdoc.util.DocParser;
import java.io.File;
import org.junit.Test;

/**
 * Small test class for the CDA.
 * 
 * @author walterm
 * 
 */
public class PraxisTest {

    /** Test the mobile API package of the CDA. **/
    @Test
    public void testCda() {
        Project project = Project.create("CDA", "Content Delivery Application", "UTF-8", new File(
            "D:\\workspaces\\kepler_CS\\CDA_trunk\\src\\main\\java"));

        RestDocConfiguration config = new RestDocConfiguration();
        config.setIncludedPackages("de.dw.cda.api");
        config.setReturnPatternStrings(DocParser.RETURN_PATTERNS);
        config.setIgnoredCommentPatternStrings(new String[] {
            "CHECKSTYLE\\.O.*", "CHECKSTYLE:.*"
        });
        RestDocReport report = RestDocReport.create(project, config);

        RestDocPrinter printer = new NiceHtmlPrinter();
        printer.print(new File("D:\\temp"), report);
    }
}
