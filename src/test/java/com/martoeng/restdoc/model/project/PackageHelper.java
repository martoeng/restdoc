package com.martoeng.restdoc.model.project;

import org.codehaus.plexus.util.StringUtils;

/**
 * Helper class when working with packages.
 * 
 * @author walterm
 * 
 */
public final class PackageHelper {

    private PackageHelper() {}

    /**
     * Dumps the contents of a package and all of its sub-packages.
     * 
     * @param pkg
     *        The package to dump.
     * @param level
     *        The level of the indentation. Should normally be 0 for the initial method call.
     */
    public static void dumpPackageContents(final Package pkg, final int level) {
        // CHECKSTYLE.OFF: RegexpSinglelineJava
        System.out.print(StringUtils.repeat(" ", level * 3));
        System.out.println(pkg.getName());
        for (JavaClass cls : pkg.getJavaClassIterable()) {
            System.out.print(StringUtils.repeat(" ", level * 3 + 3));
            System.out.print("class ");
            System.out.println(cls.getName());
        }
        for (Package subpackage : pkg.getPackageIterable()) {
            dumpPackageContents(subpackage, level + 1);
        }
        // CHECKSTYLE.ON: RegexpSinglelineJava
    }

}
