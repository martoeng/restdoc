package com.martoeng.restdoc.model.project;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import com.martoeng.restdoc.model.ExpressionResolver;
import com.martoeng.restdoc.model.VariableType;

/**
 * Test class for {@link JaxRsTestClass}.
 * 
 * @author walterm
 * 
 */
public class JaxRsTestClassTest {

    /** Tests whether a Java class is correctly converted via reflection. **/
    @Test
    public void testJaxRsTestClass() {
        JavaClass cls = JavaClass.create(JaxRsTestClass.class, new StandardPackage("test", null));
        assertNotNull(cls);
        // class annotations
        assertEquals(2, cls.getAnnotations().size());
        assertEquals(JavaAnnotation.JAXRS_PATH, cls.findAnnotation(JavaAnnotation.JAXRS_PATH).getName());
        assertEquals("/test", ExpressionResolver.resolveToString(
            cls.findAnnotation(JavaAnnotation.JAXRS_PATH).getParameter(JavaAnnotation.VALUE), cls, null));
        // methods
        assertEquals(1, cls.getMethods().size());
        JavaMethod method = cls.getMethods().get(0);
        assertEquals("getNothing", method.getName());
        assertEquals(VariableType.VOID, method.getReturnType());
        assertEquals(1, method.getParameters().size());
        assertEquals(VariableType.INTEGER, method.getParameters().get(0).getType());
        assertEquals("id", ExpressionResolver.resolveToString(method.getParameters().get(0).getAnnotations().get(0).getParameter(
            JavaAnnotation.VALUE), cls, null));
        assertArrayEquals(new String[] {
            "application/json"
        }, ExpressionResolver.resolveToStringArray(method.findAnnotation("Produces").getParameter(JavaAnnotation.VALUE), cls,
            null));
    }
}
