package com.martoeng.restdoc.model.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.io.File;
import org.junit.Test;

/**
 * Test class for checking Java class enum functionality.
 * 
 * @author walterm
 *
 */
public class EnumClassTest {

    /** Tests whether an enumeration is recognized as such. **/
    @Test
    public void testSimpleEnum() {
        JavaClass cls = JavaClass.create(new File("src/test/resources/testfiles/SimpleEnum.java"), "UTF-8",
            new StandardPackage("", null))[0];
        assertTrue(cls.isEnumeration());
        List<JavaEnumConstant> constants = cls.getEnumConstants();
        assertNotNull(constants);
        assertEquals(2, constants.size());
        assertEquals("VALUE_1", constants.get(0).getName());
        assertEquals("VALUE_2", constants.get(1).getName());
    }
}
