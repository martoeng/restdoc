package com.martoeng.restdoc.model.project;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Test class for a sample JAX-RS project.
 * 
 * @author WalterM
 * 
 */
@Path("/test")
@Produces(javax.ws.rs.core.MediaType.APPLICATION_XML)
public class JaxRsTestClass {

    private static final String ID_PARAM = "id";

    /**
     * Simple test method.
     * 
     * @param idOfNothing
     *        The id of nothing to retrieve
     */
    @GET
    @Path("{" + ID_PARAM + "}")
    @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public void getNothing(@PathParam(ID_PARAM) final int idOfNothing) {
        return;
    }

}
