package com.martoeng.restdoc.model.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Test;
import com.martoeng.restdoc.util.ParserUtils;

/**
 * Test class for {@link ParserUtils}.
 * 
 * @author WalterM
 * 
 */
public class ParserUtilsTest {

    /** Test method for {@link ParserUtils#safeList(java.util.List)}. **/
    @Test
    public void testSafeList() {
        List<Object> nullList = null;
        assertNotNull(ParserUtils.safeList(nullList));
        assertTrue(ParserUtils.safeList(nullList).isEmpty());
    }

    /** Test method for {@link ParserUtils#extractPackage(String)}. **/
    public void testExtractPackage() {
        assertEquals("com.martoeng.test", ParserUtils.extractPackage("com.martoeng.test.TestClass"));
        assertEquals("", ParserUtils.extractPackage("TestClass"));
        assertEquals("com", ParserUtils.extractPackage("com.TestClass"));
    }
}
