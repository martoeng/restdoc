package com.martoeng.restdoc.model;

import static org.junit.Assert.assertEquals;
import java.math.BigInteger;
import org.junit.Test;

/**
 * Test class for {@link VariableType}.
 * 
 * @author WalterM
 * 
 */
public class VariableTypeTest {

    /** Tests the {@link VariableType#valueOf(Class)} method. **/
    @Test
    public void testValueOf() {
        assertEquals(VariableType.BOOLEAN, VariableType.valueOf(boolean.class));
        assertEquals(VariableType.BOOLEAN, VariableType.valueOf(Boolean.class));
        assertEquals(VariableType.INTEGER, VariableType.valueOf(int.class));
        assertEquals(VariableType.INTEGER, VariableType.valueOf(BigInteger.class));
        assertEquals(VariableType.STRING, VariableType.valueOf(char.class));
        assertEquals(VariableType.VOID, VariableType.valueOf(void.class));
        assertEquals(VariableType.OBJECT, VariableType.valueOf(java.lang.Class.class));
    }

}
