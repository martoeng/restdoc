package com.martoeng.restdoc.util;

import org.junit.Test;

/**
 * Test class for {@link DocParser}.
 * 
 * @author walterm
 * 
 */
public class DocParserTest {

    /** Test method for {@link DocParser#parseJavaDocTags(String)}. **/
    @Test
    public void testParseJavaDocTags() {
        // {@code}
        assertCharsEqual("<code>true</code>", DocParser.parseJavaDocTags("{@code true}"));
        assertCharsEqual("Either <code>true</code> or <code>false</code>.", DocParser
            .parseJavaDocTags("Either {@code true} or {@code false}."));
        // {@docRoot}
        assertCharsEqual("", DocParser.parseJavaDocTags("{@docRoot}"));
        // {@inheritDoc}
        assertCharsEqual("See parent method.", DocParser.parseJavaDocTags("{@inheritDoc}"));
        // {@value}
        assertCharsEqual("Can be value of MyClass#MyField.", DocParser.parseJavaDocTags("Can be {@value MyClass#MyField}."));
        // {@literal}
        assertCharsEqual("An @ character.", DocParser.parseJavaDocTags("An {@literal @} character."));
        // {@link}
        assertCharsEqual("A CharSequence of length 0.", DocParser.parseJavaDocTags("A {@link CharSequence} of length 0."));
        assertCharsEqual("A sequence of length 0.", DocParser.parseJavaDocTags("A {@link CharSequence sequence} of length 0."));
        // {@linkplain}
        assertCharsEqual("A CharSequence of length 0.", DocParser.parseJavaDocTags("A {@linkplain CharSequence} of length 0."));
        assertCharsEqual("A sequence of length 0.", DocParser
            .parseJavaDocTags("A {@linkplain CharSequence sequence} of length 0."));
    }

    /**
     * Asserts that two CharSequences are equal. Unfortunately the equals method of String or StringBuilder does not
     * work with instances of other classes.
     * 
     * @param s1
     *        The first character sequence.
     * @param s2
     *        The second character sequence.
     */
    public static void assertCharsEqual(final CharSequence s1, final CharSequence s2) {
        if (s1.length() != s2.length()) throw new AssertionError("Different lengths: \"" + s1 + "\"" + ", \"" + s2 + "\"", null);
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i)) throw new AssertionError("Character difference at position " + i + ": "
                + s1.subSequence(0, i + 1) + ", \"" + s2.subSequence(0, i + 1));
        }
    }
}
